#include "pcnd.h"
#include "timer.h"
#include <libpcnd.h>

#if defined(_MSC_VER) && (_MSC_VER > 1000)
#    pragma once
#    pragma warning(disable: 4611)
#    pragma warning(disable: 4786)
#    if defined(_WIN64)
#        if defined(_DEBUG)
#            pragma comment(lib, "libtiff64MTd.lib")
#            pragma comment(lib, "libjpeg64MTd.lib")
#            pragma comment(lib, "libpng64MTd.lib")
#            pragma comment(lib, "zlib64MTd.lib")
#            pragma comment(lib, "libpcnd64d.lib")
#        else
#            pragma comment(lib, "libtiff64MT.lib")
#            pragma comment(lib, "libjpeg64MT.lib")
#            pragma comment(lib, "libpng64MT.lib")
#            pragma comment(lib, "zlib64MT.lib")
#            pragma comment(lib, "libpcnd64.lib")
#        endif
#    else
#        if defined(_DEBUG)
#            pragma comment(lib, "libtiffMTd.lib")
#            pragma comment(lib, "libjpegMTd.lib")
#            pragma comment(lib, "libpngMTd.lib")
#            pragma comment(lib, "zlibMTd.lib")
#            pragma comment(lib, "libpcndd.lib")
#        else
#            pragma comment(lib, "libtiffMT.lib")
#            pragma comment(lib, "libjpegMT.lib")
#            pragma comment(lib, "libpngMT.lib")
#            pragma comment(lib, "zlibMT.lib")
#            pragma comment(lib, "libpcnd.lib")
#        endif
#    endif // _WIN64
#endif

///////////////////////////////////////////////////////////////////////////
Pcnd::Pcnd() {
    set_defaults();
#ifdef cimg_use_tiff
    TIFFSetWarningHandler(NULL);
#endif
}

///////////////////////////////////////////////////////////////////////////
void
Pcnd::set_defaults() {
    normalize = true;

    denoiseMethod = DenoiseNone;
    medianRadius = 1;
    intensitySigma = 0.1;
    intensityStep = -50;
    spatialSigma = 2.0;
    spatialStep = -50;
    gaussSigma = 0.5;

    biasEstimationMethod = BiasDiffusive;
    xxOrder = 2;
    xyOrder = 2;
    yyOrder = 2;
    diffuseSigma = 50;
    diffuseIt = 0;
    divide = false;

    modality = ModalityDic;
    blockSize = 0;
    scale = 1.0;
    smoothness = 0.5;
    sparsity = 0.001;
    alpha = 0.2;
    tol = 1e-6;
    maxIt = 10000;
    haloAmount = 1.0;
    haloContrast = 0.1;
    haloSigmaRatio = 1.25;
    haloSigma = 2.0;
    softness = 1.0;
    angle = 45;
    invert = false;
}

///////////////////////////////////////////////////////////////////////////
void
Pcnd::set_thread_count(int count) {
    cout << "Setting maximum thread count to " << count << "...\n";
    pcndSetThreadCount(count);
}

///////////////////////////////////////////////////////////////////////////
void
Pcnd::run(const char* infile, const std::string& outfile) {
    assert(NULL != infile);

    try {
        cout << "Reading '" << infile << "'..." << endl;

        CImg<double> img(infile);

        if (!img.is_empty()) {

            string path;
            CImg<float> imgOut, imgFlat;

            cout << "Processing '" << infile << "'..." << endl;
            process_image(img, imgFlat, imgOut);

            if (!outfile.empty()) path.assign(outfile);
            else {
                path.assign(infile);
                string::size_type pos = path.find_last_of('.');
                if (pos != string::npos)
                    path.erase(pos);
                path.append(".preprocessed.tif");
            }

            cout << "Saving '" << path << "'..." << endl;
            imgOut.save(path.c_str());

            /*
            if (!imgFlat.is_empty()) {
                string::size_type pos = path.find_last_of('.');
                if (pos != string::npos)
                    path.erase(pos);
                path.append(".flat.tif");
                imgFlat.save(path.c_str());
            }
            */

            cout << "Done." << endl;
        }
    }
    catch (CImgException& e) {
        throw std::runtime_error(e.message);
    }
}

///////////////////////////////////////////////////////////////////////////
bool
Pcnd::process_image(
    const CImg<float>& img,
    CImg<float>&       imgFlat,
    CImg<float>&       imgOut)
{
    Timer timer;

    CImg<float> imgIn(img);

    unsigned int origWidth = img.width;
    unsigned int origHeight = img.height;

    if (scale != 1.0f && scale > 0.0f) {
        if (scale == 0.5f)
            imgIn.resize_halfXY();
        else {
            int percent = -(int)(scale * 100 + 0.5);
            // Resize using cubic interpolation.
            imgIn.resize(percent, percent, -100, -100, 5, -1);
        }
    }

    // Step 1: denoising
    imgIn.normalize(0.0f, 1.0f);

    if (denoiseMethod == DenoiseGaussian) {
        cout << "Performing Gaussian filtering...\n";
        imgIn.blur((float)gaussSigma);
    }
    else if (denoiseMethod == DenoiseBilateral) {
        /*
          CImg< T >& blur_bilateral(const float sigmax,
                                    const float sigmay,
                                    const float sigmaz,
                                    const float sigmar,
                                    const int   bgridx,
                                    const int   bgridy,
                                    const int   bgridz,
                                    const int   bgridr,
                                    const bool  interpolation_type = true) */
        cout << "Performing Bilateral filtering...\n";
        imgIn.blur_bilateral(
            (float)spatialSigma, (float)spatialSigma, (float)spatialSigma, (float)intensitySigma,
            spatialStep, spatialStep, spatialStep, intensityStep, true);
    }
    else if (denoiseMethod == DenoiseMedian) {
        cout << "Performing median filtering...\n";
        int medianWidth = (medianRadius <= 1) ?
            3 : medianRadius * 2 + 1;
        imgIn.blur_median(medianWidth);
    }

    imgOut.assign(imgIn.width, imgIn.height);

    // Step 2: flat-field correction
    if (biasEstimationMethod != BiasNone) {
        PCNDRESULT ret = PCND_OK;
        if (biasEstimationMethod == BiasDiffusive) {
            cout << "Performing diffusive flattening...\n";
            if (diffuseIt <= 0)
                imgFlat = imgIn.get_blur((float)diffuseSigma);
            else {
                imgFlat.assign(imgIn.width, imgIn.height);
                ret = pcndSmooth(
                    imgFlat.data, imgIn.data, imgIn.width, imgIn.height,
                    diffuseIt, (float)diffuseSigma);
            }
        }
        else if (biasEstimationMethod == BiasPolynomial) {
            cout << "Performing polynomial flattening...\n";
            imgFlat.assign(imgIn.width, imgIn.height);
            ret = pcndFitSurface(
                imgFlat.data, imgIn.data, imgIn.width, imgIn.height,
                xxOrder, yyOrder, xyOrder);
        }
        if (ret != PCND_OK) {
            cerr << "Flattening failed with code " << ret << ".\n";
            return false;
        }
        if (divide) {
            cimg_forXY(imgOut, i, j) {
                float denom = imgFlat(i, j);
                if (denom != 0.0f)
                    imgIn(i, j) = imgIn(i, j) / denom - 1.0f;
                else
                    imgIn(i, j) = 0.0f;
            }
        }
        else
            imgIn -= imgFlat;
        imgFlat = imgIn;
    }
    else {
        float maxval = 0.0f;
        float minval = imgIn.minmax(maxval);
        if (minval >= 0.0f)
            imgIn -= imgIn.mean();
    }

    // Step 3: preconditioning
    if (modality == ModalityDic) {
        cout << "Starting DIC preconditioning...\n";
        timer.restart();
        PCNDRESULT ret = pcndPreconditionDicBlocked(
            imgOut.data, imgIn.data, imgIn.width, imgIn.height,
            blockSize, blockSize, 0, false, invert, maxIt,
            (float)tol, (float)scale, (float)alpha,
            (float)sparsity, (float)smoothness,
            (float)softness, (float)angle,
            0, NULL);
        cout << "Preconditioning finished in " << timer.elapsed() << " seconds.\n";
        if (ret != PCND_OK) {
            char errorMessage[1024];
            pcndGetErrorString(ret, errorMessage, 1024);
            cerr << "Preconditioning failed with code " << ret << ": " << errorMessage << ".\n";
            return false;
        }
    }
    else if (modality == ModalityPhase) {
        cout << "Starting phase contrast preconditioning...\n";
        float unused = 0.0f;
        float saturation = imgIn.maxmin(unused);
        PCNDRESULT ret = pcndPreconditionPhaseBlocked(
            imgOut.data, imgIn.data,
            imgIn.width, imgIn.height, blockSize, blockSize, 0,
            false, invert, maxIt, (float)tol, (float)scale, (float)alpha, (float)sparsity, (float)smoothness,
            (float)haloAmount, (float)haloContrast, (float)haloSigma, (float)haloSigmaRatio,
            saturation, 0, NULL);
        cout << "Preconditioning finished in " << timer.elapsed() << " seconds.\n";
        if (ret != PCND_OK) {
            char errorMessage[1024];
            pcndGetErrorString(ret, errorMessage, 1024);
            cerr << "Preconditioning failed with code " << ret << ": " << errorMessage << ".\n";
            return false;
        }
    }
    else {
        imgOut = imgIn;
    }

    if (scale != 1.0f && scale > 0.0f) {
        if (scale == 0.5f)
            imgOut.resize_doubleXY();
        else {
            // Resize using cubic interpolation.
            imgOut.resize((int)origWidth, (int)origHeight, -100, -100, 5, -1);
        }
    }

    if (normalize)
        imgOut.normalize(0.f, 1.f);

    return true;
}
