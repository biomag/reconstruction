#include <iostream>
#include "cmdline.h"
#include "pcnd.h"

using namespace std;

void
usage_exit(const char* app_name) {
    cerr << "Usage: " << app_name << " [options] <files>" << endl;
    cerr << endl;

    cerr << "Options:" << endl;

    cerr << " -h --help                        Display this message"                            << endl;
    cerr << " -o --output=             string  Output path (default: *.preprocessed.tif)"       << endl;
    cerr << "    --nthreads=           int     Maximum number of threads (default: automatic)"  << endl;
    cerr << " -n --no-normalize                Do not normalize output image"                   << endl;
    cerr << endl;

    cerr << " -D --denoise-method=     int     Denoising method (default: 0)"                   << endl;
    cerr << "      0 : none"                                                                    << endl;
    cerr << "      1 : median filter"                                                           << endl;
    cerr << "      2 : bilateral filter"                                                        << endl;
    cerr << "      3 : Gaussian blur"                                                           << endl;
    cerr << "    --median-radius=      int     Median filter radius (default: 1)"               << endl;
    cerr << "    --intensity-sigma=    double  Bilateral filter intensity sigma (default: 0.1)" << endl;
    cerr << "    --intensity-step=     int     Bilateral filter intensity step (default: -50)"  << endl;
    cerr << "    --spatial-sigma=      double  Bilateral filter spatial sigma (default: 2.0)"   << endl;
    cerr << "    --spatial-step=       int     Bilateral filter spatial step (default: -50)"    << endl;
    cerr << "    --gauss-sigma=        double  Gaussian blur sigma (default: 0.5)"              << endl;
    cerr << endl;

    cerr << " -F --flatten-method=     int     Flattening method (default: 2)"                  << endl;
    cerr << "      0 : none"                                                                    << endl;
    cerr << "      1 : polynomial fit"                                                          << endl;
    cerr << "      2 : diffusion"                                                               << endl;
    cerr << "    --xx-order=           int     Polynomial x -term order (default: 2)"           << endl;
    cerr << "    --yy-order=           int     Polynomial y -term order (default: 2)"           << endl;
    cerr << "    --xy-order=           int     Polynomial xy-term order (default: 2)"           << endl;
    cerr << "    --diffuse-sigma=      double  Diffuse sigma (default: 50.0)"                   << endl;
    cerr << "    --diffuse-it=         int     Diffuse iterations (default: 0)"                 << endl;
    cerr << "    --divide                      Divide rather than subtract flat field"          << endl;
    cerr << endl;

    cerr << " -i --modality=           int     Imaging modality (default: 0)"                   << endl;
    cerr << "      1 : differential interference contrast (DIC)"                                << endl;
    cerr << "      2 : phase contrast"                                                          << endl;
    cerr << "    --scale=              double  Image scaling factor (default: 1.0)"             << endl;
    cerr << " -r --smoothness=         double  Smoothness coefficient (default: 2.0)"           << endl;
    cerr << " -p --sparsity=           double  Sparseness coefficient (default; 0.001)"         << endl;
    cerr << " -l --alpha=              double  Sparseness reweighting factor (default: 0.2)"    << endl;
    cerr << " -t --tol=                double  Stopping error tolerance (default: -1)"          << endl;
    cerr << " -m --max-it=             int     Maximum iterations (default: 10000)"             << endl;
    cerr << " -I --invert                      Invert imaging model"                            << endl;
    cerr << endl;

    cerr << "DIC Specific Options:"                                                             << endl;
    cerr << " -a --angle=              double  Shear angle (in degrees, default: 45)"           << endl;
    cerr << " -s --softness=           double  Derivative softness (default: 1.0)"              << endl;
    cerr << endl;

    cerr << "Phase Contrast Specific Options:"                                                  << endl;
    cerr << " -A --halo-amount=        double  Halo amount (default: 1.0)"                      << endl;
    cerr << " -C --halo-contrast=      double  Halo contrast ratio (default: 0.1)"              << endl;
    cerr << " -R --halo-sigma-ratio=   double  Halo standard deviation ratio (default: 1.25)"   << endl;
    cerr << " -S --halo-sigma=         double  Halo standard deviation (default: 2.0)"          << endl;
    cerr << endl;

    cerr << endl;
    exit(1);
}


int
main(int argc, char** argv) {

    Pcnd    pcnd;
    string  outfile;
    CmdLine cmdline(argc, argv);

    if (argc < 2)
        usage_exit(cmdline.app_name());

    try {
        while (cmdline.next()) {
            if (cmdline.is_option()) {
                if (cmdline.match("o", "output", true))
                    outfile.assign(cmdline.value());
                else if (cmdline.match("D", "denoise-method", true))
                    pcnd.denoiseMethod = (Pcnd::DenoiseMethod)cmdline.value_as_long(0, 3);
                else if (cmdline.match(NULL, "nthreads", true))
                    pcnd.set_thread_count(cmdline.value_as_long(1));
                else if (cmdline.match("n", "no-normalize", false))
                    pcnd.normalize = false;
                else if (cmdline.match(NULL, "median-radius", true)) pcnd.medianRadius = cmdline.value_as_long(1);
                else if (cmdline.match(NULL, "intensity-sigma", true)) pcnd.intensitySigma = cmdline.value_as_double();
                else if (cmdline.match(NULL, "intensity-step", true)) pcnd.intensityStep = cmdline.value_as_long();
                else if (cmdline.match(NULL, "spatial-sigma", true)) pcnd.spatialSigma = cmdline.value_as_double();
                else if (cmdline.match(NULL, "spatial-step", true)) pcnd.spatialStep = cmdline.value_as_long();
                else if (cmdline.match(NULL, "gauss-sigma", true)) pcnd.gaussSigma = cmdline.value_as_double();
                else if (cmdline.match("F", "flatten-method", true))
                    pcnd.biasEstimationMethod = (Pcnd::BiasEstimationMethod)cmdline.value_as_long(0, 2);
                else if (cmdline.match(NULL, "diffuse-sigma", true)) pcnd.diffuseSigma = cmdline.value_as_double();
                else if (cmdline.match(NULL, "diffuse-it", true)) pcnd.diffuseIt = cmdline.value_as_long();
                else if (cmdline.match(NULL, "xx-order", true)) pcnd.xxOrder = cmdline.value_as_long();
                else if (cmdline.match(NULL, "xy-order", true)) pcnd.xyOrder = cmdline.value_as_long();
                else if (cmdline.match(NULL, "yy-order", true)) pcnd.yyOrder = cmdline.value_as_long();
                else if (cmdline.match(NULL, "divide", false))
                    pcnd.divide = true;
                else if (cmdline.match("i", "modality", true))
                    pcnd.modality = (Pcnd::Modality)cmdline.value_as_long(0, 2);
                else if (cmdline.match(NULL, "scale", true)) pcnd.scale = cmdline.value_as_double(1e-3);
                else if (cmdline.match("r", "smoothness", true)) pcnd.smoothness = cmdline.value_as_double(1e-3);
                else if (cmdline.match("p", "sparsity", true)) pcnd.sparsity = cmdline.value_as_double(0);
                else if (cmdline.match("l", "alpha", true)) pcnd.alpha = cmdline.value_as_double();
                else if (cmdline.match("t", "tol", true)) pcnd.tol = cmdline.value_as_double();
                else if (cmdline.match("m", "max-it", true)) pcnd.maxIt = cmdline.value_as_long(1);
                else if (cmdline.match("I", "invert", false))
                    pcnd.invert = true;
                else if (cmdline.match("a", "angle", true)) pcnd.angle = cmdline.value_as_double();
                else if (cmdline.match("s", "softness", true)) pcnd.softness = cmdline.value_as_double();
                else if (cmdline.match("A", "halo-amount", true)) pcnd.haloAmount = cmdline.value_as_double(0);
                else if (cmdline.match("C", "halo-contrast", true)) pcnd.haloContrast = cmdline.value_as_double(0);
                else if (cmdline.match("R", "halo-sigma-ratio", true)) pcnd.haloSigmaRatio = cmdline.value_as_double(0);
                else if (cmdline.match("S", "halo-sigma", true)) pcnd.haloSigma = cmdline.value_as_double(0);
                else
                    usage_exit(cmdline.app_name());
            }
            else {
                const char* infile = cmdline.argument();
                if (NULL != infile)
                    pcnd.run(infile, outfile);
                outfile.clear();
            }
        } // while
    }
    catch (std::exception& e) {
        cerr << e.what() << endl;
        return -1;
    }

    return 0;
}
