#ifndef ___PCND_H___
#   define ___PCND_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <cassert>
#   include <iostream>
#   include <stdexcept>
#   include <string>

#   define cimg_debug   0     // disable modal window in CImg exceptions
#   define cimg_display 0     // disable display capabilities
#   define cimg_use_png
#   define cimg_use_tiff
#   include <MyCImg.h>

using namespace cimg_library;
using namespace std;

class Pcnd {
public:

    enum DenoiseMethod {
        DenoiseNone             = 0,
        DenoiseMedian           = 1,
        DenoiseBilateral        = 2,
        DenoiseGaussian         = 3
    };

    enum BiasEstimationMethod {
        BiasNone                = 0,
        BiasPolynomial          = 1,
        BiasDiffusive           = 2
    };

    enum BiasRemovalMethod {
        BiasAdditive            = 0,
        BiasMultiplicative      = 1,
        BiasLogarithmicAdditive = 2
    };

    enum Modality {
        ModalityNone            = 0,
        ModalityDic             = 1,
        ModalityPhase           = 2,
        ModalityCustomPsf       = 3
    };

    Pcnd();

    void set_defaults();
    void set_thread_count(int count);
    void run(const char*   infile,
             const string& outfile);

protected:
    bool process_image(const CImg<float>& img,
                       CImg<float>&       imgFlat,
                       CImg<float>&       imgOut);

public:

    bool            normalize;

    // correction parameters
    DenoiseMethod   denoiseMethod;
    BiasEstimationMethod   biasEstimationMethod;
    double          gaussSigma, intensitySigma, spatialSigma, diffuseSigma;
    int             medianRadius, intensityStep, spatialStep, diffuseIt, xxOrder, xyOrder, yyOrder;
    bool            divide;

    // preconditioning parameters
    Modality        modality;
    bool            invert;
    int             blockSize, maxIt;
    double          scale, alpha, sparsity, smoothness, tol,
                    haloAmount, haloContrast, haloSigmaRatio, haloSigma,
                    softness, angle;
};

#endif //___PCND_H___
