#ifndef ___TIMER_H___
#   define ___TIMER_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   if defined(_WIN32)
#       include <windows.h>
#   else
#       include <ctime>
#   endif


#   if defined(_WIN32)

///////////////////////////////////////////////////////////////////////////
///  @class Timer
///  @brief Simple timer class for measuring elapsed time.
///
///  Under Windows, this class uses the Win32 high-resolution timing APIs:
///  QueryPerformanceFrequency() and QueryPerformanceCounter(). Under the
///  other platforms, this class use the C Standard Library clock()
///  function.
///////////////////////////////////////////////////////////////////////////
class Timer
{
public:

    ///////////////////////////////////////////////////////////////////////
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    Timer() {
        LARGE_INTEGER cfreq;

        // Set the clock frequency if it is not yet set.
        QueryPerformanceFrequency(&cfreq);
        _cfreq = (double)cfreq.QuadPart;
        
        // Get the current value of the high-res performance counter.
        QueryPerformanceCounter(&_start);
    }
    
    ///////////////////////////////////////////////////////////////////////
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    inline void
    restart() {
        // Get the current value of the high-res performance counter.
        QueryPerformanceCounter(&_start);
    }
  
    ///////////////////////////////////////////////////////////////////////
    ///  Returns elapsed time since the timer is created or restarted.
    ///
    ///  @return (double) Elapsed time in seconds.
    ///
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    inline double
    elapsed() const {
        LARGE_INTEGER now;
        QueryPerformanceCounter(&now);
        return double(now.QuadPart - _start.QuadPart) / _cfreq;
    }


private:
    
    // Frequency setting is based on the hardware clock that does not
    // change between calling, so set this one only once.
    DOUBLE          _cfreq;

    // The starting time stored as a LARGE_INTEGER.
    LARGE_INTEGER   _start;

};

#   else // !_WIN32

///////////////////////////////////////////////////////////////////////////
///  @class Timer
///  @brief Simple timer class for measuring elapsed time.
///
///  Under Windows, this class uses the Win32 high-resolution timing APIs:
///  QueryPerformanceFrequency() and QueryPerformanceCounter(). Under the
///  other platforms, this class use the C Standard Library clock()
///  function.
///////////////////////////////////////////////////////////////////////////
class Timer
{
public:

    ///////////////////////////////////////////////////////////////////////
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    Timer()                    { _start = clock(); }
    
    ///////////////////////////////////////////////////////////////////////
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    inline void restart()      { _start = clock(); }
  
    ///////////////////////////////////////////////////////////////////////
    ///  Returns elapsed time since the timer is created or restarted.
    ///
    ///  @return (double) Elapsed time in seconds.
    ///
    ///  @post elapsed()==0
    ///////////////////////////////////////////////////////////////////////
    inline double
    elapsed() const {
        return double(clock() - _start) / CLOCKS_PER_SEC;
    }

private:

    // The starting time stored as a clock_t.
    clock_t _start;
};

#   endif // _WIN32


#endif
