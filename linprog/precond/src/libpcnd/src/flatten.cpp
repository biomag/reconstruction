/*
 ==========================================================================
 |
 |  $Id: flatten.cpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#include <libpcnd.h>
#include "flatten_cpu.hpp"
#ifdef _OPENMP
#   include <omp.h>
#endif

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndFitSurface(
    float* out,
    const float* in, int w, int h,
    int xxOrder, int yyOrder, int xyOrder
    )
{

#ifdef PCND_VERBOSE
    cout << endl << "Flattening image..." << endl;
#endif

    try {
        CImg<double> P;
        int n = _pcndFlattenInitialize(P, w, h, xxOrder, yyOrder, xyOrder);
        CImg<double> A(n, n), x(n, 1); // temporary matrices
        CImg<float>  I(in, w, h, 1, 1, true);
        CImg<float>  S(out, w, h, 1, 1, true);
        double* work = (double*)pcndAlloc(sizeof(double) * w * h);

        if (NULL == work)
            return PCND_E_OUTOFMEMORY;

        _pcndMatrixTransposeTimesMatrix(A, P);
        _pcndMatrixTransposeTimesVector(x, P, I, work);
        if (!_pcndSolve(x, A, x))
            return PCND_E_NUMERICAL;
        _pcndMatrixTimesVector(S, P, x, work);

        pcndFree(work);

#ifdef PCND_VERBOSE
        cout << scientific << showpos << "Surface coefficients:" << endl;
        int i;
        for (i = 0; i + 5 < n; i += 5) {
            cout << x[i    ] << ' ';
            cout << x[i + 1] << ' ';
            cout << x[i + 2] << ' ';
            cout << x[i + 3] << ' ';
            cout << x[i + 4] << endl;
        }
        for ( ; i < n; ++i)
            cout << x[i] << ' ';
        cout << endl;
        cout.unsetf(ios_base::scientific | ios_base::showpos);
#endif

    }
    catch (...) {
        return PCND_E_FAILED;
    }

    return PCND_OK;
}
