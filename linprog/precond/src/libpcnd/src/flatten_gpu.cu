#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <cutil.h>
#include <libpcnd.h>

/************************************************************************/
/* Init CUDA                                                            */
/************************************************************************/
#if __DEVICE_EMULATION__

bool initCUDA(void) { return true; }

#else

bool
initCUDA(void) {
	int count = 0;

	cudaGetDeviceCount(&count);
	if (count == 0)
		return false;

	for (int i = 0; i < count; ++i) {
		cudaDeviceProp prop;
		if (cudaGetDeviceProperties(&prop, i) == cudaSuccess) {
			if (prop.major >= 1) {
				cudaSetDevice(i);
				return true;
			}
		}
	}

	return false;
}

#endif /* __DEVICE_EMULATION__ */

/************************************************************************/
/* Computation Code                                                     */
/************************************************************************/

__global__ void
initDesignMatrix(
    float* P, int m, int n, int w, int h,
    int xxOrder, int yyOrder, int xyOrder
    )
{
	int row = blockIdx.x * blockDim.x + threadIdx.x;

	if (row < m) {
	    int i, j, k = 0;

		float xscale = 1.0f / (float)w;
		float yscale = 1.0f / (float)h;

		float dx = ((row % w) + 1) * xscale - 0.5f;
		float dy = ((row / w) + 1) * yscale - 0.5f;

		// constant
		P[row * n + k] = 1.0f;
		++k;

		P[row * n + k] = dx;
		++k;
		for (i = 2; i <= xxOrder; ++i) {
			P[row * n + k] = dx * P[row * n + k - 1]; // xx terms
			++k;
		}

		P[row * n + k] = dy;
		++k;
		for (i = 2; i <= yyOrder; ++i) {
			P[row * n + k] = dy * P[row * n + k - 1]; // yy terms
			++k;
		}

		// xy terms
		float px = 1.0f;
		for (i = 1; i <= xyOrder; ++i) {
			px *= dx;
			float py = 1.0f;
			for (j = 1; i + j <= xyOrder; ++j) {
				py *= dy;
				P[row * n + k] = px * py;
				++k;
			} // j
		} // i
	}
}


__global__ void
cholsol(float* A, float* x, int n) {
	int i, j, k;
    for (i = 0; i < n; ++i) {
        for (j = i; j < n; ++j)
            for (k = 0; k < i; ++k)
                A[i * n + j] -= A[k * n + i] * A[k * n + j];
        A[i * n + i] = sqrtf(A[i * n + i]);
        for (j = i + 1; j < n; ++j) A[i * n + j] /= A[i * n + i];
    }
    for (i = 0; i < n; ++i) {
		float s;
        for (s = x[i], k = i - 1; k >= 0; --k)
            s -= A[k * n + i] * x[k];
        x[i] = s / A[i * n + i];
    }
    for (i = n - 1; i >= 0; --i) {
		float s;
        for (s = x[i], k = i + 1; k <  n; ++k)
            s -= A[i * n + k] * x[k];
        x[i] = s / A[i * n + i];
    }
}


PCNDRESULT PCNDAPI
pcndInitGpu() {
#ifdef _WIN64
	if (!initCUDA())
		return PCND_E_DEVICE;
	if (CUBLAS_STATUS_SUCCESS != cublasInit())
		return PCND_E_DEVICE;
	return PCND_OK;
#else
	return PCND_E_NOTIMPL;
#endif
}


PCNDRESULT PCNDAPI
pcndShutdownGpu() {
#ifdef _WIN64
	if (CUBLAS_STATUS_SUCCESS != cublasShutdown())
		return PCND_E_DEVICE;
	return PCND_OK;
#else
	return PCND_E_NOTIMPL;
#endif
}


/************************************************************************/
/* Warning: These are terrible GPU implementations. DO NOT USE.         */
/************************************************************************/

PCNDRESULT PCNDAPI
pcndFitSurfaceGpu(
	float* out,
	const float* in, int w, int h,
	int xxOrder, int yyOrder, int xyOrder
	)
{
#ifdef _WIN64
	cublasStatus status;

	int m = w * h;
	int n = xxOrder + yyOrder +
		xyOrder * (xyOrder - 1) / 2 + 1;

	float* I = NULL;
	float* O = NULL;
	float* P = NULL;
	float* A = NULL;
	float* x = NULL;

	status = cublasAlloc(m,     sizeof(float), (void **)&I);
	if (CUBLAS_STATUS_SUCCESS != status)
		return PCND_E_OUTOFMEMORY;

	status = cublasAlloc(m,     sizeof(float), (void **)&O);
	if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		return PCND_E_OUTOFMEMORY;
	}

	status = cublasAlloc(m * n, sizeof(float), (void **)&P);
	if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		return PCND_E_OUTOFMEMORY;
	}

	status = cublasAlloc(n * n, sizeof(float), (void **)&A);
	if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		return PCND_E_OUTOFMEMORY;
	}

	status = cublasAlloc(n,     sizeof(float), (void **)&x);
	if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		cublasFree(A);
		return PCND_E_OUTOFMEMORY;
	}

	initDesignMatrix<<<m / 256 + 1, 256>>>(P, m, n, w, h, xxOrder, yyOrder, xyOrder);

	if (cudaSuccess != cudaGetLastError()) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		cublasFree(A);
		cublasFree(x);
		return PCND_E_NUMERICAL;
	}

	cublasSgemm('N', 'T', n, n, m, 1.0f, P, n, P, n, 0.0f, A, n);
	status = cublasGetError();
    if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		cublasFree(A);
		cublasFree(x);
		return PCND_E_NUMERICAL;
	}

	cudaMemcpy(I, in, m * sizeof(float), cudaMemcpyHostToDevice);

	cublasSgemv('N', n, m, 1.0f, P, n, I, 1, 0.0f, x, 1);
	status = cublasGetError();
    if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		cublasFree(A);
		cublasFree(x);
		return PCND_E_NUMERICAL;
	}

	cholsol<<<1, 1>>>(A, x, n);

	cublasSgemv('T', n, m, 1.0f, P, n, x, 1, 0.0f, O, 1);
	status = cublasGetError();
    if (CUBLAS_STATUS_SUCCESS != status) {
		cublasFree(I);
		cublasFree(O);
		cublasFree(P);
		cublasFree(A);
		cublasFree(x);
		return PCND_E_NUMERICAL;
	}

	status = cublasGetVector(m, sizeof(float), O, 1, out, 1);

	cublasFree(I);
	cublasFree(O);
	cublasFree(P);
	cublasFree(A);
	cublasFree(x);

	return (status == CUBLAS_STATUS_SUCCESS) ?
		PCND_OK : PCND_E_FAILED;
#else
	return PCND_E_NOTIMPL;
#endif
}
