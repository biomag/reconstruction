#ifndef ___SPARSE_HPP___
#   define ___SPARSE_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "vector.hpp"
#   include <algorithm>
#   include <stdexcept>

template <typename _Type>
class SparseMatrixT {

public:

    typedef _Type             value_type;
    typedef value_type&       reference;
    typedef value_type const& const_reference;
    typedef value_type*       pointer;
    typedef value_type const* const_pointer;
    typedef size_t            size_type;
    typedef ptrdiff_t         difference_type;

    SparseMatrixT():
        _m(0), _n(0) {
    }

    SparseMatrixT(size_type m, size_type n, size_type nzmax):
        _m(0), _n(0) {
        create(m, n, nzmax);
    }

    SparseMatrixT(size_type m, size_type n, size_type nzmax, 
                  const size_type* ir, const size_type* jc, const value_type* pr):
        _m(0), _n(0) {
        create(m, n, nzmax, ir, jc, pr);
    }

    SparseMatrixT(const SparseMatrixT& that):
        _m(that._m), _n(that._n), _jc(that._jc), _ir(that._ir), _pr(that._pr) {
    }

    template <typename _Type2>
    SparseMatrixT(const SparseMatrixT<_Type2>& that):
        _m(that.m()), _n(that.n()),
        _jc(that.jc()), _ir(that.ir()), _pr(that.pr()) {
    }

    ~SparseMatrixT() {
    }

    inline SparseMatrixT&
    operator=(const SparseMatrixT& that) {
        _m = that._m; _n = that._n;
        _jc = that._jc; _ir = that._ir; _pr = that._pr;
        return *this;
    }

    template <typename _Type2>
    inline SparseMatrixT&
    operator=(const SparseMatrixT<_Type2>& that) {
        _m = that.m(); _n = that.n();
        _jc = that.jc(); _ir = that.ir(); _pr = that.pr();
        return *this;
    }

    void
    create(size_type         m,
           size_type         n,
           size_type         nzmax, 
           const size_type*  ir = NULL, 
           const size_type*  jc = NULL,
           const value_type* pr = NULL)
    {
        if (m == _m && n == _n && nzmax == _pr.size()) return;
        if (m == 0 || n == 0 || nzmax == 0)
            release();
        else {
            size_type i;
            if (nzmax > m * n)
                nzmax = m * n;
            _pr.resize(nzmax);
            _ir.resize(nzmax);
            _jc.resize(n + 1);
            if (!jc) _jc.fill(0);
            else {
                for (i = 0; i + 8 <= n;    i += 8) {
                    _jc[i    ] = jc[i    ];
                    _jc[i + 1] = jc[i + 1];
                    _jc[i + 2] = jc[i + 2];
                    _jc[i + 3] = jc[i + 3];
                    _jc[i + 4] = jc[i + 4];
                    _jc[i + 5] = jc[i + 5];
                    _jc[i + 6] = jc[i + 6];
                    _jc[i + 7] = jc[i + 7];
                }
                for ( ; i <= n; ++i)
                    _jc[i] = jc[i];
            }
            if (ir) {
                for (i = 0; i + 8 < nzmax; i += 8) {
                    _ir[i    ] = ir[i    ];
                    _ir[i + 1] = ir[i + 1];
                    _ir[i + 2] = ir[i + 2];
                    _ir[i + 3] = ir[i + 3];
                    _ir[i + 4] = ir[i + 4];
                    _ir[i + 5] = ir[i + 5];
                    _ir[i + 6] = ir[i + 6];
                    _ir[i + 7] = ir[i + 7];
                }
                for ( ; i < nzmax; ++i)
                    _ir[i] = ir[i];
            }
            if (pr) {
                for (i = 0; i + 8 < nzmax; i += 8) {
                    _pr[i    ] = pr[i    ];
                    _pr[i + 1] = pr[i + 1];
                    _pr[i + 2] = pr[i + 2];
                    _pr[i + 3] = pr[i + 3];
                    _pr[i + 4] = pr[i + 4];
                    _pr[i + 5] = pr[i + 5];
                    _pr[i + 6] = pr[i + 6];
                    _pr[i + 7] = pr[i + 7];
                }
                for ( ; i < nzmax; ++i)
                    _pr[i] = pr[i];
            }
            _m = m;
            _n = n;
        }
    }

    inline void
    release() {
        _pr.resize(0);
        _ir.resize(0);
        _jc.resize(0);
        _m = _n = 0;
    }

    inline void
    reserve(size_type nzmax) {
        if (nzmax > _pr.size()) {
            size_type mn = _m * _n;
            if (nzmax > mn) nzmax = mn;
            else if (nzmax < mn) {
                size_type sz = _pr.size() * 3 / 2 + 1;
                nzmax = sz < mn ? sz : mn;
            }
            _pr.resize(nzmax);
            _ir.resize(nzmax);
        }
    }

    void
    pack() {
        size_type nz = _jc[_n];
        if (nz != 0) {
            _pr.resize(nz);
            _ir.resize(nz);
        }
    }

    inline size_type m()     const { return _m; }
    inline size_type n()     const { return _n; }
    inline size_type nzmax() const { return _pr.size(); }

    inline size_type
    nz() const {
        return _jc[_n];
    }

    inline const VectorT<size_type>&  jc() const { return _jc; }
    inline VectorT<size_type>&        jc()       { return _jc; }
    inline const VectorT<size_type>&  ir() const { return _ir; }
    inline VectorT<size_type>&        ir()       { return _ir; }
    inline const VectorT<value_type>& pr() const { return _pr; }
    inline VectorT<value_type>        pr()       { return _pr; }

    inline size_type const&           jc(size_type j) const { return _jc[j]; }
    inline size_type&                 jc(size_type j)       { return _jc[j]; }
    inline size_type const&           ir(size_type i) const { return _ir[i]; }
    inline size_type&                 ir(size_type i)       { return _ir[i]; }
    inline const_reference            pr(size_type i) const { return _pr[i]; }
    inline reference                  pr(size_type i)       { return _pr[i]; }

    inline value_type
    operator()(size_type i, size_type j) const {
        size_type k = find_offset(i, _ir.data(), _jc[j], _jc[j + 1]);
        if (k != _jc[j + 1])
            return _pr[k];
        return value_type();
    }

    double
    norm_1() const {
        double ans = 0.0;
        for (size_type j = 0; j < _n; ++j) {
            double sum = 0.0;
            for (size_type i = _jc[j]; i < _jc[j + 1]; ++i)
                sum += std::abs(_pr[i]);
            if (sum > ans)
                ans = sum;
        }
        return ans;
    }

    template <typename _Type2>
    VectorT<value_type>
    multiply(const VectorBaseT<_Type2>& x) const {
        VectorT<value_type> y(_m, value_type());
        size_type i, j;
        for (j = 0; j < _n; ++j) {
            for (i = _jc[j]; i < _jc[j + 1]; ++i)
                y[_ir[i]] += static_cast<value_type>(_pr[i] * x[j]);
        }
        return y;
    }

    template <typename _Type2>
    void
    multiply(VectorT<value_type>& y, const VectorBaseT<_Type2>& x) const {
        y.fill(value_type());
        size_type i, j;
        for (j = 0; j < _n; ++j) {
            for (i = _jc[j]; i <_jc[j + 1]; ++i)
                y[_ir[i]] += static_cast<value_type>(_pr[i] * x[j]);
        }
    }

    template <typename _Type2>
    VectorT<value_type>
    transpose_multiply(const VectorBaseT<_Type2>& x) const {
        VectorT<value_type> y(_n, value_type());
        int i, j;
#   ifdef _OPENMP
        #pragma omp parallel private(i, j)
        {
            #pragma omp for
#   endif
        for (i = 0; i < (int)_n; ++i) {
            for (j = (int)_jc[i]; j < (int)_jc[i + 1]; ++j)
                y[i] += static_cast<value_type>(_pr[j] * x[_ir[j]]);
        }
#   ifdef _OPENMP
        }
#   endif
        return y;
    }

    template <typename _Type2>
    void
    transpose_multiply(VectorT<value_type>& y, const VectorBaseT<_Type2>& x) const {
        y.fill(value_type());
        int i, j;
#   ifdef _OPENMP
        #pragma omp parallel private(i, j)
        {
            #pragma omp for
#   endif
        for (i = 0; i < (int)_n; ++i) {
            for (j = (int)_jc[i]; j < (int)_jc[i + 1]; ++j)
                y[i] += static_cast<value_type>(_pr[j] * x[_ir[j]]);
        }
#   ifdef _OPENMP
        }
#   endif
    }

    template <typename _Type2>
    inline void
    nonneg_multiply(
        VectorT<value_type>&       y, 
        VectorT<value_type>&       z, 
        const VectorBaseT<_Type2>& x) const
    {
        int i, j;
        y.fill(value_type());
        z.fill(value_type());
        for (j = 0; j < (int)_n; ++j) {
            if (0 == x[j]) continue;
            for (i = (int)_jc[j]; i < (int)_jc[j + 1]; ++i) {
                value_type prod = static_cast<value_type>(_pr[i] * x[j]);
                if (_pr[i] > 0)
                    y[_ir[i]] += prod;
                else
                    z[_ir[i]] -= prod;
            }
        }
    }

    template <typename _Type1, typename _Type2>
    inline void
    nonneg_transpose_multiply(
        VectorT<_Type1>&           y,
        VectorT<_Type1>&           z,
        const VectorBaseT<_Type2>& x) const
    {
        int i;
#   ifdef _OPENMP
        #pragma omp parallel private(i)
        {
            #pragma omp for
#   endif
        for (i = 0; i < (int)_n; ++i) {
            y[i] = z[i] = _Type1();
            for (size_type j = _jc[i]; j < _jc[i + 1]; ++j) {
                _Type1 prod = static_cast<_Type1>(_pr[j] * x[_ir[j]]);
                if (_pr[j] > 0)
                    y[i] += prod;
                else
                    z[i] -= prod;
            }
        }
#   ifdef _OPENMP
        }
#   endif
    }

    template <typename _Type2>
    inline void
    nonneg_transpose_multiply_lower(
        VectorT<value_type>&       y, 
        VectorT<value_type>&       z, 
        const VectorBaseT<_Type2>& x) const
    {
        int i;
#   ifdef _OPENMP
        #pragma omp parallel private(i)
        {
            #pragma omp for
#   endif
        for (i = 0; i < (int)_n; ++i) {
            y[i] = z[i] = value_type();
            for (size_type j = _jc[i]; j < _jc[i + 1]; ++j) {
                value_type prod = static_cast<value_type>(_pr[j] * x[_ir[j]]);
                if (_pr[j] > 0)
                    y[i] += prod;
                else
                    z[i] -= prod;
            }
        }
#   ifdef _OPENMP
        }
#   endif
        for (i = 0; i < (int)_n; ++i) {
            for (size_type j = _jc[i]; j < _jc[i + 1]; ++j) {
                if (_ir[j] == i) continue;
                value_type prod = static_cast<value_type>(_pr[j] * x[i]);
                if (_pr[j] > 0)
                    y[_ir[j]] += prod;
                else
                    z[_ir[j]] -= prod;
            }
        }
    }

    SparseMatrixT
    transposed() const {
        size_type i, j, k;

        VectorT<size_type> counts(_m);

        SparseMatrixT at(_n, _m, _pr.size());

        counts.fill(0);

        for (i = 0; i < _n; ++i) {
            for (j = _jc[i]; j < _jc[i + 1]; ++j) {
                k = _ir[j];
                ++counts[k];
            } // j
        } // i

        for (j = 0; j < _m; ++j)
            at._jc[j + 1] = at._jc[j] + counts[j];

        counts.fill(0);

        for (i = 0; i < _n; ++i) {
            for (j = _jc[i]; j < _jc[i + 1]; ++j) {
                k = _ir[j];
                size_type index = at._jc[k] + counts[k];
                at._ir[index] = i;
                at._pr[index] = _pr[j];
                ++counts[k];
            } // j
        } // i

        return at;
    }

    SparseMatrixT
    transposed_nodiag() const {
        size_type i, j, k;

        VectorT<size_type> counts(_m);

        SparseMatrixT at(_n, _m, _pr.size());

        counts.fill(0);

        for (i = 0; i < _n; ++i) {
            for (k = _jc[i]; k < _jc[i + 1]; ++k) {
                j = _ir[k];
                if (j != i) ++counts[j];
            } // k
        } // i

        for (k = 0; k < _m; ++k)
            at._jc[k + 1] = at._jc[k] + counts[k];

        counts.fill(0);

        for (i = 0; i < _n; ++i) {
            for (k = _jc[i]; k < _jc[i + 1]; ++k) {
                j = _ir[k];
                if (j != i) {
                    size_t index = at._jc[j] + counts[j];
                    at._ir[index] = i;
                    at._pr[index] = _pr[k];
                    ++counts[j];
                }
            } // k
        } // i

        return at;
    }

    inline SparseMatrixT& transpose() { return (*this = transposed()); }
    inline SparseMatrixT& negate() { _pr.negate(); return *this; }
    inline SparseMatrixT& abs() { _pr.abs(); return *this; }

    SparseMatrixT
    multiply_transposed() const {
        size_type j, k, c, r, nz = 0;
        VectorT<size_type> tags(_m, 0);

        // First pass: count non-zeros.
        for (c = 0; c < _m; ++c) { // loop over output columns
            for (j = 0; j < _n; ++j) {
                // check if the c-th row exists
                k = find_offset(c, _ir.data(), _jc[j], _jc[j + 1]);
                if (k == _jc[j + 1]) continue;
                for (k = _jc[j]; k < _jc[j + 1]; ++k) {
                    r = _ir[k];
                    if (tags[r] <= c) {
                        tags[r] = c + 1;
                        ++nz;
                    }
                } // k
            } // j
        } // c

        SparseMatrixT b(_m, _m, nz);

        VectorT<value_type> col(_m);

        // Second pass: do multiplication.
        nz = 0;
        for (c = 0; c < _m; ++c) { // loop over output columns
            b._jc[c] = nz;
            for (j = 0; j < _n; ++j) {
                k = find_offset(c, _ir.data(), _jc[j], _jc[j + 1]);
                if (k == _jc[j + 1]) continue;
                value_type ar = _pr[k];
                for (k = _jc[j]; k < _jc[j + 1]; ++k) {
                    r = _ir[k];
                    value_type ab = ar * _pr[k];
                    if (tags[r] > _m + c) col[r] += ab; 
                    else {
                        tags[r] = _m + c + 1;
                        b._ir[nz] = r;
                        col[r] = ab;
                        ++nz;
                    }
                } // k
            } // j
            std::sort(&(b._ir[b._jc[c]]), &(b._ir[nz]));
            for (k = b._jc[c]; k < nz; ++k)
                b._pr[k] = col[b._ir[k]];
        } // c

        b._jc[_m] = nz;

        return b;
    }

    inline SparseMatrixT&
    operator+=(const SparseMatrixT& b) {

        int j;

        if (_m != b.m() || _n != b.n()) {
            throw std::invalid_argument(
                "SparseMatrixT::operator+=: Matrix dimensions must agree.");
        }

        VectorT<value_type> pr(_pr);
        VectorT<size_type>  ir(_ir);
        VectorT<size_type>  jc(_jc);

        // First pass: count non-zeros.
#   ifdef _OPENMP
        #pragma omp parallel private(j)
        {
            #pragma omp for
#   endif
        for (j = 0; j < (int)_n; ++j) {
            _jc[j + 1] = merge_count(
                0, ir.data(), jc[j], jc[j + 1],
                b._ir.data(), b._jc[j], b._jc[j + 1]);
        } // j
#   ifdef _OPENMP
        }
#   endif

        for (j = 0; j < (int)_n; ++j)
            _jc[j + 1] += _jc[j];

        // Second pass: do addition.
        _ir.resize(_jc[_n], false);
        _pr.resize(_jc[_n], false);

#   ifdef _OPENMP
        #pragma omp parallel private(j)
        {
            #pragma omp for
#   endif
        for (j = 0; j < (int)_n; ++j) {
            merge_add(
                _pr.data(), _ir.data(), _jc[j], 
                pr.data(), ir.data(), jc[j], jc[j + 1],
                b._pr.data(), b._ir.data(), b._jc[j], b._jc[j + 1]);
        } // j
#   ifdef _OPENMP
        }
#   endif
        
        return *this;
    }

    inline SparseMatrixT&
    operator-=(const SparseMatrixT& b) {

        if (_m != b.m() || _n != b.n()) {
            throw std::invalid_argument(
                "SparseMatrixT::operator-=: Matrix dimensions must agree.");
        }

        VectorT<value_type> pr(_pr);
        VectorT<size_type>  ir(_ir);

        // First pass: count non-zeros.
        size_type nz = 0;
        for (size_type j = 0; j < _n; ++j) {
            nz = merge_count(
                nz,
                ir.data(), _jc[j], _jc[j + 1],
                b._ir.data(), b._jc[j], b._jc[j + 1]);
        } // j

        // Second pass.
        _ir.resize(nz, false);
        _pr.resize(nz, false);

        nz = 0;
        for (size_type j = 0; j < _n; ++j) {
            size_type jc = nz;
            nz = merge_subtract(
                _pr.data(), _ir.data(), nz, 
                pr.data(), ir.data(), _jc[j], _jc[j + 1],
                b._pr.data(), b._ir.data(), b._jc[j], b._jc[j + 1]);
            _jc[j] = jc;
        } // j
        _jc[_n] = nz;
        
        return *this;
    }

    inline SparseMatrixT&
    operator*=(const SparseMatrixT& b) {

        if (b.m() != _n) {
            throw std::invalid_argument(
                "SparseMatrixT::operator*=: Matrix dimensions must agree.");
        }

        size_type n = b.n();

        // First pass: count non-zeros.
        VectorT<size_type> jc(_jc); // backup _jc
        _jc.resize(n + 1, false);
        _jc[0] = 0;

#   ifdef _OPENMP

        #pragma omp parallel sections
        {
            #pragma omp section
            {
                VectorT<size_type> tag(_m, 0);
                for (size_type j = 0; j < n; j += 4) {
                    size_type nz = 0;
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        size_type c = b._ir[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = _ir[k];
                            if (tag[r] <= j) {
                                tag[r] = j + 1;
                                ++nz;
                            }
                        } // k
                    } // i
                    _jc[j + 1] = nz;
                } // j
            }

            #pragma omp section
            {
                VectorT<size_type> tag(_m, 0);
                for (size_type j = 1; j < n; j += 4) {
                    size_type nz = 0;
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        size_type c = b._ir[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = _ir[k];
                            if (tag[r] <= j) {
                                tag[r] = j + 1;
                                ++nz;
                            }
                        } // k
                    } // i
                    _jc[j + 1] = nz;
                } // j
            }

            #pragma omp section
            {
                VectorT<size_type> tag(_m, 0);
                for (size_type j = 2; j < n; j += 4) {
                    size_type nz = 0;
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        size_type c = b._ir[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = _ir[k];
                            if (tag[r] <= j) {
                                tag[r] = j + 1;
                                ++nz;
                            }
                        } // k
                    } // i
                    _jc[j + 1] = nz;
                } // j
            }

            #pragma omp section
            {
                VectorT<size_type> tag(_m, 0);
                for (size_type j = 3; j < n; j += 4) {
                    size_type nz = 0;
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        size_type c = b._ir[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = _ir[k];
                            if (tag[r] <= j) {
                                tag[r] = j + 1;
                                ++nz;
                            }
                        } // k
                    } // i
                    _jc[j + 1] = nz;
                } // j
            }
        }

#   else // !defined(_OPENMP)

        VectorT<size_type> tag(_m, 0);
        for (size_type j = 0; j < n; ++j) {
            size_type nz = 0;
            for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                size_type c = b._ir[i];
                for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                    size_type r = _ir[k];
                    if (tag[r] <= j) {
                        tag[r] = j + 1;
                        ++nz;
                    }
                } // k
            } // i
            _jc[j + 1] = nz;
        } // j

#   endif

        for (size_type j = 1; j <= n; ++j)
            _jc[j] += _jc[j - 1];

        // Second pass: do multiplication.
        VectorT<size_type>  ir(_ir); // backup _ir
        VectorT<value_type> pr(_pr); // backup _pr

        _ir.resize(_jc[n], false);
        _pr.resize(_jc[n], false);

#   ifdef _OPENMP

        #pragma omp parallel sections
        {
            #pragma omp section
            {
                VectorT<value_type> col(_m);
                VectorT<size_type>  tag(_m, 0);
                for (size_type j = 0; j < n; j += 4) {
                    size_type colnz = _jc[j];
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        // Multiply a(:,i) with b(i,j) and add the result to new a(:,j).
                        size_type c = b._ir[i];
                        value_type br = b._pr[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = ir[k];
                            value_type ab = br * pr[k];
                            if (tag[r] > j + n) col[r] += ab;
                            else {
                                tag[r] = j + n + 1;
                                _ir[colnz++] = r;
                                col[r] = ab;
                            }
                        }
                    }
                    std::sort(&(_ir[_jc[j]]), &(_ir[colnz]));
                    for (size_type i = _jc[j]; i < colnz; ++i)
                        _pr[i] = col[_ir[i]];
                } // j
            }

            #pragma omp section
            {
                VectorT<value_type> col(_m);
                VectorT<size_type>  tag(_m, 0);
                for (size_type j = 1; j < n; j += 4) {
                    size_type colnz = _jc[j];
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        // Multiply a(:,i) with b(i,j) and add the result to new a(:,j).
                        size_type c = b._ir[i];
                        value_type br = b._pr[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = ir[k];
                            value_type ab = br * pr[k];
                            if (tag[r] > j + n) col[r] += ab;
                            else {
                                tag[r] = j + n + 1;
                                _ir[colnz++] = r;
                                col[r] = ab;
                            }
                        }
                    }
                    std::sort(&(_ir[_jc[j]]), &(_ir[colnz]));
                    for (size_type i = _jc[j]; i < colnz; ++i)
                        _pr[i] = col[_ir[i]];
                } // j
            }

            #pragma omp section
            {
                VectorT<value_type> col(_m);
                VectorT<size_type>  tag(_m, 0);
                for (size_type j = 2; j < n; j += 4) {
                    size_type colnz = _jc[j];
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        // Multiply a(:,i) with b(i,j) and add the result to new a(:,j).
                        size_type c = b._ir[i];
                        value_type br = b._pr[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = ir[k];
                            value_type ab = br * pr[k];
                            if (tag[r] > j + n) col[r] += ab;
                            else {
                                tag[r] = j + n + 1;
                                _ir[colnz++] = r;
                                col[r] = ab;
                            }
                        }
                    }
                    std::sort(&(_ir[_jc[j]]), &(_ir[colnz]));
                    for (size_type i = _jc[j]; i < colnz; ++i)
                        _pr[i] = col[_ir[i]];
                } // j
            }

            #pragma omp section
            {
                VectorT<value_type> col(_m);
                VectorT<size_type>  tag(_m, 0);
                for (size_type j = 3; j < n; j += 4) {
                    size_type colnz = _jc[j];
                    for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                        // Multiply a(:,i) with b(i,j) and add the result to new a(:,j).
                        size_type c = b._ir[i];
                        value_type br = b._pr[i];
                        for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                            size_type r = ir[k];
                            value_type ab = br * pr[k];
                            if (tag[r] > j + n) col[r] += ab;
                            else {
                                tag[r] = j + n + 1;
                                _ir[colnz++] = r;
                                col[r] = ab;
                            }
                        }
                    }
                    std::sort(&(_ir[_jc[j]]), &(_ir[colnz]));
                    for (size_type i = _jc[j]; i < colnz; ++i)
                        _pr[i] = col[_ir[i]];
                } // j
            }
        } // #pragma parallel sections

#   else // !defined(_OPENMP)

        VectorT<value_type> col(_m);

        for (size_type j = 0; j < n; ++j) {
            size_type nz = _jc[j];
            for (size_type i = b._jc[j]; i < b._jc[j + 1]; ++i) {
                // Multiply a(:,i) with b(i,j) and add the result to new a(:,j).
                size_type c = b._ir[i];
                value_type br = b._pr[i];
                for (size_type k = jc[c]; k < jc[c + 1]; ++k) {
                    size_type r = ir[k];
                    value_type ab = br * pr[k];
                    if (tag[r] > j + n) col[r] += ab;
                    else {
                        tag[r] = j + n + 1;
                        _ir[nz++] = r;
                        col[r] = ab;
                    }
                }
            }
            std::sort(&(_ir[_jc[j]]), &(_ir[nz]));
            for (size_type i = _jc[j]; i < nz; ++i)
                _pr[i] = col[_ir[i]];
        } // j

#   endif // _OPENMP

        _n = n;

        return *this;
    }

    inline SparseMatrixT
    operator-() const {
        return SparseMatrixT(*this).negate();
    }


protected:

    inline size_type
    find_offset(
        size_type        i,
        const size_type* ir,
        size_type        first,
        size_type        last
        ) const
    {
        if (first >= last || ir[first] > i || ir[last - 1] < i)
            return last;
        else if (first + 1 == last)
            return (i == ir[first]) ? first : last;
        else {
            difference_type l = (difference_type)first - 1;
            difference_type u = (difference_type)last;
            while (l + 1 != u) {
                difference_type m = (l + u) >> 1;
                if (ir[m] < i)
                    l = m;
                else
                    u = m;
            }
            // assert l + 1 = u && ir[l] < i && ir[u] >= i
            if (u >= (difference_type)last || ir[u] != i)
                return last;
            return (size_type)u;
        }
    }

    inline size_type
    merge_count(
        size_type         ibo,
        const size_type*  ir1,
        size_type         ib1,
        size_type         ie1,
        const size_type*  ir2,
        size_type         ib2,
        size_type         ie2
        )
    {
        while (ib1 < ie1 && ib2 < ie2) {
            if (ir1[ib1] < ir2[ib2]) {
                ++ibo;
                ++ib1;
            }
            else if (ir1[ib1] == ir2[ib2]) {
                ++ibo;
                ++ib1;
                ++ib2;
            }
            else {
                ++ibo;
                ++ib2;
            }
        } // while
        ibo += ie1 - ib1;
        ibo += ie2 - ib2;
        return ibo;
    }

    inline size_type
    merge_add(
        value_type*       pro,
        size_type*        iro,
        size_type         ibo,
        const value_type* pr1,
        const size_type*  ir1,
        size_type         ib1,
        size_type         ie1,
        const value_type* pr2,
        const size_type*  ir2,
        size_type         ib2,
        size_type         ie2
        )
    {
        while (ib1 < ie1 && ib2 < ie2) {
            if (ir1[ib1] < ir2[ib2]) {
                pro[ibo] = pr1[ib1];
                iro[ibo] = ir1[ib1];
                ++ibo;
                ++ib1;
            }
            else if (ir1[ib1] == ir2[ib2]) {
                pro[ibo] = pr1[ib1] + pr2[ib2];
                iro[ibo] = ir1[ib1];
                ++ibo;
                ++ib1;
                ++ib2;
            }
            else {
                pro[ibo] = pr2[ib2];
                iro[ibo] = ir2[ib2];
                ++ibo;
                ++ib2;
            }
        } // while
        while (ib1 < ie1) {
            pro[ibo] = pr1[ib1];
            iro[ibo] = ir1[ib1];
            ++ibo;
            ++ib1;
        }
        while (ib2 < ie2) {
            pro[ibo] = pr2[ib2];
            iro[ibo] = ir2[ib2];
            ++ibo;
            ++ib2;
        }
        return ibo;
    }

    inline size_type
    merge_subtract(
        value_type*       pro,
        size_type*        iro,
        size_type         ibo,
        const value_type* pr1,
        const size_type*  ir1,
        size_type         ib1,
        size_type         ie1,
        const value_type* pr2,
        const size_type*  ir2,
        size_type         ib2,
        size_type         ie2
        )
    {
        while (ib1 < ie1 && ib2 < ie2) {
            if (ir1[ib1] < ir2[ib2]) {
                pro[ibo] = pr1[ib1];
                iro[ibo] = ir1[ib1];
                ++ibo;
                ++ib1;
            }
            else if (ir1[ib1] == ir2[ib2]) {
                pro[ibo] = pr1[ib1] - pr2[ib2];
                iro[ibo] = ir1[ib1];
                ++ibo;
                ++ib1;
                ++ib2;
            }
            else {
                pro[ibo] = pr2[ib2];
                iro[ibo] = ir2[ib2];
                ++ibo;
                ++ib2;
            }
        } // while
        while (ib1 < ie1) {
            pro[ibo] = pr1[ib1];
            iro[ibo] = ir1[ib1];
            ++ibo;
            ++ib1;
        }
        while (ib2 < ie2) {
            pro[ibo] = pr2[ib2];
            iro[ibo] = ir2[ib2];
            ++ibo;
            ++ib2;
        }
        return ibo;
    }


private:
    size_type           _m, _n;
    VectorT<size_type>  _jc, _ir;
    VectorT<value_type> _pr;
};

///////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline SparseMatrixT<_Type>
operator+(const SparseMatrixT<_Type>& a, const SparseMatrixT<_Type>& b) {
    SparseMatrixT<_Type> c(a);
    c += b;
    return c;
}

///////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline SparseMatrixT<_Type>
operator-(const SparseMatrixT<_Type>& a, const SparseMatrixT<_Type>& b) {
    SparseMatrixT<_Type> c(a);
    c -= b;
    return c;
}

///////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline SparseMatrixT<_Type>
operator*(const SparseMatrixT<_Type>& a, const SparseMatrixT<_Type>& b) {
    SparseMatrixT<_Type> c(a);
    c *= b;
    return c;
}

///////////////////////////////////////////////////////////////////////////
/// Computes vector matrix product.
///////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
inline VectorT<_Type1>
times(const _Type2* x, SparseMatrixT<_Type1>& a) {
    typedef typename VectorT<_Type1>::size_type size_type;
    int j, n = (int)a.n();
    VectorT<_Type1> v(n);
#   ifdef _OPENMP
    #pragma omp parallel private(j)
    {
        #pragma omp for
#   endif
    for (j = 0; j < n; ++j) {
        v[j] = 0.0f;
        for (size_type k = a.jc(j); k < a.jc(j + 1); ++k)
            v[j] += x[(unsigned long)a.ir(k)] * a.pr(k);
    }
#   ifdef _OPENMP
    }
#   endif
    return v;
}

///////////////////////////////////////////////////////////////////////////
/// Computes A' * A, and returns a lower triangular matrix.
///////////////////////////////////////////////////////////////////////////
template <typename _Type>
SparseMatrixT<_Type>
mttimes_lower(
    const SparseMatrixT<_Type>& at, 
    const SparseMatrixT<_Type>& a
    )
{
    if (a.m() != at.n())
        throw std::invalid_argument("Matrix dimensions must agree.");

    typedef typename SparseMatrixT<_Type>::size_type size_type;
    size_type i, j, k, l, m, nz = 0;

    VectorT<size_type> ir(at.m(), 0);
    VectorT<_Type>     pr(at.m());

    // First pass: count non-zeros.
    for (j = 0; j < a.n(); ++j) {
        for (i = a.jc(j); i < a.jc(j + 1); ++i) {
            l = a.ir(i);
            for (k = at.jc(l); k < at.jc(l + 1); ++k) {
                m = at.ir(k);
                if (m < j) continue;
                if (ir[m] <= j) {
                    ir[m] = j + 1;
                    ++nz;
                }
            }
        }
    } // j

    // Second pass.
    SparseMatrixT<_Type> c(at.m(), a.n(), nz);
    nz = 0;
    for (j = 0; j < a.n(); ++j) {
        c.jc(j) = nz;
        for (i = a.jc(j); i < a.jc(j + 1); ++i) {
            // Multiply at(:,i) with a(i,j) and add the result to c(:,j).
            l = a.ir(i);
            _Type br = a.pr(i);
            for (k = at.jc(l); k < at.jc(l + 1); ++k) {
                m = at.ir(k);
                if (m < j) continue;
                _Type ab = br * at.pr(k);
                if (ir[m] > j + a.n()) pr[m] += ab;
                else {
                    ir[m] = j + a.n() + 1;
                    c.ir(nz++) = m;
                    pr[m] = ab;
                }
            }
        }
        std::sort(&(c.ir(c.jc(j))), &(c.ir(nz)));
        for (i = c.jc(j); i < nz; ++i)
            c.pr(i) = pr[c.ir(i)];
    } // j

    c.jc(a.n()) = nz;

    return c;
}

#endif // ___SPARSE_HPP___
