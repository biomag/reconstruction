#include "ioutils.h"
#include <stdint.h>

/* These routines do not take care of endianness, but we can change them later. */

size_t
pcndPutInt(FILE* fp, int  i) {
    int64_t i64 = (int64_t)i;
    return fwrite(&i64, sizeof(int64_t), 1, fp);
}


size_t
pcndGetInt(FILE* fp, int* i) {
    int64_t i64 = 0;
    size_t nread = fread(&i64, sizeof(int64_t), 1, fp);
    if (nread == 1)
        *i = (int)i64;
    return nread;
}


size_t
pcndPutUInt(FILE* fp, unsigned int  u) {
    uint64_t u64 = (uint64_t)u;
    return fwrite(&u64, sizeof(uint64_t), 1, fp);
}


size_t
pcndGetUInt(FILE* fp, unsigned int* u) {
    uint64_t u64 = 0;
    size_t nread = fread(&u64, sizeof(uint64_t), 1, fp);
    if (nread == 1)
        *u = (int)u64;
    return nread;
}

