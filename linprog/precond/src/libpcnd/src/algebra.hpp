/*
 ==========================================================================
 |
 |  $Id: algebra.hpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#ifndef ___ALGEBRA_HPP___
#   define ___ALGEBRA_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "config.h"

#   ifdef USE_MKL
#       include <mkl.h>
#   else
#       include "chol.hpp"
#   endif

template <typename _Type1, typename _Type2, typename _Type3>
void
_pcndMatrixTimesVector(CImg<_Type1>& y, const CImg<_Type2>& A, const CImg<_Type3>& x, double* work = NULL) {
    assert(y.size() == A.height);
    int i;
    #pragma omp parallel private(i)
    {
        #pragma omp for
        for (i = 0; i < (int)A.height; ++i) {
            _Type1 sum = 0.0f;
            for (int j = 0; j < (int)A.width; ++j)
                sum += (_Type1)A(j, i) * (_Type1)x(j);
            y(i) = sum;
        } // i
    }
}


#   ifdef USE_MKL
void
_pcndMatrixTimesVector(CImg<float>& y, const CImg<double>& A, const CImg<double>& x, double* work = NULL) {
    assert(y.size() == A.height);
    int i;
    char trans = 'T';
    double alpha = 1.0, beta = 0.0;
    MKL_INT m = (int)A.width, n = (int)A.height, one = 1;
    dgemv(&trans, &m, &n, &alpha, A.data, &m, x.data, &one, &beta, work, &one);
    for (i = 0; i + 4 < n; i += 4) {
        y[i    ] = (float)work[i    ];
        y[i + 1] = (float)work[i + 1];
        y[i + 2] = (float)work[i + 2];
        y[i + 3] = (float)work[i + 3];
    }
    for ( ; i < n; ++i) y[i] = (float)work[i];
}
#   endif


template <typename _Type1, typename _Type2, typename _Type3>
void
_pcndMatrixTransposeTimesVector(CImg<_Type1>& y, const CImg<_Type2>& A, const CImg<_Type3>& x, double* work = NULL) {
    assert(y.size() == A.width);
    int j;
    #pragma omp parallel private(j)
    {
        #pragma omp for
        for (j = 0; j < (int)A.width; ++j) {
            _Type1 sum = 0.0f;
            for (int i = 0; i < (int)A.height; ++i)
                sum += (_Type1)A(j, i) * (_Type1)x(i);
            y(j) = sum;
        } // j
    }
}


#   ifdef USE_MKL
void
_pcndMatrixTransposeTimesVector(CImg<double>& y, const CImg<double>& A, const CImg<float>& x, double* work = NULL) {
    assert(y.size() == A.width);
    int i;
    char trans = 'N';
    double alpha = 1.0, beta = 0.0;
    MKL_INT m = (int)A.width, n = (int)A.height, one = 1;
    for (i = 0; i + 4 < n; i += 4) {
        work[i    ] = (double)x[i    ];
        work[i + 1] = (double)x[i + 1];
        work[i + 2] = (double)x[i + 2];
        work[i + 3] = (double)x[i + 3];
    }
    for ( ; i < n; ++i) work[i] = (double)x[i];
    dgemv(&trans, &m, &n, &alpha, A.data, &m, work, &one, &beta, y.data, &one);
}
#   endif


template <typename _Type1, typename _Type2>
void
_pcndMatrixTransposeTimesMatrix(CImg<_Type1>& B, const CImg<_Type2>& A) {
    assert(B.width == A.width && B.width == B.height);
    int i, j, k;
    #pragma omp parallel private(i, j, k)
    {
        #pragma omp for
        for (j = 0; j < (int)A.width; ++j) {
            {
                // diagonal
                _Type1 sum = 0.0f;
                for (k = 0; k < (int)A.height; ++k)
                    sum += (_Type1)A(j, k) * (_Type1)A(j, k);
                B(j, j) = sum;
            }
            for (i = 0; i < j; ++i) {
                // non-diagonal
                _Type1 sum = 0.0f;
                for (k = 0; k < (int)A.height; ++k)
                    sum += (_Type1)A(i, k) * (_Type1)A(j, k);
                B(j, i) = B(i, j) = sum;
            } // i
        } // j
    }
}


#   ifdef USE_MKL
void
_pcndMatrixTransposeTimesMatrix(CImg<float >& B, const CImg<float >& A) {
    assert(B.width == A.width && B.width == B.height);
    float alpha = 1.0f, beta = 0.0f;
    char transa = 'N', transb = 'T';
    MKL_INT n = (MKL_INT)B.width, k = (MKL_INT)A.height;
    sgemm(&transa, &transb, &n, &n, &k, &alpha, A.data, &n, A.data, &n, &beta, B.data, &n);
}


void
_pcndMatrixTransposeTimesMatrix(CImg<double>& B, const CImg<double>& A) {
    assert(B.width == A.width && B.width == B.height);
    double alpha = 1.0, beta = 0.0;
    char transa = 'N', transb = 'T';
    MKL_INT n = (MKL_INT)B.width, k = (MKL_INT)A.height;
    dgemm(&transa, &transb, &n, &n, &k, &alpha, A.data, &n, A.data, &n, &beta, B.data, &n);
}
#   endif


inline bool
_pcndSolve(CImg<float >& x, CImg<float >& A, const CImg<float >& b) {
#   ifdef USE_MKL
    char uplo = 'L';
    MKL_INT info = 0, n = (MKL_INT)A.width, one = 1;
    scopy(&n, b.data, &one, x.data, &one);
    spotrf(&uplo, &n, A.data, &n, &info);
    if (0 == info)
        spotrs(&uplo, &n, &one, A.data, &n, x.data, &n, &info);
    return (0 == info);
#   else
    CholeskyT<CImg<float >, CImg<float > > chol(A, true, false);
    chol.solve(b, x);
    return true;
#   endif
}

inline bool
_pcndSolve(CImg<double>& x, CImg<double>& A, const CImg<double>& b) {
#   ifdef USE_MKL
    char uplo = 'L';
    MKL_INT info = 0, n = (MKL_INT)A.width, one = 1;
    dcopy(&n, b.data, &one, x.data, &one);
    dpotrf(&uplo, &n, A.data, &n, &info);
    if (0 == info)
        dpotrs(&uplo, &n, &one, A.data, &n, x.data, &n, &info);
    return (0 == info);
#   else
    CholeskyT<CImg<double>, CImg<double> > chol(A, true, false);
    chol.solve(b, x);
    return true;
#   endif
}

#endif // ___ALGEBRA_HPP___
