#include <libpcnd.h>
#include "version.h"

PCNDRESULT PCNDAPI
pcndGetVersion(
    int* major,
    int* minor,
    int* magic,
    int* build)
{
    if (major) *major = VERSION_MAJOR;
    if (minor) *minor = VERSION_MINOR;
    if (magic) *magic = VERSION_MAGIC;
    if (build) *build = VERSION_BUILD;
    return PCND_OK;
}
