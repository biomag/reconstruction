#ifndef ___MUNNQP_HPP___
#   define ___MUNNQP_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <libpcnd.h>
#   include "sparse.hpp"

#   ifdef PCND_VERBOSE
#       include <ctime>
#       include <iostream>
#       include <iomanip>
#   endif
#   include <limits>
#   include <cfloat>

inline int
_pcndIsNaN(double x) {
#   ifdef _WIN32
    return _isnan(x);
#   else
    return isnan(x);
#   endif
}

///////////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2, typename _Type3>
void
_pcndMUNNQP(
    VectorBaseT<_Type1>&         x,
    const SparseMatrixT<_Type3>& A,
    const VectorBaseT<_Type2>&   b,
    VectorT<_Type2>&             bb,
    VectorT<_Type2>&             xp,
    VectorT<_Type2>&             xm,
    bool                         init,
    int                          width,
    int                          height,
    int                          tag,
    _Type2                       scale,
    _Type2                       sparsity,
    _Type2                       alpha,
    _Type2                       tol,
    int                          maxit,
    int*                         cancel,
    bool                         verbose = false,
    PCNDCALLBACK_UPDATE          updatecb = NULL)
{
    assert(A.m() == A.n());

    int n = (int)A.n();

    if (maxit < 0)
        maxit = 100 * n;
    if (tol < 0) {
        // default tolerance
        tol = 1e3f * numeric_limits<_Type2>::epsilon()
            * (_Type2)A.norm_1() * (_Type2)n;
    }

#ifdef PCND_VERBOSE
    if (verbose) {
        cout << "Error tolerance = " << tol
            << ", maximum iterations = " << maxit << "." << endl;
    }
#endif

    if (init)
        x.fill(1.0f); // initialization
    b.get_squared(bb);

#ifdef PCND_VERBOSE
    clock_t tlast = 0, tstart = 0;
    if (verbose) {
        cout << "ITER        CHANGE          TIME" << endl
            << left << scientific << setprecision(6);
        tlast = tstart = clock();
    }
#endif

    for (int it = 0; it < maxit; ++it) {

        if (cancel && *cancel) {
#ifdef PCND_VERBOSE
            if (verbose)
                cout << "Cancelled at iteration " << it << "." << endl;
#endif
            break;
        }

        A.nonneg_transpose_multiply(xp, xm, x);

        _Type2 delta = 0.0f;

        int i;

        if (alpha < 0.0) {
#ifdef _OPENMP
        #pragma omp parallel private(i)
        {
            #pragma omp for reduction(+:delta)
#endif
            for (i = 0; i < n; ++i) {
                if (x[i] == 0.0) continue;
                _Type2 bi = b[i] + sparsity;
                _Type2 bbi = bb[i] + sparsity * (sparsity + 2.0f * b[i]);
                _Type2 y = x[i] * (sqrt(bbi + 4.0f * xp[i] * xm[i]) - bi) /
                    (2.0f * xp[i] + numeric_limits<_Type2>::epsilon());
                if (y > 1.0f)
                    y = 1.0f;
                _Type2 e = y - x[i];
                x[i] = (_Type1)y;
                delta += e * e;
            }
#ifdef _OPENMP
        }
#endif
        }
        else {
            if (alpha == 0.0)
                alpha = numeric_limits<_Type2>::epsilon();
#ifdef _OPENMP
        #pragma omp parallel private(i)
        {
            #pragma omp for reduction(+:delta)
#endif
            for (i = 0; i < n; ++i) {
                if (x[i] == 0.0) continue;
                _Type2 penalty = sparsity / (alpha + x[i]);
                _Type2 bi = b[i] + penalty;
                _Type2 bbi = bb[i] + penalty * (penalty + 2.0f * b[i]);
                _Type2 y = x[i] * (sqrt(bbi + 4.0f * xp[i] * xm[i]) - bi) /
                    (2.0f * xp[i] + numeric_limits<_Type2>::epsilon());
                if (y > 1.0f)
                    y = 1.0f;
                _Type2 e = y - x[i];
                x[i] = (_Type1)y;
                delta += e * e;
            }
#ifdef _OPENMP
        }
#endif
        }

        bool stop = delta < tol || _pcndIsNaN(delta);

#ifdef PCND_VERBOSE
        if (verbose) {
            clock_t now = clock();
            if (stop || now - tlast > CLOCKS_PER_SEC * 2) {
                cout << setw(12) << it << setw(16) << delta
                    << setw(16) << (float)(now - tstart) / CLOCKS_PER_SEC << endl;
                tlast = now;
                if (NULL != updatecb)
                    (*updatecb)(x.data(), scale, width, height, tag, it, delta);
            }
        }
#endif

        if (stop) {
#ifdef PCND_VERBOSE
            if (verbose) {
                cout << "Converged at iteration " << it
                    << " (" << delta << " < "
                    << tol << ")" << endl;
                if (NULL != updatecb)
                    (*updatecb)(x.data(), scale, width, height, tag, it, delta);
            }
#endif
            break;
        }
    } // it

#ifdef PCND_VERBOSE
    cout.unsetf(ios_base::left | ios_base::scientific);
#endif

}

///////////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2, typename _Type3>
void
_pcndMUNNQP(
    VectorBaseT<_Type1>&         x,
    const SparseMatrixT<_Type3>& A,
    const VectorBaseT<_Type2>&   b,
    bool                         init,
    int                          width,
    int                          height,
    int                          tag,
    _Type2                       scale,
    _Type2                       sparsity,
    _Type2                       alpha,
    _Type2                       tol,
    int                          maxit,
    int*                         cancel,
    bool                         verbose = false,
    PCNDCALLBACK_UPDATE          updatecb = NULL)
{
    VectorT<_Type2> xp(x.size());
    VectorT<_Type2> xm(x.size());
    VectorT<_Type2> bb(b.size());

    _pcndMUNNQP(
        x, A, b, bb, xp, xm, init, width, height, tag,
        scale, sparsity, alpha, tol, maxit, cancel, verbose, updatecb);
}

#endif // ___MUNNQP_HPP___
