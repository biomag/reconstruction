/*
 ==========================================================================
 |
 |  $Id: precnd_fft.cpp 704 2009-05-07 23:56:37Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#include <libpcnd.h>
#ifdef _WIN32
#   include <windows.h>
#endif
#include "models.hpp"

//////////////////////////////////////////////////////////////////////////
inline unsigned int
pcndNextPowerOfTwo(unsigned int n) {
    if (n == 0) n = 1;
    else {
        --n;
        n |= n >>  1;
        n |= n >>  2;
        n |= n >>  4;
        n |= n >>  8;
        n |= n >> 16;
        ++n;
    }
    return n;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionPhaseFft(
    float* out, const float* in,
    int w, int h, int tag, bool invert, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    )
{
    /*
    int radius   = _pcndGetPhasePsfRadius(haloSigma, haloSigmaRatio);
    int diameter = radius * 2 + 1;

    CImg<double> D;
    CImg<double> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetPhasePsf(
        M.data,
        diameter, radius, invert,
        haloAmount, haloContrast, haloSigma, haloSigmaRatio);
    _pcndMakeSparseMatrix(w, h, M, D, H, R);
    */

    return PCND_E_NOTIMPL;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionDicFft(
    float* out, const float* in,
    int w, int h, int tag, bool invert,
    float smoothness, float softness, float angle,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    )
{
    int radius   = _pcndGetDicPsfRadius(softness);
    int diameter = radius * 2 + 1;

    CImg<double> D;
    CImg<double> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetDicPsf(M.data, diameter, radius, invert, softness, angle);

    unsigned int n = (unsigned int)w;
    if ((unsigned int)h > n) n = (unsigned int)h;
    if ((unsigned int)diameter > n) n = (unsigned int)diameter;
    if (D.width > n) n = D.width;

    n = pcndNextPowerOfTwo(n);

    CImg<double> g(n, n, 1, 1, 0.0);
    CImg<double> t(n, n, 1, 1, 0.0);
    CImg<double> r(n, n, 1, 1, 0.0);

    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x) g(x, y) = in[y * w + x];

    for (int y = 0; y < (int)D.height; ++y)
        for (int x = 0; x < (int)D.width; ++x)
            r(x, y) = D(x, y);

    for (int y = 0; y < diameter; ++y)
        for (int x = 0; x < diameter; ++x)
            t(x, y) = M(x, y);

    CImgList<> G = g.get_FFT();
    CImgList<> T = t.get_FFT();
    CImgList<> R = r.get_FFT();

    T[0] = T[0].get_mul( T[0]) + R[0].get_mul( R[0]);
    T[1] = T[1].get_mul(-T[1]) + R[1].get_mul(-R[1]);

    //TODO: Implement this...

    return PCND_E_NOTIMPL;
}
