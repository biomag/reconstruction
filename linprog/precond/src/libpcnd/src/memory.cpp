#include <libpcnd.h>
#include <malloc.h>
#include <memory.h>
#include <stdlib.h>

PCNDAPI void*
pcndAlloc(size_t sizeInBytes) {
#ifdef _WIN32
    return _aligned_malloc(sizeInBytes, 16);
#else
    void* ptr = NULL;
    posix_memalign(&ptr, 16, sizeInBytes);
    return ptr;
#endif
}


PCNDAPI void*
pcndCalloc(size_t count, size_t elementSize) {
    size_t sizeInBytes = count * elementSize;
#ifdef _WIN32
    void* ptr = _aligned_malloc(sizeInBytes, 16);
#else
    void* ptr = NULL;
    posix_memalign(&ptr, 16, sizeInBytes);
#endif
    if (ptr)
        memset(ptr, 0, sizeInBytes);
    return ptr;
}


PCNDAPI void
pcndFree(void* ptr) {
#ifdef _WIN32
    _aligned_free(ptr);
#else
    free(ptr);
#endif
}

