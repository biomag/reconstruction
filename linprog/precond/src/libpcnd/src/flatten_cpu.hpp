/*
 ==========================================================================
 |
 |  $Id: flatten_cpu.hpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#ifndef ___FLATTEN_CPU_HPP___
#   define ___FLATTEN_CPU_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "algebra.hpp"

template <typename _Type>
int
_pcndFlattenInitialize(
    CImg<_Type>& coef, int w, int h,
    int xxOrder, int yyOrder, int xyOrder
    )
{
    int row, size = w * h;
    int xxCount = xxOrder;
    int yyCount = yyOrder;
    int xyCount = xyOrder * (xyOrder - 1) / 2;

    coef.assign(xxCount + yyCount + xyCount + 1, size);

    _Type xscale = 1.0f / (_Type)w;
    _Type yscale = 1.0f / (_Type)h;

    #pragma omp parallel private(row)
    {
        #pragma omp for
        for (row = 0; row < size; ++row) {

            int i, j, k = 0;

            ldiv_t t = ldiv(row, w);

            _Type dx = (t.rem  + 1) * xscale - 0.5f;
            _Type dy = (t.quot + 1) * yscale - 0.5f;

            // constant
            coef(k++, row) = 1.0;

            for (i = 1; i <= xxCount; ++i)
                coef(k++, row) = pow(dx, (_Type)i); // xx terms
            for (i = 1; i <= yyCount; ++i)
                coef(k++, row) = pow(dy, (_Type)i); // yy terms

            // xy terms
            for (i = 1; i <= xyOrder; ++i) {
                for (j = 1; i + j <= xyOrder; ++j) {
                    coef(k++, row) = pow(dx, (_Type)i) *
                                     pow(dy, (_Type)j);
                } // j
            } // i

        } // for
    }

    return coef.dimx();
}

#endif //___FLATTEN_CPU_HPP___
