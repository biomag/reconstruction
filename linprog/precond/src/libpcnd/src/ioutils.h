#ifndef ___IOUTILS_H___
#   define ___IOUTILS_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <stdio.h>

size_t pcndPutInt (FILE* fp, int  i);
size_t pcndGetInt (FILE* fp, int* i);

size_t pcndPutUInt(FILE* fp, unsigned int  u);
size_t pcndGetUInt(FILE* fp, unsigned int* u);

#endif // ___IOUTILS_H___

