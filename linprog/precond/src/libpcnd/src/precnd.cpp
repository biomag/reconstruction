#include "precnd.hpp"

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndSetThreadCount(int count) {
#   ifdef _OPENMP
    omp_set_num_threads(count);
    return PCND_OK;
#   else
    return PCND_E_NOTIMPL;
#   endif
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionPhase(
    float* out, const float* in,
    int w, int h, int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionPhaseT<float>(
            out, in, w, h, tag,
            invert, maxit, tol, scale, alpha, sparsity, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio,
            cancel, updatecb
        );
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionPhaseDbl(
    float* out, const float* in,
    int w, int h, int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionPhaseT<double>(
            out, in, w, h, tag,
            invert, maxit, tol, scale, alpha, sparsity, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio,
            cancel,updatecb
        );
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionPhaseBlocked(
    float* out, const float* in,
    int w, int h, int blkw, int blkh,
    int tag, bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, float saturation, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionPhaseBlockedT<float>(
            out, in, w, h, blkw, blkh, tag,
            init, invert, maxit, tol, scale, alpha, sparsity, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio,
            saturation, cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionPhaseBlockedDbl(
    float* out, const float* in,
    int w, int h, int blkw, int blkh,
    int tag, bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, float saturation, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionPhaseBlockedT<double>(
            out, in, w, h, blkw, blkh, tag,
            init, invert, maxit, tol, scale, alpha, sparsity, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio,
            saturation, cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionDic(
    float* out, const float* in, int w, int h,
    int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionDicT<float>(
            out, in, w, h, tag, invert, maxit, tol,
            scale, alpha, sparsity, smoothness,
            softness, angle, cancel,
            updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionDicDbl(
    float* out, const float* in, int w, int h,
    int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionDicT<double>(
            out, in, w, h, tag, invert, maxit, tol,
            scale, alpha, sparsity, smoothness,
            softness, angle, cancel,
            updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionDicBlocked(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness, float softness, float angle,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionDicBlockedT<float>(
            out, in, w, h, blkw, blkh,
            tag, init, invert, maxit, tol, scale, alpha,
            sparsity, smoothness, softness,
            angle, cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionDicBlockedDbl(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness, float softness, float angle,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionDicBlockedT<double>(
            out, in, w, h, blkw, blkh,
            tag, init, invert, maxit, tol, scale, alpha,
            sparsity, smoothness, softness,
            angle, cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionWithPsf(
    float* out, const float* in, int w, int h, int tag,
    const float* psf, int psfw, int psfh, bool init, bool invert,
    int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness,
    int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionWithPsfT<float>(
            out, in, w, h, tag,
            psf, psfw, psfh, init, invert,
            maxit, tol, scale, alpha,
            sparsity, smoothness,
            cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndPreconditionWithPsfDbl(
    float* out, const float* in, int w, int h, int tag,
    const float* psf, int psfw, int psfh, bool init, bool invert,
    int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness,
    int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    PCNDRESULT ret = PCND_OK;

#ifdef _WIN32
    __try {
#endif // _WIN32
        ret = pcndPreconditionWithPsfT<double>(
            out, in, w, h, tag,
            psf, psfw, psfh, init, invert,
            maxit, tol, scale, alpha,
            sparsity, smoothness,
            cancel, updatecb);
#ifdef _WIN32
    }
    __except(EXCEPTION_EXECUTE_HANDLER) {
        ret = PCND_E_ACCESSVIOLATION;
    }
#endif // _WIN32

    return ret;
}
