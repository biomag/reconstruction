/*
 ==========================================================================
 |
 |  $Id: config.h 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#ifndef ___CONFIG_H___
#   define ___CONFIG_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   ifdef HAS_MKL
#       define USE_MKL
#   endif

#   ifdef HAS_CUDA
#       define USE_CUDA
#   endif

#   define PCND_VERBOSE

#   ifdef _OPENMP
#       include <omp.h>
#   endif

#   include <cassert>
#   include <cmath>
#   include <cstdlib>
#   include <cstring>
#   include <iostream>
#   include <stdexcept>
#   include <vector>
using namespace std;

#   define cimg_debug    0   // disable modal window in CImg exceptions
#   define cimg_display  0   // disable display capabilities
#   include <MyCImg.h>
using namespace cimg_library;


#   ifndef M_PI
#       define M_PI     3.14159265358979323846264338328
#   endif

#   ifndef M_PI_2
#       define M_PI_2   1.57079632679489661923132169164
#   endif

#   ifndef M_PI_4
#       define M_PI_4   0.78539816339744830966156608458
#   endif

#   ifndef M_SQRT2
#       define M_SQRT2  1.41421356237309504880168872421
#   endif

#   if defined(_MSC_VER) && (_MSC_VER > 1200)
#       if defined(_M_X64) || (_M_IX86_FP >= 2)
#           define PCND_USE_EMMINTRIN_H
#       endif
#       if defined(_M_X64) || (_M_IX86_FP >= 1)
#           define PCND_USE_XMMINTRIN_H
#       endif
#   endif

#endif //___CONFIG_H___
