#!/usr/bin/python

import getopt, os, os.path, fnmatch, string, re, sys

def walkPath(root, patterns="*", recurse=1, return_folders=1, return_files=1):

    # expand patterns from semicolon-separated string to list
    pattern_list = patterns.split(';')
    
    # collect input and output arguments into one bunch
    class Bunch:
        def __init__(self, **kwds): self.__dict__.update(kwds)
    
    arg = Bunch(recurse=recurse, pattern_list=pattern_list,
        return_folders=return_folders, return_files=return_files,
        results=[])
    
    def visit(arg, dirname, files):
        # append to arg.results all relevant files (and perhaps folders)
        for name in files:
            fullname = os.path.normpath(os.path.join(dirname, name))
            if (arg.return_folders and os.path.isdir(fullname)) or \
                (arg.return_files and os.path.isfile(fullname)):
                for pattern in arg.pattern_list:
                    if fnmatch.fnmatch(name, pattern):
                        arg.results.append(fullname)
                        break
                        
        # block recursion if recursion was disallowed
        if not arg.recurse: files[:] = []
        
    os.path.walk(root, visit, arg)
    
    return arg.results;


def getVersion(rcpath):
    version = [0, 0, 0, 0]
    lines = file(rcpath, 'r').readlines()
    for line in lines:
        line = line.strip()
        if line.startswith('FILEVERSION'):
            line = line[len('FILEVERSION') + 1:]
            parts = line.split(',')
            version[0] = int(parts[0])
            version[1] = int(parts[1])
            version[2] = int(parts[2])
            version[3] = int(parts[3])
            break
    return version


def updateRC(rcpath, version):
    lines = file(rcpath, 'r').readlines()
    versionText = "%d,%d,%d,%d"%(version[0], version[1], version[2], version[3])
    for i in range(len(lines)):
        line = lines[i].strip()
        if line.startswith("FILEVERSION"):
            pos = lines[i].find('FILEVERSION')
            if pos >= 0:
                newline = lines[i]
                lines[i] = newline[0:pos + 11] + ' ' + versionText + '\n'
        elif line.startswith("PRODUCTVERSION"):
            pos = lines[i].find('PRODUCTVERSION')
            if pos >= 0:
                newline = lines[i]
                lines[i] = newline[0:pos + 14] + ' ' + versionText + '\n'
        elif line.startswith("VALUE"):
            pos = lines[i].find('"FileVersion"')
            if pos >= 0:
                newline = lines[i]
                lines[i] = newline[0:pos + 13] + ', "' + versionText + '"\n'
            else:
                pos = lines[i].find('"ProductVersion"')
                if pos >= 0:
                    newline = lines[i]
                    lines[i] = newline[0:pos + 16] + ', "' + versionText + '"\n'
    file(rcpath, 'w').writelines(lines)


def updateHeader(version):
    text = open('version.h.in', "r").read()
    text = re.sub("%VERSION_MAJOR%", str(version[0]), text)
    text = re.sub("%VERSION_MINOR%", str(version[1]), text)
    text = re.sub("%VERSION_MAGIC%", str(version[2]), text)
    text = re.sub("%VERSION_BUILD%", str(version[3]), text)
    f = open('version.h', "w")
    f.write(text)
    f.close()


rcpaths = walkPath('.', '*.rc', 0, 0, 1)
for rcpath in rcpaths:
    version = getVersion(rcpath)
    version[3] = version[3] + 1
    print '%s: %d.%d.%d.%d'%(rcpath, version[0], version[1], version[2], version[3])
    updateRC(rcpath, version)
    updateHeader(version)
    break
