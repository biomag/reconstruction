#ifndef ___MODELS_FFT_HPP___
#   define ___MODELS_FFT_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "models.hpp"

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndCreatePhaseModelFft(
    SparseMatrixT<_Type1>& H, SparseMatrixT<_Type1>& R,
    unsigned long w, unsigned long h, bool invert, _Type2 smoothness,
    _Type2 haloAmount, _Type2 haloContrast, _Type2 haloSigma,
    _Type2 haloSigmaRatio
    )
{
    int radius   = _pcndGetPhasePsfRadius(haloSigma, haloSigmaRatio);
    int diameter = radius * 2 + 1;

    CImg<_Type1> D;
    CImg<_Type1> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetPhasePsf(
        M.data, diameter, radius, invert,
        haloAmount, haloContrast, haloSigma, haloSigmaRatio);
    // _pcndMakeSparseMatrix(w, h, M, D, H, R);
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndCreateDicModelFft(
    SparseMatrixT<_Type1>& H, SparseMatrixT<_Type1>& R,
    unsigned long w, unsigned long h, bool invert,
    _Type2 smoothness, _Type2 softness,
    _Type2 angle
    )
{
    int radius   = _pcndGetDicPsfRadius(softness);
    int diameter = radius * 2 + 1;

    CImg<_Type1> D;
    CImg<_Type1> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetDicPsf(M.data, diameter, radius, invert, softness, angle);
    // _pcndMakeSparseMatrix(w, h, M, D, H, R);
}

#endif // ___MODELS_FFT_HPP___
