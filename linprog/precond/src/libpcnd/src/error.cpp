/*
 ==========================================================================
 |
 |  $Id: error.cpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#include <libpcnd.h>
#include <cstring>

static const char* ERROR_STRINGS[] = {
    "No error",                                             // PCND_OK
    "Generic failure",                                      // PCND_E_FAILED
    "Feature not implemented or not supported",             // PCND_E_NOTIMPL
    "Invalid function call or argument",                    // PCND_E_INVALIDARG
    "Invalid file or data format",                          // PCND_E_INVALIDFORMAT
    "Access violation (out of memory or system resources)", // PCND_E_ACCESSVIOLATION
    "Out of memory",                                        // PCND_E_OUTOFMEMORY
    "Numerical error",                                      // PCND_E_NUMERICAL
    "CUDA device error",                                    // PCND_E_DEVICE
    "IO error",                                             // PCND_E_IO
    "Undefined error code"                                  // PCND_UNKNOWN
};

PCNDRESULT PCNDAPI
pcndGetErrorString(PCNDRESULT result, char* buffer, size_t size) {
    memset(buffer, 0, size);
    if (result < PCND_OK && result >= PCND_UNKNOWN) {
        strncpy(buffer, ERROR_STRINGS[PCND_UNKNOWN], size);
        return PCND_E_INVALIDARG;
    }
    strncpy(buffer, ERROR_STRINGS[result], size);
    return PCND_OK;
}

