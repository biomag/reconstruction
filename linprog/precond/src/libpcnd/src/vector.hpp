#ifndef ___VECTOR_HPP___
#   define ___VECTOR_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "config.h"
#   include <malloc.h>
#   include <memory.h>

// Vector class for primitive types (not for objects)
template <typename _Type>
class VectorBaseT {

    // not implemented
    VectorBaseT(const VectorBaseT&);

protected:

    _Type* _p;
    size_t _n;  // size of array. upper index is n - 1

    inline size_t
    _max_size() const {
        size_t count = (size_t)(-1) / sizeof(_Type);
        return (count > 0) ? count : 1;
    }

    inline _Type*
    _allocate(size_t count) {
        _Type* p = NULL;
        if (count > _max_size())
            throw std::invalid_argument("Vector length exceeds limits.");
        size_t bytes = count * sizeof(_Type);
#ifdef _WIN32
        p = static_cast<_Type*>(_aligned_malloc(bytes, 16));
#else
        posix_memalign((void**)&p, 16, bytes);
#endif
        if (NULL == p)
            throw std::bad_alloc();
        return p;
    }

    inline void
    _free(_Type* p) {
#ifdef _WIN32
        _aligned_free(p);
#else
        free(p);
#endif
    }

public:

    typedef _Type             value_type;
    typedef value_type&       reference;
    typedef value_type const& const_reference;
    typedef value_type*       pointer;
    typedef value_type const* const_pointer;
    typedef size_t            size_type;

    VectorBaseT(): _p(NULL), _n(0) {}
    explicit VectorBaseT(const value_type* p, size_type n):
        _p(const_cast<value_type*>(p)), _n(n) {}

    inline VectorBaseT&
    operator=(const VectorBaseT& that) {
        assert(that._n == _n);
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] = that._p[i    ];
            _p[i + 1] = that._p[i + 1];
            _p[i + 2] = that._p[i + 2];
            _p[i + 3] = that._p[i + 3];
            _p[i + 4] = that._p[i + 4];
            _p[i + 5] = that._p[i + 5];
            _p[i + 6] = that._p[i + 6];
            _p[i + 7] = that._p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] = that._p[i];
        return *this;
    }

    inline VectorBaseT&
    operator=(const value_type& v) { // assign a to every element
        fill(v);
        return *this;
    }

    inline void
    fill(const value_type& v) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] = v;
            _p[i + 1] = v;
            _p[i + 2] = v;
            _p[i + 3] = v;
            _p[i + 4] = v;
            _p[i + 5] = v;
            _p[i + 6] = v;
            _p[i + 7] = v;
        }
        for ( ; i < _n; ++i)
            _p[i] = v;
    }

    inline VectorBaseT&
    abs() {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] = abs(_p[i    ]);
            _p[i + 1] = abs(_p[i + 1]);
            _p[i + 2] = abs(_p[i + 2]);
            _p[i + 3] = abs(_p[i + 3]);
            _p[i + 4] = abs(_p[i + 4]);
            _p[i + 5] = abs(_p[i + 5]);
            _p[i + 6] = abs(_p[i + 6]);
            _p[i + 7] = abs(_p[i + 7]);
        }
        for ( ; i < _n; ++i)
            _p[i] = abs(_p[i]);
        return *this;
    }

    inline VectorBaseT&
    negate() {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] = -_p[i    ];
            _p[i + 1] = -_p[i + 1];
            _p[i + 2] = -_p[i + 2];
            _p[i + 3] = -_p[i + 3];
            _p[i + 4] = -_p[i + 4];
            _p[i + 5] = -_p[i + 5];
            _p[i + 6] = -_p[i + 6];
            _p[i + 7] = -_p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] = -_p[i];
        return *this;
    }

    inline VectorBaseT&
    get_negated(VectorBaseT& that) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            that._p[i    ] = -_p[i    ];
            that._p[i + 1] = -_p[i + 1];
            that._p[i + 2] = -_p[i + 2];
            that._p[i + 3] = -_p[i + 3];
            that._p[i + 4] = -_p[i + 4];
            that._p[i + 5] = -_p[i + 5];
            that._p[i + 6] = -_p[i + 6];
            that._p[i + 7] = -_p[i + 7];
        }
        for ( ; i < _n; ++i)
            that._p[i] = -_p[i];
        return *this;
    }

    inline VectorBaseT&
    square() {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] = _p[i    ] * _p[i    ];
            _p[i + 1] = _p[i + 1] * _p[i + 1];
            _p[i + 2] = _p[i + 2] * _p[i + 2];
            _p[i + 3] = _p[i + 3] * _p[i + 3];
            _p[i + 4] = _p[i + 4] * _p[i + 4];
            _p[i + 5] = _p[i + 5] * _p[i + 5];
            _p[i + 6] = _p[i + 6] * _p[i + 6];
            _p[i + 7] = _p[i + 7] * _p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] = _p[i] * _p[i];
        return *this;
    }

    inline void
    get_squared(VectorBaseT& that) const {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            that._p[i    ] = _p[i    ] * _p[i    ];
            that._p[i + 1] = _p[i + 1] * _p[i + 1];
            that._p[i + 2] = _p[i + 2] * _p[i + 2];
            that._p[i + 3] = _p[i + 3] * _p[i + 3];
            that._p[i + 4] = _p[i + 4] * _p[i + 4];
            that._p[i + 5] = _p[i + 5] * _p[i + 5];
            that._p[i + 6] = _p[i + 6] * _p[i + 6];
            that._p[i + 7] = _p[i + 7] * _p[i + 7];
        }
        for ( ; i < _n; ++i)
            that._p[i] = _p[i] * _p[i];
    }

    inline VectorBaseT&
    operator+=(const value_type& v) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] += v;
            _p[i + 1] += v;
            _p[i + 2] += v;
            _p[i + 3] += v;
            _p[i + 4] += v;
            _p[i + 5] += v;
            _p[i + 6] += v;
            _p[i + 7] += v;
        }
        for ( ; i < _n; ++i)
            _p[i] += v;
        return *this;
    }

    inline VectorBaseT&
    operator+=(const VectorBaseT& that) {
        assert(that._n == _n);
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] += that._p[i    ];
            _p[i + 1] += that._p[i + 1];
            _p[i + 2] += that._p[i + 2];
            _p[i + 3] += that._p[i + 3];
            _p[i + 4] += that._p[i + 4];
            _p[i + 5] += that._p[i + 5];
            _p[i + 6] += that._p[i + 6];
            _p[i + 7] += that._p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] += that._p[i];
        return *this;
    }

    inline VectorBaseT&
    operator-=(const value_type& v) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] -= v;
            _p[i + 1] -= v;
            _p[i + 2] -= v;
            _p[i + 3] -= v;
            _p[i + 4] -= v;
            _p[i + 5] -= v;
            _p[i + 6] -= v;
            _p[i + 7] -= v;
        }
        for ( ; i < _n; ++i)
            _p[i] -= v;
        return *this;
    }

    inline VectorBaseT&
    operator-=(const VectorBaseT& that) {
        assert(that._n == _n);
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] -= that._p[i    ];
            _p[i + 1] -= that._p[i + 1];
            _p[i + 2] -= that._p[i + 2];
            _p[i + 3] -= that._p[i + 3];
            _p[i + 4] -= that._p[i + 4];
            _p[i + 5] -= that._p[i + 5];
            _p[i + 6] -= that._p[i + 6];
            _p[i + 7] -= that._p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] -= that._p[i];
        return *this;
    }

    inline VectorBaseT&
    operator*=(const value_type& v) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] *= v;
            _p[i + 1] *= v;
            _p[i + 2] *= v;
            _p[i + 3] *= v;
            _p[i + 4] *= v;
            _p[i + 5] *= v;
            _p[i + 6] *= v;
            _p[i + 7] *= v;
        }
        for ( ; i < _n; ++i)
            _p[i] *= v;
        return *this;
    }

    inline VectorBaseT&
    operator*=(const VectorBaseT& that) {
        assert(that._n == _n);
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] *= that._p[i    ];
            _p[i + 1] *= that._p[i + 1];
            _p[i + 2] *= that._p[i + 2];
            _p[i + 3] *= that._p[i + 3];
            _p[i + 4] *= that._p[i + 4];
            _p[i + 5] *= that._p[i + 5];
            _p[i + 6] *= that._p[i + 6];
            _p[i + 7] *= that._p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] *= that._p[i];
        return *this;
    }

    inline VectorBaseT&
    operator/=(const value_type& v) {
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] /= v;
            _p[i + 1] /= v;
            _p[i + 2] /= v;
            _p[i + 3] /= v;
            _p[i + 4] /= v;
            _p[i + 5] /= v;
            _p[i + 6] /= v;
            _p[i + 7] /= v;
        }
        for ( ; i < _n; ++i)
            _p[i] /= v;
        return *this;
    }

    inline VectorBaseT&
    operator/=(const VectorBaseT& that) {
        assert(that._n == _n);
        size_type i;
        for (i = 0; i + 8 < _n; i += 8) {
            _p[i    ] /= that._p[i    ];
            _p[i + 1] /= that._p[i + 1];
            _p[i + 2] /= that._p[i + 2];
            _p[i + 3] /= that._p[i + 3];
            _p[i + 4] /= that._p[i + 4];
            _p[i + 5] /= that._p[i + 5];
            _p[i + 6] /= that._p[i + 6];
            _p[i + 7] /= that._p[i + 7];
        }
        for ( ; i < _n; ++i)
            _p[i] /= that._p[i];
        return *this;
    }

    // i'th element
    inline reference       operator[](size_type i)       { return _p[i]; }
    inline const_reference operator[](size_type i) const { return _p[i]; }
    inline pointer         data()       { return _p; }
    inline const_pointer   data() const { return _p; }
    inline size_type       size() const { return _n; }

};

///////////////////////////////////////////////////////////////////////////
template <typename _Type>
class VectorRefT:
    public VectorBaseT<_Type> {

private:
    typedef VectorBaseT<_Type> _Base;

public:
    
    typedef typename _Base::value_type      value_type;
    typedef typename _Base::reference       reference;
    typedef typename _Base::const_reference const_reference;
    typedef typename _Base::pointer         pointer;
    typedef typename _Base::const_pointer   const_pointer;
    typedef typename _Base::size_type       size_type;

    explicit VectorRefT(const value_type* p, size_type n):
        _Base(p, n) {}
};


///////////////////////////////////////////////////////////////////////////
template <typename _Type>
class VectorT:
    public VectorBaseT<_Type> {

private:
    typedef VectorBaseT<_Type> _Base;

public:
    
    typedef typename _Base::value_type      value_type;
    typedef typename _Base::reference       reference;
    typedef typename _Base::const_reference const_reference;
    typedef typename _Base::pointer         pointer;
    typedef typename _Base::const_pointer   const_pointer;
    typedef typename _Base::size_type       size_type;
    
    VectorT(): _Base() {}
    explicit VectorT(size_type n);                          // zero-based array
    explicit VectorT(size_type n, const value_type& v);     // initialize to constant value
    VectorT(const value_type* p, size_type n);              // Initialize to array
    VectorT(const VectorT& that);                           // copy constructor

    template <typename _Type2>
    VectorT(const VectorBaseT<_Type2>& that);               // copy constructor

    inline VectorT& operator=(const VectorT& that);         // assignment

    template <typename _Type2>
    inline VectorT& operator=(const VectorT<_Type2>& that); // assignment

    inline VectorT
    operator-() const {
        return VectorT(*this).negate();
    }

    inline void resize(size_type n, bool retain = true);

    ~VectorT();
};

template <class _Type>
VectorT<_Type>::VectorT(size_type n):
    _Base(_Base::_allocate(n), n)
{
}

template <class _Type>
VectorT<_Type>::VectorT(size_type n, const value_type& v):
    _Base(_Base::_allocate(n), n)
{
    _Base::fill(v);
}

template <class _Type>
VectorT<_Type>::VectorT(const value_type* p, size_type n):
    _Base(_Base::_allocate(n), n)
{
    size_type i;
    for (i = 0; i + 8 < n; i += 8) {
        _Base::_p[i    ] = *p++;
        _Base::_p[i + 1] = *p++;
        _Base::_p[i + 2] = *p++;
        _Base::_p[i + 3] = *p++;
        _Base::_p[i + 4] = *p++;
        _Base::_p[i + 5] = *p++;
        _Base::_p[i + 6] = *p++;
        _Base::_p[i + 7] = *p++;
    }
    for ( ; i < n; ++i)
        _Base::_p[i] = *p++;
}

template <class _Type>
VectorT<_Type>::VectorT(const VectorT<_Type>& that):
    _Base(_Base::_allocate(that._n), that._n)
{
    size_type i;
    for (i = 0; i + 8 < that._n; i += 8) {
        _Base::_p[i    ] = that._p[i    ];
        _Base::_p[i + 1] = that._p[i + 1];
        _Base::_p[i + 2] = that._p[i + 2];
        _Base::_p[i + 3] = that._p[i + 3];
        _Base::_p[i + 4] = that._p[i + 4];
        _Base::_p[i + 5] = that._p[i + 5];
        _Base::_p[i + 6] = that._p[i + 6];
        _Base::_p[i + 7] = that._p[i + 7];
    }
    for ( ; i < that._n; ++i)
        _Base::_p[i] = that._p[i];
}

template <class _Type>
    template <typename _Type2>
VectorT<_Type>::VectorT(const VectorBaseT<_Type2>& that):
    _Base(_Base::_allocate(that.size()), that.size())
{
    size_type i;
    for (i = 0; i + 8 < _Base::_n; i += 8) {
        _Base::_p[i    ] = static_cast<_Type>(that[i    ]);
        _Base::_p[i + 1] = static_cast<_Type>(that[i + 1]);
        _Base::_p[i + 2] = static_cast<_Type>(that[i + 2]);
        _Base::_p[i + 3] = static_cast<_Type>(that[i + 3]);
        _Base::_p[i + 4] = static_cast<_Type>(that[i + 4]);
        _Base::_p[i + 5] = static_cast<_Type>(that[i + 5]);
        _Base::_p[i + 6] = static_cast<_Type>(that[i + 6]);
        _Base::_p[i + 7] = static_cast<_Type>(that[i + 7]);
    }
    for ( ; i < _Base::_n; ++i)
        _Base::_p[i] = static_cast<_Type>(that[i]);
}

template <class _Type>
inline VectorT<_Type>&
VectorT<_Type>::operator=(const VectorT<_Type>& that)
// postcondition: normal assignment via copying has been performed;
//                if vector and that were different sizes, vector
//                has been resized to match the Count of that
{
    if (this != &that) {
        if (_Base::_n != that._n) {
            if (_Base::_p != NULL)
                _free(_Base::_p);
            _Base::_p = _Base::_allocate(that._n);
            _Base::_n = that._n;
        }
        size_type i;
        for (i = 0; i + 8 < that._n; i += 8) {
            _Base::_p[i    ] = that._p[i    ];
            _Base::_p[i + 1] = that._p[i + 1];
            _Base::_p[i + 2] = that._p[i + 2];
            _Base::_p[i + 3] = that._p[i + 3];
            _Base::_p[i + 4] = that._p[i + 4];
            _Base::_p[i + 5] = that._p[i + 5];
            _Base::_p[i + 6] = that._p[i + 6];
            _Base::_p[i + 7] = that._p[i + 7];
        }
        for ( ; i < that._n; ++i)
            _Base::_p[i] = that._p[i];
    }
    return *this;
}

template <class _Type>
    template <typename _Type2>
inline VectorT<_Type>&
VectorT<_Type>::operator=(const VectorT<_Type2>& that)
// postcondition: normal assignment via copying has been performed;
//                if vector and that were different sizes, vector
//                has been resized to match the Count of that
{
    if (this != &that) {
        if (_Base::_n != that.size()) {
            if (_Base::_p != NULL)
                _free(_Base::_p);
            _Base::_p = _Base::_allocate(that._n);
            _Base::_n = that.size();
        }
        size_type i;
        for (i = 0; i + 8 < that.size(); i += 8) {
            _Base::_p[i    ] = static_cast<_Type>(that[i    ]);
            _Base::_p[i + 1] = static_cast<_Type>(that[i + 1]);
            _Base::_p[i + 2] = static_cast<_Type>(that[i + 2]);
            _Base::_p[i + 3] = static_cast<_Type>(that[i + 3]);
            _Base::_p[i + 4] = static_cast<_Type>(that[i + 4]);
            _Base::_p[i + 5] = static_cast<_Type>(that[i + 5]);
            _Base::_p[i + 6] = static_cast<_Type>(that[i + 6]);
            _Base::_p[i + 7] = static_cast<_Type>(that[i + 7]);
        }
        for ( ; i < that.size(); ++i)
            _Base::_p[i] = static_cast<_Type>(that[i]);
    }
    return *this;
}

template <class _Type>
inline void
VectorT<_Type>::resize(size_type n, bool retain) {
    if (_Base::_n != n) {
        value_type* p = NULL;
        if (0 != n) {
            p = _Base::_allocate(n);
            if (retain) {
                for (size_type i = 0; i < n && i < _Base::_n; ++i)
                    p[i] = _Base::_p[i];
            }
        }
        if (_Base::_p != NULL)
            _free(_Base::_p);
        _Base::_p = p;
        _Base::_n = n;
    }
}

template <class _Type>
VectorT<_Type>::~VectorT() {
    if (_Base::_p != NULL)
        _free(_Base::_p);
}

///////////////////////////////////////////////////////////////////////////
// operators
///////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline VectorT<_Type>
operator+(const VectorBaseT<_Type>& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(a);
    c += b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator-(const VectorBaseT<_Type>& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(a);
    c -= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator*(const VectorBaseT<_Type>& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(a);
    c *= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator/(const VectorBaseT<_Type>& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(a);
    c /= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator+(const VectorBaseT<_Type>& a, const _Type& b) {
    VectorT<_Type> c(a);
    c += b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator-(const VectorBaseT<_Type>& a, const _Type& b) {
    VectorT<_Type> c(a);
    c -= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator*(const VectorBaseT<_Type>& a, const _Type& b) {
    VectorT<_Type> c(a);
    c *= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator/(const VectorBaseT<_Type>& a, const _Type& b) {
    VectorT<_Type> c(a);
    c /= b;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator+(const _Type& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(b);
    c += a;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator-(const _Type& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(b);
    c.negate();
    c += a;
    return c;
}

template <typename _Type>
inline VectorT<_Type>
operator*(const _Type& a, const VectorBaseT<_Type>& b) {
    VectorT<_Type> c(b);
    c *= a;
    return c;
}

#endif // ___VECTOR_HPP___
