#include <libpcnd.h>
#include "config.h"

///////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline void
_pcndConv(
    const _Type* kernel,
    int          kernelSize,
    _Type*       out,
    const _Type* in,
    int          size,
    int          stride
    )
{
    int x, i, m;

    for (x = 0; x + 4 < size; x += 4) {
        m = x + kernelSize - 1;
        out[(x    ) * stride] = in[m    ] * kernel[0];
        out[(x + 1) * stride] = in[m + 1] * kernel[0];
        out[(x + 2) * stride] = in[m + 2] * kernel[0];
        out[(x + 3) * stride] = in[m + 3] * kernel[0];
    }
    while (x < size) {
        out[x * stride] = in[x + kernelSize - 1] * kernel[0];
        ++x;
    }

    for (i = 1; i < kernelSize; ++i) {
        for (x = 0; x + 4 < size; x += 4) {
            m = x + kernelSize - 1 - i;
            out[(x    ) * stride] += in[m    ] * kernel[i];
            out[(x + 1) * stride] += in[m + 1] * kernel[i];
            out[(x + 2) * stride] += in[m + 2] * kernel[i];
            out[(x + 3) * stride] += in[m + 3] * kernel[i];
        }
        while (x < size) {
            out[x * stride] += in[x + kernelSize - 1 - i] * kernel[i];
            ++x;
        }
    }
}

///////////////////////////////////////////////////////////////////////////
// n! / ((n-k)!k!)
template <typename _Type>
inline _Type
_pcndNChooseK(_Type n, _Type k) {
    assert(k <= n);
    _Type c = 1;
    _Type m = n - k;
    while (n > k) c *= (n--);
    while (m) c /= (m--);
    return c;
}

//////////////////////////////////////////////////////////////////////////
void
_pcndConvLine(
    float*       out,
    const float* in,
    int          size,
    int          stride,
    const float* kernel,
    int          kernelSize,
    float*       work
    )
{
    int x;
    int half = kernelSize / 2;
    int fullSize = size + kernelSize - 1;

    // Mirror boundary condition.
    for (x = 0; x < half;        ++x) work[x] = in[(half - x) * stride];
    for (     ; x < half + size; ++x) work[x] = in[(x - half) * stride];
    for (     ; x < fullSize;    ++x)
        work[x] = in[(size + size + half - x - 1) * stride];

    _pcndConv(kernel, kernelSize, out, work, size, stride);
}

///////////////////////////////////////////////////////////////////////////
PCNDRESULT
_pcndMakeSmoothKernel(
    float** kernel,
    int*    kernelSize,
    int     n,
    float   sigma)
{
    int r = (int)ceil(3.0 * sqrt((float)n) * sigma);
    if (r <= 0)
        return PCND_E_INVALIDARG;

    *kernelSize = r * 2 + 1;

    *kernel = (float*)pcndAlloc(sizeof(float) * (*kernelSize));
    if (NULL == (*kernel))
        return PCND_E_OUTOFMEMORY;

    memset((*kernel), 0, sizeof(float) * (*kernelSize));

    float sqrt2Pi = (float)sqrt(2.0 * acos(-1.0));
    for (int i = 1; i <= n; ++i) {
        int x;
        float s = (float)sqrt((float)i) * sigma;
        float denom = (float)(_pcndNChooseK(n, i) / (sqrt2Pi * s));
        float halfNegVar = (float)(-0.5 / (s * s));
        if (0 != (i % 2)) {
            for (x = -r; x <= r; ++x)
                (*kernel)[x + r] += denom * exp(x * x * halfNegVar);
        }
        else {
            for (x = -r; x <= r; ++x)
                (*kernel)[x + r] -= denom * exp(x * x * halfNegVar);
        }
    }

    return PCND_OK;
}

///////////////////////////////////////////////////////////////////////////
PCNDRESULT
_pcndSmoothByKernel(
    float* out,
    const float* in, int w, int h,
    const float* kernel, int kernelSize
    )
{
    int bufferSize = (w > h) ?
        kernelSize + w - 1 : kernelSize + h - 1;

    if (kernelSize >= w * 2 ||
        kernelSize >= h * 2)
        return PCND_E_INVALIDARG;

    float* work = (float*)pcndAlloc(bufferSize * sizeof(float));
    if (NULL == work)
        return PCND_E_OUTOFMEMORY;

    {
        float* dst = out;
        const float* src = in;
        for (int y = 0; y < h; ++y) {
            _pcndConvLine(dst, src, w, 1, kernel,
                kernelSize, work);
            src += w;
            dst += w;
        }
    }

    {
        float* dst = out;
        const float* src = out;
        for (int x = 0; x < w; ++x) {
            _pcndConvLine(dst, src, h, w, kernel,
                kernelSize, work);
            src += 1;
            dst += 1;
        }
    }

    pcndFree(work);

    return PCND_OK;
}


///////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndSmooth(
    float* out,
    const float* in,
    int w, int h, int n, float sigma
    )
{
    PCNDRESULT ret;
    float*     kernel = NULL;
    int        kernelSize = 0;

    ret = _pcndMakeSmoothKernel(
        &kernel, &kernelSize, n, sigma);
    if (ret == PCND_OK) {
        ret = _pcndSmoothByKernel(out, in, w, h, kernel, kernelSize);
        pcndFree(kernel);
    }

    return ret;
}
