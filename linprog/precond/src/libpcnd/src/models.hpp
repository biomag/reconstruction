#ifndef ___MODELS_HPP___
#   define ___MODELS_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include "sparse.hpp"
#   include <vector>

//////////////////////////////////////////////////////////////////////////
template <typename _Type>
void
_pcndMakeSparseMatrix(
    unsigned long         w,
    unsigned long         h,
    const CImg<_Type>&    M,
    const CImg<_Type>&    D,
    SparseMatrixT<_Type>& H,
    SparseMatrixT<_Type>& R
    )
{
    unsigned long npixels = w * h;

    {
        unsigned long mnz = 0;
        unsigned long dnz = 0;

        for (int i = 0; i < (int)M.size(); ++i)
            if (M[i] != 0) ++mnz;

        for (int i = 0; i < (int)D.size(); ++i)
            if (D[i] != 0) ++dnz;

        H.create(npixels, npixels, mnz * npixels);
        R.create(npixels, npixels, dnz * npixels);
    }

    CImg<int> Mx(M.dimx(), M.dimy());
    CImg<int> My(M.dimx(), M.dimy());
    CImg<int> Dx(D.dimx(), D.dimy());
    CImg<int> Dy(D.dimx(), D.dimy());

    for (int y = 0; y < (int)M.dimy(); ++y) {
        for (int x = 0; x < (int)M.dimx(); ++x) {
            Mx(x, y) = x - (int)(M.dimx() / 2);
            My(x, y) = y - (int)(M.dimy() / 2);
        }
    }
    for (int y = 0; y < (int)D.dimy(); ++y) {
        for (int x = 0; x < (int)D.dimx(); ++x) {
            Dx(x, y) = x - (int)(D.dimx() / 2);
            Dy(x, y) = y - (int)(D.dimy() / 2);
        }
    }

    // Fill in the values of H and R.
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            unsigned long  mnz = 0;
            VectorT<_Type> row(npixels, 0.0f);
            VectorT<int>   ind(npixels, -1);
            vector<bool>   ini(npixels, false);

            for (int j = 0; j < (int)npixels; ++j) {

                div_t t = div(j, (int)w);
                int x = t.rem ;
                int y = t.quot;

                int pos = 0;
                H.jc(j) = mnz;
                for (int k = 0; k < (int)M.size(); ++k) {
                    if (M[k] != 0) {
                        int xx = x + Mx[k];
                        if (xx < 0)
                            xx = -xx;
                        else if (xx >= (int)w)
                            xx = (int)w * 2 - xx - 2;

                        int yy = y + My[k];
                        if (yy < 0)
                            yy = -yy;
                        else if (yy >= (int)h)
                            yy = (int)h * 2 - yy - 2;

                        int i = (int)(yy * w + xx);

                        if (ini[i]) row[i] += M[k];
                        else {
                            row[i]     = M[k];
                            ini[i]     = true;
                            ind[pos++] = i;
                        }
                    }
                } // k

                for (int k = 0; k < pos; ++k) {
                    int i = ind[k];
                    H.ir(mnz) = i;
                    H.pr(mnz) = row[i];
                    ini[i] = false;
                    row[i] = 0.0f;
                    ++mnz;
                }
            }
            H.jc(npixels) = mnz;
        }

        #pragma omp section
        {
            unsigned long  dnz = 0;
            VectorT<_Type> row(npixels, 0.0f);
            VectorT<int>   ind(npixels, -1);
            vector<bool>   ini(npixels, false);

            for (int j = 0; j < (int)npixels; ++j) {

                div_t t = div(j, (int)w);
                int x = t.rem ;
                int y = t.quot;

                int pos = 0;
                R.jc(j) = dnz;
                for (int k = 0; k < (int)D.size(); ++k) {
                    if (D[k] != 0) {
                        int xx = x + Dx[k];
                        if (xx < 0)
                            xx = -xx;
                        else if (xx >= (int)w)
                            xx = (int)w * 2 - xx - 2;

                        int yy = y + Dy[k];
                        if (yy < 0)
                            yy = -yy;
                        else if (yy >= (int)h)
                            yy = (int)h * 2 - yy - 2;

                        int i = (int)(yy * w + xx);

                        if (ini[i]) row[i] += D[k];
                        else {
                            row[i]     = D[k];
                            ini[i]     = true;
                            ind[pos++] = i;
                        }
                    }
                } // k

                for (int k = 0; k < pos; ++k) {
                    int i = ind[k];
                    R.ir(dnz) = i;
                    R.pr(dnz) = row[i];
                    ini[i] = false;
                    row[i] = 0.0;
                    ++dnz;
                }

            } // j
            R.jc(npixels) = dnz;
        }
    } // #pragma omp parallel sections
}


//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndGetLaplacian(CImg<_Type1>& D, _Type2 smoothness) {
    D.assign(3, 3);
    D(0, 0) =-0.125f * smoothness;
    D(0, 1) =-0.125f * smoothness;
    D(0, 2) =-0.125f * smoothness;
    D(1, 0) =-0.125f * smoothness;
    D(1, 1) = 1.000f * smoothness;
    D(1, 2) =-0.125f * smoothness;
    D(2, 0) =-0.125f * smoothness;
    D(2, 1) =-0.125f * smoothness;
    D(2, 2) =-0.125f * smoothness;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndGetLaplacian(CImg<_Type1>& D, _Type2 sparsity, _Type2 smoothness) {
    D.assign(3, 3);
    D(0, 0) = 0.0f;
    D(0, 1) =-0.125f * smoothness;
    D(0, 2) = 0.0f;
    D(1, 0) =-0.125f * smoothness;
    D(1, 1) = 0.500f * smoothness + sparsity;
    D(1, 2) =-0.125f * smoothness;
    D(2, 0) = 0.0f;
    D(2, 1) =-0.125f * smoothness;
    D(2, 2) = 0.0f;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline int
_pcndGetPhasePsfRadius(_Type haloSigma, _Type haloSigmaRatio) {
    _Type sigma = haloSigmaRatio > 1.0f ?
        haloSigma * haloSigmaRatio : haloSigma;
    int radius = (int)(sigma + 0.5f);
    if (radius < 1)
        radius = 1;
    return radius;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
inline void
_pcndGetPhasePsf(
    _Type1* data,
    int diameter, int radius, bool invert,
    _Type2 haloAmount, _Type2 haloContrast, _Type2 haloSigma,
    _Type2 haloSigmaRatio)
{
    int size = diameter * diameter;
    VectorT<_Type1> vtemp(size);
    VectorRefT<_Type1> vdata(data, size);

    _Type2 haloSigma2 = haloSigma * haloSigmaRatio;

    #pragma omp parallel sections
    {
        #pragma omp section
        {
            if (haloSigma <= 0.0) {
                vdata.fill(0.0f);
                vdata[radius + radius * diameter] = 1.0f;
            }
            else {
                _Type1 sum = 0.0f;
                _Type1 scale = (_Type1)(1.0f / haloSigma);

                for (int i = 0; i < diameter; ++i) {
                    for (int j = 0; j < diameter; ++j) {
                        _Type1 x = (_Type1)(j - radius) * scale;
                        _Type1 y = (_Type1)(i - radius) * scale;
                        _Type1 v = exp(-0.5f * (x * x + y * y));
                        vdata[j + i * diameter] = v;
                        sum += v;
                    } // j
                } // i

                if (0.0f != sum) vdata /= sum;
            }
        }

        #pragma omp section
        {
            if (haloSigma2 <= 0.0) {
                vtemp.fill(0.0f);
                vtemp[radius + radius * diameter] = 1.0f;
            }
            else {
                _Type1 sum = 0.0f;
                _Type1 scale = (_Type1)(1.0f / haloSigma2);

                for (int i = 0; i < diameter; ++i) {
                    for (int j = 0; j < diameter; ++j) {
                        _Type1 x = (_Type1)(j - radius) * scale;
                        _Type1 y = (_Type1)(i - radius) * scale;
                        _Type1 v = exp(-0.5f * (x * x + y * y));
                        vtemp[j + i * diameter] = v;
                        sum += v;
                    } // j
                } // i

                if (0.0f != sum) vtemp /= sum;
            }
        }
    } // #pragma omp parallel sections

    {
        for (int i = 0; i < size; ++i)
            vdata[i] -= haloContrast * vtemp[i];

        if (invert) {
            for (int i = 0; i < size; ++i) vdata[i] *= -haloAmount;
            vdata[radius + radius * diameter] += 1.0;
        }
        else {
            for (int i = 0; i < size; ++i) vdata[i] *=  haloAmount;
            vdata[radius + radius * diameter] -= 1.0;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndCreatePhaseModel(
    SparseMatrixT<_Type1>& H, SparseMatrixT<_Type1>& R,
    unsigned long w, unsigned long h, bool invert, _Type2 smoothness,
    _Type2 haloAmount, _Type2 haloContrast, _Type2 haloSigma,
    _Type2 haloSigmaRatio
    )
{
    int radius   = _pcndGetPhasePsfRadius(haloSigma, haloSigmaRatio);
    int diameter = radius * 2 + 1;

    CImg<_Type1> D;
    CImg<_Type1> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetPhasePsf(
        M.data,
        diameter, radius, invert,
        haloAmount, haloContrast, haloSigma, haloSigmaRatio);
    _pcndMakeSparseMatrix(w, h, M, D, H, R);
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type>
inline int
_pcndGetDicPsfRadius(_Type softness) {
    int r = (int)(softness + 0.5);
    if (r < 1)
        r = 1;
    return r;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
inline void
_pcndGetDicPsf(_Type1* data, int diameter, int radius,
               bool invert, _Type2 softness, _Type2 angle)
{
    _Type1 rad = (_Type1)(angle * M_PI / 180.0f);

    if (invert)
        rad += (_Type1)M_PI;

    int i;
    _Type1 sum = 0.f;
    _Type1 c = cos(rad), s = sin(rad);
    _Type1 scale = (softness == 0.0f) ? 1.0f : (_Type1)(1.0f / softness);

    #pragma omp parallel private(i)
    {
        #pragma omp for reduction(+:sum)
        for (i = 0; i < diameter; ++i) {
            for (int j = 0; j < diameter; ++j) {
                _Type1 x = static_cast<_Type1>(j - radius) * scale;
                _Type1 y = static_cast<_Type1>(i - radius) * scale;
                _Type1 e = exp(-.5f * (x * x + y * y));
                _Type1 v = c * (-x * e) + s * (-y * e);
                data[j + i * diameter] = v;
                sum += fabs(v);
            } // j
        } // i
    }

    if (sum > 0.0f) {
        sum = 1.0f / sum;
        #pragma omp parallel private(i)
        {
            #pragma omp for
            for (i = 0; i < diameter * diameter; ++i)
                data[i] *= sum;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndCreateDicModel(
    SparseMatrixT<_Type1>& H, SparseMatrixT<_Type1>& R,
    unsigned long w, unsigned long h, bool invert,
    _Type2 smoothness, _Type2 softness,
    _Type2 angle
    )
{
    int radius   = _pcndGetDicPsfRadius(softness);
    int diameter = radius * 2 + 1;

    CImg<_Type1> D;
    CImg<_Type1> M(diameter, diameter);

    _pcndGetLaplacian(D, smoothness);
    _pcndGetDicPsf(M.data, diameter, radius, invert, softness, angle);
    _pcndMakeSparseMatrix(w, h, M, D, H, R);
}

//////////////////////////////////////////////////////////////////////////
template <typename _Type1, typename _Type2>
void
_pcndCreateModelWithPsf(
    SparseMatrixT<_Type1>& H, SparseMatrixT<_Type1>& R,
    const _Type2* psf, int psfw, int psfh,
    unsigned long w, unsigned long h,
    bool invert, _Type2 smoothness
    )
{
    CImg<_Type1> D;
    CImg<_Type1> M(psf, psfw, psfh, 1, 1, false);

    int i;
    int size = M.size();
    _Type1 sum = 0.0f;

    #pragma omp parallel private(i)
    {
        #pragma omp for reduction(+:sum)
        for (i = 0; i < size; ++i) {
            if (invert) M[i] = -M[i];
            sum += fabs(M[i]);
        } // i
    }

    sum = 1.0f / sum;

    #pragma omp parallel private(i)
    {
        #pragma omp for
        for (i = 0; i < size; ++i)
            M[i] *= sum;
    }

    _pcndGetLaplacian(D, smoothness);
    _pcndMakeSparseMatrix(w, h, M, D, H, R);
}


#endif //___MODELS_HPP___
