/*
 ==========================================================================
 |
 |  $Id: chol.hpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#ifndef ___CHOL_HPP___
#   define ___CHOL_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <cmath>
#   include <stdexcept>

template <typename _Matrix, typename _Vector>
struct CholeskyT {

    typedef _Matrix matrix_type;
    typedef _Vector vector_type;
    typedef typename matrix_type::value_type value_type;

    int n;
    matrix_type el;

    CholeskyT(const matrix_type &a, bool shared = false, bool fill = true):
        n(a.dimy()), el(a, shared)
    {
        int i, j, k;
        if (el.dimx() != n)
            throw std::invalid_argument("The input matrix must be square.");
        /*
        for (i = 0; i < n; ++i) {
            for (j = i; j < n; ++j) {
                value_type sum;
                for (sum = el(j, i), k = i - 1; k >= 0; --k)
                    sum -= el(k, i) * el(k, j);
                if (i == j) {
                    if (sum <= 0.0f)
                        throw std::runtime_error("Cholesky failed.");
                    el(i, i) = sqrt(sum);
                }
                else
                    el(i, j) = sum / el(i, i);
            }
        }
        if (fill) {
            for (i = 0; i < n; ++i)
                for (j = 0; j < i; ++j) el(i, j) = 0.0f;
        }
        */
        // Submatrix Cholesky
        /*
        for (k = 0; k < n; ++k) {
            el(k, k) = sqrt(el(k, k));
            for (j = k + 1; j < n; ++j) el(k, j) /= el(k, k);
            for (i = k + 1; i < n; ++i)
                for (j = i; j < n; ++j)
                    el(i, j) -= el(k, i) * el(k, j);
        }
        */

        // Row Cholesky
        for (i = 0; i < n; ++i) {
            for (j = i; j < n; ++j)
                for (k = 0; k < i; ++k)
                    el(i, j) -= el(k, i) * el(k, j);
            el(i, i) = sqrt(el(i, i));
            for (j = i + 1; j < n; ++j) el(i, j) /= el(i, i);
        }

        if (fill) {
            // Fill in lower triangular part.
            for (i = 0; i < n; ++i)
                for (j = 0; j < i; ++j) el(i, j) = 0.0f;
        }
    }

    void
    solve(const vector_type &b, vector_type &x) {
        int i, k;
        value_type sum;
        if (b.size() != n || x.size() != n)
            throw std::invalid_argument("Unexpected vector lengths.");
        for (i = 0; i < n; ++i) {
            for (sum = b[i], k = i - 1; k >= 0; --k)
                sum -= el(k, i) * x[k];
            x[i] = sum / el(i, i);
        }
        for (i = n - 1; i >= 0; --i) {
            for (sum = x[i], k = i + 1; k < n; ++k)
                sum -= el(i, k) * x[k];
            x[i] = sum / el(i, i);
        }
    }

    void
    elmult(const vector_type &y, vector_type &b) {
        int i, j;
        if (b.size() != n || y.size() != n)
            throw std::invalid_argument("Unexpected vector lengths.");
        for (i = 0; i < n; ++i) {
            b[i] = 0.0f;
            for (j = 0; j <= i; ++j)
                b[i] += el(j, i) * y[j];
        }
    }

    void
    elsolve(const vector_type &b, vector_type &y) {
        int i, j;
        value_type sum;
        if (b.size() != n || y.size() != n)
            throw std::invalid_argument("Unexpected vector lengths.");
        for (i = 0; i < n; ++i) {
            for (sum = b[i], j = 0; j < i; ++j)
                sum -= el(j, i) * y[j];
            y[i] = sum / el(i, i);
        }
    }

    void
    inverse(matrix_type &ainv) {
        int i,j,k;
        value_type sum;
        ainv.assign(n, n);
        for (i = 0; i < n; ++i) for (j = 0; j <= i; ++j){
            sum = (i == j ? 1.0f : 0.0f);
            for (k = i - 1; k >= j; --k)
                sum -= el(k, i) * ainv(k, j);
            ainv(i, j) = sum / el(i, i);
        }
        for (i = n - 1; i >= 0; --i) {
            for (j = 0; j <= i; ++j) {
                sum = (i < j) ? 0.0f : ainv(i, j);
                for (k = i + 1; k < n; ++k) sum -= el(i, k) * ainv(k, j);
                ainv(j, i) = ainv(i, j) = sum / el(i, i);
            }
        }
    }

    value_type
    logdet() {
        value_type sum = 0.0f;
        for (int i = 0; i < n; ++i)
            sum += log(el(i, i));
        return 2.0f * sum;
    }
};

#endif // ___CHOL_HPP___

