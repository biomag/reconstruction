#ifndef ___VERSION_H___
#   define ___VERSION_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   define VERSION_MAJOR 1
#   define VERSION_MINOR 0
#   define VERSION_MAGIC 0
#   define VERSION_BUILD 102

#endif
