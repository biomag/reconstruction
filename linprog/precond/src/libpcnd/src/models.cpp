#include <libpcnd.h>
#include "models.hpp"
#include "ioutils.h"

static const unsigned int PCND_PSF_MAGIC = 0xFF465350;

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndIsPsfFile(const char* path) {
    size_t nread = 0;

    FILE* fp = fopen(path, "rb");
    if (!fp)
        return PCND_E_IO;

    unsigned int magic = PCND_PSF_MAGIC;
    if (1 != (nread = pcndGetUInt(fp, &magic))
        || magic != PCND_PSF_MAGIC) {
        fclose(fp);
        return (1 != nread) ? PCND_E_IO : PCND_E_INVALIDFORMAT;
    }

    fclose(fp);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndLoadPsf(
    int*         ndim,
    int**        dims,
    float**      data,
    const char*  path
    )
{
    size_t nread = 0;

    FILE* fp = fopen(path, "rb");
    if (!fp)
        return PCND_E_IO;

    unsigned int magic = 0;
    if (1 != (nread = pcndGetUInt(fp, &magic))
        || magic != PCND_PSF_MAGIC) {
        fclose(fp);
        return (1 != nread) ? PCND_E_IO : PCND_E_INVALIDFORMAT;
    }

    if (1 != pcndGetInt(fp, ndim)) {
        fclose(fp);
        return PCND_E_IO;
    }

    if (*ndim <= 0) {
        fclose(fp);
        return PCND_E_INVALIDARG;
    }

    *dims = (int*)pcndCalloc(sizeof(int), *ndim);

    size_t count = 1;
    for (int i = 0; i < *ndim; ++i) {
        if (1 != (nread = pcndGetInt(fp, &(*dims)[i]))
            || (*dims)[i] <= 0) {
            fclose(fp);
            pcndFree(*dims);
            return (1 != nread) ?
                PCND_E_IO : PCND_E_INVALIDARG;
        }
        count *= (size_t)(*dims)[i];
    }

    *data = (float*)pcndCalloc(sizeof(float), count);
    if (count != fread(*data, sizeof(float), count, fp)) {
        fclose(fp);
        pcndFree(*dims);
        pcndFree(*data);
        return PCND_E_IO;
    }

    fclose(fp);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndLoadPsf2(
    int*         w,
    int*         h,
    float**      data,
    const char*  path)
{
    int ndim = 0;
    int* dims = 0;
    PCNDRESULT ret = PCND_OK;

    ret = pcndLoadPsf(&ndim, &dims, data, path);
    if (PCND_OK == ret) {
        if (ndim < 2)
            ret = PCND_E_INVALIDARG;
        else {
            *w = dims[0];
            *h = dims[1];
        }
        pcndFree(dims);
    }

    return ret;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndSavePsf(
    int          ndim,
    const int*   dims,
    const float* data,
    const char*  path
    )
{
    if (ndim <= 0)
        return PCND_E_INVALIDARG;
    for (int i = 0; i < ndim; ++i)
        if (dims[i] <= 0) return PCND_E_INVALIDARG;

    FILE* fp = fopen(path, "wb");
    if (!fp)
        return PCND_E_IO;

    if (1 != pcndPutUInt(fp, PCND_PSF_MAGIC)) {
        fclose(fp);
        return PCND_E_IO;
    }

    if (1 != pcndPutInt(fp, ndim)) {
        fclose(fp);
        return PCND_E_IO;
    }

    size_t count = 1;
    for (int i = 0; i < ndim; ++i) {
        if (1 != pcndPutInt(fp, dims[i])) {
            fclose(fp);
            return PCND_E_IO;
        }
        count *= (size_t)dims[i];
    }

    if (count != fwrite(data, sizeof(float), count, fp)) {
        fclose(fp);
        return PCND_E_IO;
    }

    fclose(fp);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndSavePsf2(
    int          w,
    int          h,
    const float* data,
    const char*  path
    )
{
    int dims[2];
    dims[0] = w;
    dims[1] = h;
    return pcndSavePsf(2, dims, data, path);
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndGetPhasePsfRadius(
    int*  radius,
    float haloSigma,
    float haloSigmaRatio
    )
{
    if (NULL == radius)
        return PCND_E_INVALIDARG;
    *radius = _pcndGetPhasePsfRadius(haloSigma, haloSigmaRatio);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndGetPhasePsf(
    float* psf, int radius, bool invert,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio
    )
{
    if (NULL == psf)
        return PCND_E_INVALIDARG;
    int diameter = radius * 2 + 1;
    _pcndGetPhasePsf(
        psf, diameter, radius, invert,
        haloAmount, haloContrast, haloSigma, haloSigmaRatio);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndGetDicPsfRadius(int* radius, float softness) {
    if (NULL == radius)
        return PCND_E_INVALIDARG;
    *radius = _pcndGetDicPsfRadius(softness);
    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
PCNDRESULT PCNDAPI
pcndGetDicPsf(
    float* psf, int radius, bool invert,
    float softness, float angle
    )
{
    if (NULL == psf)
        return PCND_E_INVALIDARG;
    int diameter = radius * 2 + 1;
    _pcndGetDicPsf(psf, diameter, radius, invert, softness, angle);
    return PCND_OK;
}
