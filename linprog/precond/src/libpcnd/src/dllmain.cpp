/*
 ==========================================================================
 |
 |  $Id: dllmain.cpp 702 2009-05-06 18:10:36Z kangli $
 |
 |  Written by Kang Li <kangli@cs.cmu.edu>
 |
 ==========================================================================
 |  This file is a part of the Kang library.
 ==========================================================================
 |  Copyright (c) 2009 Kang Li <kangl@cmu.edu>. All rights reserved.
 |
 |  This software is supplied under the terms of a license agreement or
 |  nondisclosure agreement  with the author  and may not be copied  or
 |  disclosed except in accordance with the terms of that agreement.
 ==========================================================================
 */

#include "config.h"

#ifdef _WIN32
#   include <windows.h>

BOOL WINAPI
DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD     fdwReason, // reason for calling function
    LPVOID    lpReserved // reserved
    )
{
    // Perform actions based on the reason for calling.
    switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
        // Initialize once for each new process.
        break;

    default:
        break;
    }

    return TRUE;
}
#endif
