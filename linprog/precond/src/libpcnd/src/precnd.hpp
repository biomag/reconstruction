#ifndef ___PRECND_HPP___
#   define ___PRECND_HPP___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <libpcnd.h>
#   ifdef _WIN32
#       include <windows.h>
#   endif
#   include "models.hpp"
#   include "munnqp.hpp"


//////////////////////////////////////////////////////////////////////////
template <typename _Real>
inline PCNDRESULT
pcndPreconditionPhaseT(
    float* out,
    const float* in, int w, int h,
    int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    if (smoothness == 0.0)
        return PCND_E_INVALIDARG;

    try {
        SparseMatrixT<_Real> H, R;

#ifdef PCND_VERBOSE
        cout << "Creating phase contrast linear model..." << endl;
#endif
        _pcndCreatePhaseModel(
            H, R, (unsigned long)w, (unsigned long)h, invert, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio);

#ifdef PCND_VERBOSE
        cout << "Precomputing matrices..." << endl;
#endif
        SparseMatrixT<_Real> Ht(H.transposed());
        SparseMatrixT<_Real> A(H * Ht + R * R.transposed());
        VectorT<double>      b(-times(in, Ht));
        VectorRefT<float>    x(out, w * h);

#ifdef PCND_VERBOSE
        cout << "Optimizing..." << endl;
#endif

        _pcndMUNNQP(x, A, b, true, w, h, tag,
            (double)scale, (double)sparsity, (double)alpha,
            (double)tol, maxit, cancel, true, updatecb);
    }
    catch (exception& e) {
#ifdef PCND_VERBOSE
        cerr << "Exception: " << e.what() << endl;
#endif
        return PCND_E_FAILED;
    }

    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Real>
inline PCNDRESULT
pcndPreconditionPhaseBlockedT(
    float* out, const float* in, int w, int h,
    int blkw, int blkh, int tag, bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma,
    float haloSigmaRatio, float saturation, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    if (smoothness == 0.0)
        return PCND_E_INVALIDARG;

    CImg<float> imgIn(in, w, h, 1, 1, true); // shared
    CImg<float> imgOut(out, w, h, 1, 1, true); // shared
    CImg<float> imgDesat(in, w, h, 1, 1, false); // not shared

    if (!invert && saturation >= 0.0) {
        cimg_forXY(imgDesat, x, y) {
            if (imgDesat(x, y) >= saturation)
                imgDesat(x, y) = 0.0;
        }
    }

    if (!init && blkw > 0 && blkh > 0 && w >= blkw && h >= blkh) {

        init = true;

        try {

            SparseMatrixT<_Real> blkH, blkR;
            VectorT<float>       blkv(blkw * blkh);

            _pcndCreatePhaseModel(
                blkH, blkR, (unsigned long)blkw, (unsigned long)blkh, invert, smoothness,
                haloAmount, haloContrast, haloSigma, haloSigmaRatio);

            SparseMatrixT<_Real> blkHt(blkH.transposed());
            SparseMatrixT<_Real> blkA(blkH * blkHt + blkR * blkR.transposed());

            CImg<float> imgBlock(blkw, blkh);
            int x0, y0, x1, y1;

            VectorT<double> blkxp(blkv.size());
            VectorT<double> blkxm(blkv.size());
            VectorT<double> blkbb(blkv.size());

            // Loop through all blocks.
            for (y0 = 0; y0 + blkh <= h; y0 += blkh) {
                y1 = y0 + blkh - 1;

                for (x0 = 0; x0 + blkw <= w; x0 += blkw) {
                    x1 = x0 + blkw - 1;

                    imgBlock.draw_image(-x0, -y0, imgDesat);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];

                } // x0

                if (cancel && *cancel)
                    return PCND_OK;

                if (x0 != w) {

                    x0 = w - blkw;
                    x1 = w - 1;

                    imgBlock.draw_image(-x0, -y0, imgDesat);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];
                }

                if (NULL != updatecb)
                    (*updatecb)(out, scale, w, h, tag, -1, 0.0);

            } // y0

            if (y0 != h) {

                y0 = h - blkh;
                y1 = h - 1;

                for (x0 = 0; x0 + blkw <= w; x0 += blkw) {
                    x1 = x0 + blkw - 1;

                    imgBlock.draw_image(-x0, -y0, imgDesat);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];

                } // x0

                if (x0 != w) {

                    x0 = w - blkw;
                    x1 = w - 1;

                    imgBlock.draw_image(-x0, -y0, imgDesat);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];
                }

                if (NULL != updatecb)
                    (*updatecb)(out, scale, w, h, tag, -1, 0.0);

            }
        }
        catch (bad_alloc&  ) {
#ifdef PCND_VERBOSE
            cerr << "Could not allocate memory." << endl;
#endif
            return PCND_E_OUTOFMEMORY;
        }
        catch (exception& e) {
#ifdef PCND_VERBOSE
            cerr << "Exception: " << e.what() << endl;
#endif
            return PCND_E_FAILED;
        }
    }

    try {
        SparseMatrixT<_Real> H, R;

#ifdef PCND_VERBOSE
        cout << "Creating phase contrast linear model..." << endl;
#endif

        _pcndCreatePhaseModel(
            H, R, (unsigned long)w, (unsigned long)h, invert, smoothness,
            haloAmount, haloContrast, haloSigma, haloSigmaRatio);

#ifdef PCND_VERBOSE
        cout << "Precomputing matrices..." << endl;
#endif

        if (!init && !invert) {
            cimg_forXY(imgOut, x, y) {
                if (imgIn(x, y) >= saturation)
                    imgOut(x, y) = 0.0;
                else
                    imgOut(x, y) = 1.0;
            }
            init = true;
        }

        SparseMatrixT<_Real> Ht(H.transposed());
        SparseMatrixT<_Real> A(H * Ht + R * R.transposed());
        VectorT<double>      b(-times(imgDesat.data, Ht));
        VectorRefT<float>    v(out, w * h);

#ifdef PCND_VERBOSE
        cout << "Optimizing..." << endl;
#endif

        if (tol < 0)
            tol = 5e-4f;

        _pcndMUNNQP(v, A, b, !init, w, h, tag,
            (double)scale, (double)sparsity, (double)alpha,
            (double)tol, maxit, cancel, true, updatecb);
    }
    catch (bad_alloc&  ) {
#ifdef PCND_VERBOSE
        cerr << "Could not allocate memory." << endl;
#endif
        return PCND_E_OUTOFMEMORY;
    }
    catch (exception& e) {
#ifdef PCND_VERBOSE
        cerr << "Exception: " << e.what() << endl;
#endif
        return PCND_E_FAILED;
    }

    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Real>
inline PCNDRESULT
pcndPreconditionDicT(
    float* out, const float* in, int w, int h,
    int tag, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    if (smoothness == 0.0)
        return PCND_E_INVALIDARG;

    try {

        SparseMatrixT<_Real> H, R;

#ifdef PCND_VERBOSE
        cout << "Creating DIC linear model..." << endl;
#endif

        _pcndCreateDicModel(
            H, R, (unsigned long)w, (unsigned long)h, invert,
            smoothness, softness, angle);

#ifdef PCND_VERBOSE
        cout << "Precomputing matrices..." << endl;
#endif

        SparseMatrixT<_Real> Ht(H.transposed());
        SparseMatrixT<_Real> A(H * Ht + R * R.transposed());
        VectorT<double>      b(-times(in, Ht));
        VectorRefT<float>    v(out, w * h);

#ifdef PCND_VERBOSE
        cout << "Optimizing..." << endl;
#endif

        if (tol < 0)
            tol = 1e-4f;

        _pcndMUNNQP(v, A, b, true, w, h, tag,
            (double)scale, (double)sparsity, (double)alpha,
            (double)tol, maxit, cancel, true, updatecb);
    }
    catch (exception& e) {
#ifdef PCND_VERBOSE
        cerr << "Exception: " << e.what() << endl;
#endif
        return PCND_E_FAILED;
    }

    return PCND_OK;
}


//////////////////////////////////////////////////////////////////////////
template <typename _Real>
inline PCNDRESULT
pcndPreconditionDicBlockedT(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness, float softness, float angle,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    )
{
    if (smoothness == 0.0)
        return PCND_E_INVALIDARG;

    if (!init && blkw > 0 && blkh > 0 && w >= blkw && h >= blkh) {

        init = true;

        try {

            SparseMatrixT<_Real> blkH, blkR;
            VectorT<float>       blkv(blkw * blkh);

            _pcndCreateDicModel(
                blkH, blkR, (unsigned long)blkw, (unsigned long)blkh, invert,
                smoothness, softness, angle);

            SparseMatrixT<_Real> blkHt(blkH.transposed());
            SparseMatrixT<_Real> blkA(blkH * blkHt + blkR * blkR.transposed());

            CImg<float> imgIn(in, w, h, 1, 1, true);
            CImg<float> imgOut(out, w, h, 1, 1, true);
            CImg<float> imgBlock(blkw, blkh);
            int x0, y0, x1, y1;

            VectorT<double> blkxp(blkv.size());
            VectorT<double> blkxm(blkv.size());
            VectorT<double> blkbb(blkv.size());

            // Loop through all blocks.
            for (y0 = 0; y0 + blkh <= h; y0 += blkh) {
                y1 = y0 + blkh - 1;

                for (x0 = 0; x0 + blkw <= w; x0 += blkw) {
                    x1 = x0 + blkw - 1;

                    imgBlock.draw_image(-x0, -y0, imgIn);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];

                    if (NULL != updatecb)
                        (*updatecb)(out, scale, w, h, tag, -1, 0.0);

                } // x0

                if (cancel && *cancel)
                    return PCND_OK;

                if (x0 != w) {

                    x0 = w - blkw;
                    x1 = w - 1;

                    imgBlock.draw_image(-x0, -y0, imgIn);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];
                }

                if (NULL != updatecb)
                    (*updatecb)(out, scale, w, h, tag, -1, 0.0);

            } // y0

            if (y0 != h) {

                y0 = h - blkh;
                y1 = h - 1;

                for (x0 = 0; x0 + blkw <= w; x0 += blkw) {
                    x1 = x0 + blkw - 1;

                    imgBlock.draw_image(-x0, -y0, imgIn);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];

                } // x0

                if (x0 != w) {

                    x0 = w - blkw;
                    x1 = w - 1;

                    imgBlock.draw_image(-x0, -y0, imgIn);
                    VectorT<double> blkb(-times(imgBlock.data, blkHt));

                    /*
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            blkv[i++] = imgOut(x, y);
                    */

                    // Optimize.
                    _pcndMUNNQP(blkv, blkA, blkb, blkbb, blkxp, blkxm, true,
                        blkw, blkh, tag, (double)scale, (double)sparsity, (double)alpha,
                        (double)tol, maxit, cancel, false, NULL);

                    if (cancel && *cancel)
                        return PCND_OK;

                    // Assign result to output.
                    for (int y = y0, i = 0; y <= y1; ++y)
                        for (int x = x0; x <= x1; ++x)
                            imgOut(x, y) = blkv[i++];
                }

                if (NULL != updatecb)
                    (*updatecb)(out, scale, w, h, tag, -1, 0.0);

            }

            // Reduce blocky artifact by blurring.
            imgOut.blur(smoothness);
        }
        catch (bad_alloc&  ) {
#ifdef PCND_VERBOSE
            cerr << "Could not allocate memory." << endl;
#endif
            return PCND_E_OUTOFMEMORY;
        }
        catch (exception& e) {
#ifdef PCND_VERBOSE
            cerr << "Exception: " << e.what() << endl;
#endif
            return PCND_E_FAILED;
        }

    }

    try {
        SparseMatrixT<_Real> H, R;

#ifdef PCND_VERBOSE
        cout << "Creating DIC linear model..." << endl;
#endif
        _pcndCreateDicModel(
            H, R, (unsigned long)w, (unsigned long)h, invert,
            smoothness, softness, angle);

#ifdef PCND_VERBOSE
        cout << "Precomputing matrices..." << endl;
#endif

        SparseMatrixT<_Real> Ht(H.transposed());
        SparseMatrixT<_Real> A(H * Ht + R * R.transposed());
        VectorT<double>      b(-times(in, Ht));
        VectorRefT<float>    v(out, w * h);

#ifdef PCND_VERBOSE
        cout << "Optimizing..." << endl;
#endif

        if (tol < 0)
            tol = 5e-4f;

        _pcndMUNNQP(v, A, b, !init, w, h, tag,
            (double)scale, (double)sparsity, (double)alpha,
            (double)tol, maxit, cancel, true, updatecb);
    }
    catch (bad_alloc&  ) {
#ifdef PCND_VERBOSE
        cerr << "Could not allocate memory." << endl;
#endif
        return PCND_E_OUTOFMEMORY;
    }
    catch (exception& e) {
#ifdef PCND_VERBOSE
        cerr << "Exception: " << e.what() << endl;
#endif
        return PCND_E_FAILED;
    }

    return PCND_OK;
}

//////////////////////////////////////////////////////////////////////////
template <typename _Real>
inline PCNDRESULT
pcndPreconditionWithPsfT(
    float* out, const float* in, int w, int h, int tag,
    const float* psf, int psfw, int psfh, bool init, bool invert,
    int maxit, float tol, float scale, float alpha,
    float sparsity, float smoothness,
    int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    )
{
    if (smoothness == 0.0 ||
        NULL == out || NULL == in || NULL == psf)
        return PCND_E_INVALIDARG;

    try {
        SparseMatrixT<_Real> H, R;

#ifdef PCND_VERBOSE
        cout << "Creating linear model from PSF..." << endl;
#endif
        _pcndCreateModelWithPsf(
            H, R, psf, psfw, psfh, (unsigned long)w, (unsigned long)h,
            invert, smoothness);

#ifdef PCND_VERBOSE
        cout << "Precomputing matrices..." << endl;
#endif
        SparseMatrixT<_Real> Ht(H.transposed());
        SparseMatrixT<_Real> A(H * Ht + R * R.transposed());
        VectorT<double>      b(-times(in, Ht));
        VectorRefT<float>    x(out, w * h);

#ifdef PCND_VERBOSE
        cout << "Optimizing..." << endl;
#endif

        _pcndMUNNQP(x, A, b, !init, w, h, tag,
            (double)scale, (double)sparsity, (double)alpha,
            (double)tol, maxit, cancel, true, updatecb);
    }
    catch (exception& e) {
#ifdef PCND_VERBOSE
        cerr << "Exception: " << e.what() << endl;
#endif
        return PCND_E_FAILED;
    }

    return PCND_OK;
}


#endif //___PRECND_HPP___
