#ifndef ___LIBPCND_H___
#   define ___LIBPCND_H___

#   if defined(_MSC_VER) && (_MSC_VER > 1000)
#       pragma once
#   endif

#   include <stddef.h>

#   ifdef _USRDLL
#       ifdef LIBPCND_EXPORTS
#           define PCNDAPI __declspec(dllexport)
#       else
#           define PCNDAPI __declspec(dllimport)
#       endif
#   else
#       define PCNDAPI
#   endif

#   ifndef M_PI
#       define M_PI 3.14159265358979323846
#   endif

/**************************************************************************
 * Return Codes
 **************************************************************************/
typedef enum _PCNDRESULT {
    PCND_OK                = 0,
    PCND_E_FAILED          = 1,
    PCND_E_NOTIMPL         = 2,
    PCND_E_INVALIDARG      = 3,
    PCND_E_INVALIDFORMAT   = 4,
    PCND_E_ACCESSVIOLATION = 5,
    PCND_E_OUTOFMEMORY     = 6,
    PCND_E_NUMERICAL       = 7,
    PCND_E_DEVICE          = 8,
    PCND_E_IO              = 9,
    PCND_UNKNOWN
} PCNDRESULT;

/**************************************************************************
 * Call-Back Functions
 **************************************************************************/
typedef void (*PCNDCALLBACK_UPDATE)(
    const float* data, double scale, int w, int h, int tag,
    int iter, double delta);

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/
#   ifdef __cplusplus
extern "C"
{
#   endif

PCNDRESULT PCNDAPI pcndGetVersion(int* major,
                                  int* minor,
                                  int* magic,
                                  int* build);

PCNDRESULT PCNDAPI pcndGetErrorString(
    PCNDRESULT result, char* buffer, size_t size);

PCNDRESULT PCNDAPI pcndInitGpu();
PCNDRESULT PCNDAPI pcndShutdownGpu();

PCNDRESULT PCNDAPI
pcndFitSurfaceGpu(
    float* out,
    const float* in, int w, int h,
    int xxOrder, int yyOrder, int xyOrder
    );


PCNDRESULT PCNDAPI pcndSetThreadCount(int count);

/* Memory Management */
PCNDAPI void* pcndAlloc(size_t sizeInBytes);
PCNDAPI void* pcndCalloc(size_t count, size_t elementSize);
PCNDAPI void  pcndFree(void* ptr);


/* Filtering & Surface Fitting */
PCNDRESULT PCNDAPI
pcndSmooth(
    float*       out,
    const float* in,
    int          w,
    int          h,
    int          n,
    float        sigma
    );

PCNDRESULT PCNDAPI
pcndFitSurface(
    float* out,
    const float* in, int w, int h,
    int xxOrder, int yyOrder, int xyOrder
    );

/* Point Spread Function (PSF) Management */
PCNDRESULT PCNDAPI pcndIsPsfFile(const char* path);

PCNDRESULT PCNDAPI
pcndLoadPsf(
    int*         ndim,
    int**        dims,
    float**      data,
    const char*  path);

PCNDRESULT PCNDAPI
pcndLoadPsf2(
    int*         w,
    int*         h,
    float**      data,
    const char*  path);

PCNDRESULT PCNDAPI
pcndSavePsf(
    int          ndim,
    const int*   dims,
    const float* data,
    const char*  path
    );

PCNDRESULT PCNDAPI
pcndSavePsf2(
    int          w,
    int          h,
    const float* data,
    const char*  path
    );

PCNDRESULT PCNDAPI
pcndGetPhasePsfRadius(
    int*  radius,
    float haloSigma,
    float haloSigmaRatio
    );

PCNDRESULT PCNDAPI
pcndGetPhasePsf(
    float* psf, int radius, bool invert,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio
    );

/* Preconditioning */
PCNDRESULT PCNDAPI
pcndPreconditionPhase(
    float* out, const float* in, int w, int h, int tag,
    bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionPhaseDbl(
    float* out, const float* in, int w, int h, int tag,
    bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionPhaseBlocked(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    float saturation,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionPhaseBlockedDbl(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    float saturation,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionPhaseFft(
    float* out, const float* in,
    int w, int h, int tag, bool invert, float smoothness,
    float haloAmount, float haloContrast, float haloSigma, float haloSigmaRatio,
    int* cancel, PCNDCALLBACK_UPDATE updatecb
    );


PCNDRESULT PCNDAPI
pcndGetDicPsfRadius(int* radius, float softness);

PCNDRESULT PCNDAPI
pcndGetDicPsf(
    float* psf, int radius, bool invert,
    float softness, float angle
    );

PCNDRESULT PCNDAPI
pcndPreconditionDic(
    float* out, const float* in, int w, int h, int tag,
    bool invert, int maxit, float tol, float scale,
    float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionDicDbl(
    float* out, const float* in, int w, int h, int tag,
    bool invert, int maxit, float tol, float scale,
    float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionDicBlocked(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionDicBlockedDbl(
    float* out, const float* in,
    int w, int h, int blkw, int blkh, int tag,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionDicFft(
    float* out, const float* in,
    int w, int h, int tag, bool invert,
    float smoothness, float softness, float angle, int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );


PCNDRESULT PCNDAPI
pcndPreconditionWithPsf(
    float* out, const float* in,
    int w, int h, int tag,
    const float* psf, int psfw, int psfh,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );

PCNDRESULT PCNDAPI
pcndPreconditionWithPsfDbl(
    float* out, const float* in,
    int w, int h, int tag,
    const float* psf, int psfw, int psfh,
    bool init, bool invert, int maxit, float tol,
    float scale, float alpha, float sparsity, float smoothness,
    int* cancel,
    PCNDCALLBACK_UPDATE updatecb
    );


#   ifdef __cplusplus
}
#   endif


#endif //___LIBPCND_H___
