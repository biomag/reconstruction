function [f, g, H] = gaussianloss (w, X, y)
  m = size(X,2);      % The number of features.
  K = length(w) - m;  % The number of groups.
  w = w(1:m);

  % Compute some useful quantities.
  XX  = X.'*X;
  XXw = XX*w;
  Xy  = X.'*y;
  
  % Compute the response of the objective function.
  f = w.'*XXw - 2*w.'*Xy + y.'*y;

  % Compute the gradient of the objective function.
  g      = zeros(m + K,1);
  g(1:m) = 2*XXw - 2*Xy;

  % Compute the Hessian of the objective function.
  H          = zeros(m + K);
  H(1:m,1:m) = 2*XX;
