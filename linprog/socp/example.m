% This little script shows how to formulate the "group feature selection"
% problem as a second-order cone program, and solve it using the MATLAB
% implementation of the primal-dual interior-point method. For more
% information on this problem, see:
%
%   Simila and Tikka (2007). Input selection and shrinkage in
%   multiresponse linear regression. Computational Statistics
%   and data analysis. Computational Statistics and Data Analysis,
%   Volume 52, Issue 1, pp. 406--422.
%
%                                         Peter Carbonetto and Mark Schmidt
%                                         Dept. of Computer Science
%                                         University of British Columbia
%                                         Copyright 2008
%
clear

% This controls for the level of L1 regularization (i.e. sparsity).
lambda = 200;

% GENERATE THE DATA
% Generate the "true" regression coefficients and input vectors from the
% standard normal.
n      = 100;                    % The number of training examples.
m      = 10;                     % Number of features.
K      = 3;                      % The number of groups.
X      = randn(n,m);             % The matrix of training examples.
groups = [1 1 1 1 2 2 2 3 3 3]'; % Group membership of features.
beta   = randn(m,1);             % The "true" regression coefficients.

beta(groups == 1) = 0;

% Generate the outputs.
y = X*beta;
y = y - mean(y);

% SOLVE THE "GROUP SELECTION" CONSTRAINTED OPTIMIZATION
% Set up the second-order constraints.
for k = 1:K
    t      = zeros(m + K,1);
    t(1:m) = groups == k;
    A{k}   = diag(sparse(t));
    t      = zeros(K,1);
    t(k)   = 1;
    p{k}   = [zeros(m,1); t];
end

% Set up the first-order constraint.
q = [ zeros(m,1);
      ones(K,1) ];
b = lambda;

% The starting point.
x0 = [ zeros(m,1); 
       ones(K,1) ];

% Call the primal-dual interior-point solver.
w = socpsolver(A,p,q,b,x0,@(w) gaussianloss(w,X,y),100,1e-6,true);
w = w(1:m);

% Compare the estimated coefficients with the true regression
% coefficients.
disp(' ');
disp('      True | Estimated');
disp([beta w]);
