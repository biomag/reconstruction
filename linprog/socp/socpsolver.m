% SOCPSOLVER(A,P,Q,B,X0,FUNC,MAXITER,TOL,VERBOSE) attempts to solve the
% following quadratically constrained convex program using a primal-dual
% interior-point method:
%
%   minimize    f(x)
%   subject to  ||Ax|| < p'x,
%               q'x < b,
%
% where ||x|| is the L2-norm of x. This is a special form of second-orer
% cone program (see Sec. 4.4.2 of the book by Boyd and Vandenberghe). It is
% assumed that the objective function is convex (i.e. the Hessian is
% symmetric positive-definite). There may be many inequalities of the first
% type. Note that the interior-point method transforms the problem slightly
% by squaring both sides of the inequality constraints like so: 
% ||Ax||^2 < (p'x)^2.
%
% The input X0 is the initial point for the solver. It must be an n x 1
% matrix, where n is the number of (primal) optimization variables.
%
% The inputs Q and B specify the linear constraint; B should be a single
% number and Q should be an n x 1 vector, where n is equal to the number
% of optimization variables.
%
% The inputs A and P specify the quadratic norm constraints. Each of
% these inputs must be a cell matrix of length equal to the number of
% these types of constraints. Each cell entry A{i} should be an n x n
% matrix, and each cell entry p{i} should be an n x 1 vector.
%
% The inputs FUNC must be a function handle. If you don't know what function
% handles are, type HELP FUNCTION_HANDLE in MATLAB. FUNC must be a handle to
% a function that takes one input, the vector of optimization variables, and
% outputs three quantities: the response of the objective at the current
% point, the n x 1 gradient of the objective, and the n x n Hessian of the
% objective. 
%
% MAXITER tells the solver to stop when it reaches this number of
% iterations. TOL is the tolerance of the convergence criterion; it
% determines when the solver should stop. The final input, VERBOSE, must be
% set to true or false depending on whether you would like to see the
% progress of the solver.
%
%                                         Peter Carbonetto
%                                         Dept. of Computer Science
%                                         University of British Columbia
%                                         Copyright 2008

function x = socpsolver (A, p, q, b, x, func, maxiter, tolerance, verbose)
  
  % Some algorithm parameters.
  sigmamax    = 0.5;    % The maximum centering parameter.
  etamax      = 0.25;   % The maximum forcing number.
  mumin       = 1e-9;   % Minimum barrier parameter.
  alphamax    = 0.995;  % Maximum step size.
  alphamin    = 1e-6;   % Minimum step size.
  beta        = 0.75;   % Granularity of backtracking search.
  tau         = 0.01;   % Amount of actual decrease we will accept in 
                        % line search.
  
  % Get the number of primal variables (n) and inequality constraints
  % (m). The third quantity is the total number of (primal and dual)
  % variables. Also, initialize the Lagrange multipliers.
  n  = length(x);
  m  = length(p) + 1;
  nv = n + m;
  z  = ones(m,1);
			
  if verbose
    fprintf('  i f(x)       lg(mu) sigma   ||rx||  ||rc||  alpha   #ls\n');
  end

  % Repeat while the convergence criterion has not been satisfied, and
  % we haven't reached the maximum number of iterations.
  alpha = 0;
  ls    = 0;
  for iter = 1:maxiter

    % Compute the response of the objective, gradient of the objective, the
    % response of the inequality constraints, the Jacobian of the inequality
    % constraints, and the Hessian of the Lagrangian at the current point.
    [f g H] = func(x);
    c       = constraints(A,p,q,b,x);
    J       = jacobian(A,p,q,x);
    H       = hessian(A,p,x,z,H);

    % Compute the responses of the unperturbed Karush-Kuhn-Tucker
    % optimality conditions.
    [rx rc] = residual(z,g,c,J,0);
    r0      = [rx; rc]; 
    
    % Set some parameters that affect convergence of the interior-point
    % method. This particular forcing sequence should ensure superlinear (or
    % quadratic?) convergence. See p. 572 of Nocedal and Wright (2006). We
    % use the mu update rule described in Boyd and Vandenberghe (2004) and
    % Bellavia (1998). Here we divide sigma and eta by the number of
    % variables to ensure that it doesn't depend on the problem size. Also,
    % note that the duality gap has a negative sign here because the
    % constraints are less than or equal to zero; see p. 612 of Boyd and
    % Vandenberghe (2004).
    eta   = min(etamax,norm(r0)/nv);
    sigma = min(sigmamax,sqrt(norm(r0)/nv));
    mu    = max(mumin,sigma*(-c'*z)/m);
    
    % Print the status of the algorithm.
    if verbose
      fprintf('%3d %+0.3e  %+5.2f %0.1e %0.1e %0.1e %0.1e %3d\n',...
	      iter,f,log10(mu),sigma,norm(rx),norm(rc),alpha,ls);
    end

    % CONVERGENCE CHECK.
    % If the norm of the responses is less than the specified tolerance,
    % we are done. This is the convergence criterion used by Yamashita
    % and Yabe (1998).
    if norm(r0)/nv < tolerance
      break
    end
  
    % SOLUTION TO PERTURBED KKT SYSTEM.
    % Compute the perturbed residual.
    rc = rc + mu;
    r  = [rx; rc];

    % Compute the search direction of x and z.
    S  = diag(sparse(z./c));
    gb = g - mu*J'*(1./c);
    px = (H - J'*S*J) \ (-gb);
    pz = -(z + mu./c + S*J*px);
    
    % BACKTRACKING LINE SEARCH.
    % To ensure global convergence, execute backtracking line search to
    % determine the step length. First, we have to find the largest step
    % size which ensures that z remains feasible. Next, we perform
    % backtracking line search.
    alpha = alphamax;
    is    = find(z + pz < 0);
    if length(is)
      alpha = alphamax * min(1,min(z(is) ./ -pz(is)));
    end

    ls = 0;
    while true

      % Compute the candidate point, compute the response of the
      % vector-valued constraint function, and compute the response of
      % the residuals.
      xnew      = x + alpha * px;
      znew      = z + alpha * pz;
      c         = constraints(A,p,q,b,xnew);
      J         = jacobian(A,p,q,xnew);
      [f g ans] = func(xnew);
      rnew      = residual(znew,g,c,J,mu);
      ls        = ls + 1;

      % Stop backtracking search if we've found a candidate point that
      % sufficiently decreases the norm of the residual, and is feasible.
      if sum(c > 0) == 0 & norm(rnew) <= (1 - tau*alpha*(1-eta)) * norm(r)
	x = xnew;
	z = znew;
	break
      end
      
      % The candidate point does not meet our criteria, so decrease the step
      % size for 0 < beta < 1.
      alpha = alpha * beta;
      if alpha < alphamin
	error('Step size too small');
      end
    end
  end

% ------------------------------------------------------------------
% Compute the response of the vector-valued constraint function.
function c = constraints (A, p, q, b, x)

  % Initialize the return value.
  m = length(p);
  c = zeros(m+1,1);
  
  % Repeat for each constraint of the form ||Ax|| < p'x.
  for i = 1:m
    c(i) = norm(A{i}*x)^2 - (p{i}'*x)^2;
  end

  % Compute the constraint r'x < b.
  c(m+1) = q'*x - b;

% ----------------------------------------------------------------------
% Compute the Jacobian of the vector-valued constraint function.
function J = jacobian (A, p, q, x)

  % Initialize the return value.
  n = length(x);
  m = length(p);
  J = zeros(n,m+1);
  
  % Repeat for each constraint of the form ||Ax|| < p'x.
  for i = 1:m
    J(:,i) = 2*A{i}'*(A{i}*x) - 2*p{i}*(p{i}'*x);
  end

  % Compute the partial derivatives of the constraint r'x < b.
  J(:,m+1) = q;
  
  J = J';
  
% ----------------------------------------------------------------------
% Compute the Hessian of the Lagrangian. Input H is Hessian of objective.
function H = hessian (A, p, x, z, H)
  
  % Compute the Hessian of each quadratic inequality constraint.
  m = length(p);
  for i = 1:m
    H = H + z(i) * (2*A{i}'*A{i} - 2*p{i}*p{i}');
  end

% ----------------------------------------------------------------------
% Compute the residual of the perturbed Karush-Kuhn-Tucker system.
function varargout = residual (z, g, c, J, mu)
  rx = g + J'*z;   % Dual residual.
  rc = c.*z + mu;  % Complementarity.

  if nargout == 1
    varargout = { [rx; rc] };
  else
    varargout = { rx rc };
  end
