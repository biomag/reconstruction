#!/bin/bash
folder=~/dic_reconstruction_build/
if [ "$#" -ne 0 ]; then
	folder="$1"
fi
case "$folder" in
*/)
    folder=${folder%/}
    ;;
esac

rm -rf $folder
mkdir $folder
echo "Building to $folder"
# copy folders and files
cp startup_for_public_build.m "$folder/startup.m"
cp main_gui.m "$folder"
mkdir "$folder/src"
cp src/dicCvxSocp.m src/dicEMM.m src/dicFeineigle.m src/dicHilbert.m src/dicWiener.m src/dicZhaozhengYin.m src/precondWinWrapper.m src/initInputImage.m "$folder/src"
mkdir "$folder/src/kernel"
cp src/kernel/getGaussDeriv.m src/kernel/getPsfApproximation.m "$folder/src/kernel"
mkdir "$folder/src/gui"
cp src/gui/dic_gui.fig src/gui/dic_gui.m src/gui/dic_gui_control.m "$folder/src/gui"
mkdir "$folder/linprog"
mkdir "$folder/linprog/precond"
mkdir "$folder/linprog/cvx30"
mkdir "$folder/linprog/scs"
mkdir "$folder/util"
cp util/getCDiffKernel.m util/getConvMtx.m util/getDivergence.m util/getDivergence2.m "$folder/util"
cp readme_for_public.txt "$folder/readme.txt"
cp license.txt "$folder"

echo "Done."
