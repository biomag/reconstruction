function f = dicCvxSocpPartitioner( g, varargin )
%DICCVXSOCPPARTITIONER Partition image and reconstruct by SOCP

%% parse inputs
p = inputParser;
defaultPartitionSize = 100;
defaultOverlap = 2;
defaultPsf = -1;
defaultBeta = 10^-4;
defaultTau = 10^-3;
defaultDirection = 0;
defaultNumiter = 20000;

addParameter(p, 'partitionSize', defaultPartitionSize, @isnumeric);
addParameter(p, 'overlap', defaultOverlap, @isnumeric);
addParameter(p, 'psf', defaultPsf, @isnumeric);
addParameter(p, 'beta', defaultBeta, @isnumeric); % sparsity
addParameter(p, 'tau', defaultTau, @isnumeric);   % smoothness
addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numiter', defaultNumiter, @isnumeric);
parse(p, varargin{:});

partitionSize = p.Results.partitionSize;
overlap = p.Results.overlap;
psf = p.Results.psf;
beta = p.Results.beta;
tau = p.Results.tau;
direction = p.Results.direction;
numiter = p.Results.numiter;

%% initialization
g = initInputImage(g);
[m, n] = size(g);
if min(m, n) < partitionSize
    partitionSize = min(m, n);
end
mIter = ceil((m-partitionSize+1)/(partitionSize-overlap))+1;
nIter = ceil((n-partitionSize+1)/(partitionSize-overlap))+1;
f = zeros(m, n);
for i = 1:mIter
    uOverlap = overlap;
    if i == 1
        uOverlap = 0;
    end
    for j = 1:nIter
        disp([num2str(((i-1)*nIter+j)/(mIter*nIter)*100), '%']); %TODO times nIter or mIter
        vOverlap = overlap;
        if j == 1
            vOverlap = 0;
        end
        u1 = (i-1)*(partitionSize - uOverlap) + 1;
        v1 = (j-1)*(partitionSize - vOverlap) + 1;
        u2 = u1 + partitionSize - 1;
        v2 = v1 + partitionSize - 1;
        if u2 > m
            u2 = m;
            u1prev = u1;
            u1 = u2 - partitionSize + 1;
            uOverlap = uOverlap + u1prev - u1;
        end
        if v2 > n
            v2 = n;
            v1prev = v1;
            v1 = v2 - partitionSize + 1;
            vOverlap = vOverlap + v1prev - v1;
        end
        tic
        result = dicCvxSocp(g(u1:u2, v1:v2), 'psf', psf, 'beta', beta, 'tau', tau, 'direction', direction', ...
            'numiter', numiter);
        time = toc
        f(u1+uOverlap:u2, v1+vOverlap:v2) = f(u1+uOverlap:u2, v1+vOverlap:v2) + result(uOverlap+1:end, vOverlap+1:end);
        f(u1:u1+uOverlap-1, v1:v2) = min(f(u1:u1+uOverlap-1, v1:v2), result(1:uOverlap, :));
        f(u1:u2, v1:v1+vOverlap-1) = min(f(u1:u2, v1:v1+vOverlap-1), result(:, 1:vOverlap));
    end
end

end