function r = dicWiener( I, varargin )
%DICWIENER DIC image reconstruction using Wiener filter

I = initInputImage(I);
%% parse inputs
p = inputParser;
defaultNSR = 0.01;
defaultDirection = 0;
defaultKernelSigma = 0.5;
defaultPsf = -1;

addParameter(p, 'NSR', defaultNSR, @isnumeric);
addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'psf', defaultPsf, @(A) isnumeric(A) || ischar(A));
parse(p, varargin{:});

NSR = p.Results.NSR;
direction = p.Results.direction;
kernelSigma = p.Results.kernelSigma;
psf = p.Results.psf;

if numel(psf)==1 && psf==-1
    psf = getGaussDeriv('sigma', kernelSigma, 'direction', direction);
end
psf = psf./sum(abs(psf(:)));

%% normalize DIC image
delPhi = -I;

%% using Matlab wiener filter
r = deconvwnr(delPhi, psf, NSR);

end



%% custom wiener filter: sharper, maybe a bit worse
% for i=1:fs
%     for j=1:fs
%         PSF(i, j) = 2*1i*sin(pi*((i-ceil(fs/2))*1.75+(j-ceil(fs/2))*5.25)); 
%     end
% end
% 
% s = 0.1;
% SN = zeros(size(PSF));
% W  = zeros(size(PSF));
% for i = 1:size(PSF,1)
%     for j = 1:size(PSF,2)
%         SN(i,j) = s*exp(-2*pi^2*sigma^2*((i-ceil(fs/2))^2+(j-ceil(fs/2))^2));
%         W(i,j)  = PSF(j,i)/(abs(PSF(i,j))^2+1/SN(i,j)); % note G is transposed
%     end
% end
% [m, n] = size(delPhi);
% DelPhi = fft2(delPhi, m, n);
% Wt = fft2(W, m, n);
% Phi = DelPhi.*Wt;
% phi = ifft2(Phi);
% r = phi;