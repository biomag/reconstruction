function f = dicEMMgs( gOrig, varargin )
%DICEMM DIC deconvolution using our EMM and Gauss-Seidel iteration
%   central and intermediate differences are both used
%   kernel-less version (easier to implement)

%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultNumIter = 400;
defaultPyrSize = 3;
defaultWSmooth = 0.0125;
defaultWKeepCl = 0;
defaultWAccept = 0.025;
defaultKernelSize = 5;
defaultKernelSigma = 1.5;
defaultCustomKernel = -1;
defaultShow = false;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numIter', defaultNumIter, @isnumeric);
addParameter(p, 'pyrSize', defaultPyrSize, @(x) isnumeric(x) && x>0 && x<=4);
addParameter(p, 'wSmooth', defaultWSmooth, @isnumeric);
addParameter(p, 'wKeepCl', defaultWKeepCl, @isnumeric);
addParameter(p, 'wAccept', defaultWAccept, @isnumeric);
addParameter(p, 'kernelSize', defaultKernelSize, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'customKernel', defaultCustomKernel, @isnumeric);
addParameter(p, 'show', defaultShow, @islogical);
parse(p, varargin{:});

direction = p.Results.direction;
numIter = p.Results.numIter;
pyrSize = p.Results.pyrSize;
wSmooth = p.Results.wSmooth;
wKeepCl = p.Results.wKeepCl;
wAccept = p.Results.wAccept;
kernelSize = p.Results.kernelSize;
kernelSigma = p.Results.kernelSigma;
customKernel = p.Results.customKernel;
show = p.Results.show;

%% preprocessing
directionRad = direction*pi/180;
dirx = cos(directionRad);
diry = sin(directionRad);
gOrig = gOrig - mean(gOrig(:));
if show
    figure
    subplot(1,3,1), imshow(mat2gray(gOrig))
end
dx = getCDiffKernel(1,0);
dy = getCDiffKernel(0,1);
% dxx = getCDiffKernel(2,0);
% dxy = getCDiffKernel(1,1);
% dyy = getCDiffKernel(0,2);

if numel(customKernel)==1 && customKernel==-1
    K = fspecial('gaussian', [kernelSize kernelSize], kernelSigma);
else
    K = customKernel;
end

borderOpt = 'replicate';

%% reconstruction
for pyramidLevel = pyrSize-1:-1:0
    g = gOrig;
    for i = 1:pyramidLevel
        g = impyramid(g, 'reduce');
    end
    if pyramidLevel==pyrSize-1
        f = zeros(size(g));
    else
        f = impyramid(f, 'expand');
        [mg, ng] = size(g);
        [mf, nf] = size(f);
        if mf<mg
            f(end+1, :) = f(end, :); %#ok<AGROW>
        end
        if nf<ng
            f(:, end+1) = f(:, end); %#ok<AGROW>
        end
    end
    gx = imfilter(g, dx, borderOpt);
    gy = imfilter(g, dy, borderOpt);
    diff = dirx*gx + diry*gy;
    fPrev = f;
    [m, n] = size(f);
    for iter = 1:numIter/(pyrSize-pyramidLevel);
        for i = 2:m-1
            for j = 2:n-1
%                 fx = (f(i,j+1) - f(i,j-1))/2;
%                 fy = (f(i+1,j) - f(i-1,j))/2;
                fxx = f(i,j+1) - 2*f(i,j) + f(i,j-1);
                fyy = f(i+1,j) - 2*f(i,j) + f(i-1,j);
                fxy = (f(i-1,j-1) - f(i+1,j-1) - f(i-1,j+1) + f(i+1,j+1))/4;
                fx1 = (f(i-1,j) - f(i,j))/2;
                fx2 = (f(i,j) - f(i+1,j))/2;
                fy1 = (f(i,j-1) - f(i,j))/2;
                fy2 = (f(i,j) - f(i,j+1))/2;
%                 divdata = [fx1 fx2 fy1 fy2];
                magn = sqrt([fx1*fx1 + fy1*fy1, fx2*fx2 + fy2*fy2]) + eps;
                ddn = [fx1/magn(1), fx2/magn(2), fy1/magn(1), fy2/magn(2)];
                div = sum(ddn)/4;
                der = dirx^2*fxx + 2*dirx*diry*fxy + diry^2*fyy;
                addDiv = wSmooth*div;
                addAll = der - diff(i,j) + addDiv;
                if pyramidLevel~=pyrSize-1
                    addAll = addAll + wKeepCl*(fPrev(i,j)-f(i,j));
                end
                f(i,j) = f(i,j) + wAccept*addAll;
                % indexekkel kell szoszmotolni... a borderopts-ot sem tudom
                % igy kenyelmesen allitani. ha centralis
                % differenciakat szamolok, akkor nagyon sok lokalis
                % szamitast kell vegezni. meg ha kevesebb kulso iteracio is
                % szukseges a jo eredmenykephez, szamitasban ez soknak
                % hangzik. ha meg intermediate difference-t szamitok, akkor
                % mar ugy erzem, nem ugyanaz lesz a szamitas. persze
                % tudjuk, hogy tul nagy kulonbseg nincs kozte, de nem
                % tetszik ez a fajta osszehasonlitas.
            end
        end
        
        
%         fx = imfilter(f, dx, borderOpt);
%         fy = imfilter(f, dy, borderOpt);
%         fxx = imfilter(f, dxx, borderOpt);
%         fxy = imfilter(f, dxy, borderOpt);
%         fyy = imfilter(f, dyy, borderOpt);
%         
%         fdMagnitude = sqrt(fx.*fx + fy.*fy + eps);
%         fxNorm = fx./fdMagnitude;
%         fyNorm = fy./fdMagnitude;
%         div = getDivergence2(fxNorm, fyNorm, borderOpt);
% 
%         der = dirx^2*fxx + 2*dirx*diry*fxy + diry^2*fyy;
%         addDiv = wSmooth*div;
%         addAll = imfilter(der, K, borderOpt) - diff + addDiv;
%         if pyramidLevel~=pyrSize-1
%             addAll = addAll + wKeepCl*(fPrev-f);
%         end
%         f = f + wAccept*addAll;
        %% show the result in each iteration
        if show
            subplot(1,3,2), imagesc(imadjust(f)), colorbar
            subplot(1,3,3), imagesc(addAll), colorbar;
            drawnow
        end
    end
end

end
