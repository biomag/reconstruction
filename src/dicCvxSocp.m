function [f, cvx_optval, cvx_status] = dicCvxSocp( g, varargin )
%DICCVXSOCP DIC reconstruction using SOCP by CVX toolbox
% Requires MODIFIED cvx_solve.m
%

%% parse inputs
p = inputParser;
defaultPsf = -1;
defaultBeta = 10^-4;
defaultTau = 10^-3;
defaultDirection = 0;
defaultNumiter = 20000;

addParameter(p, 'psf', defaultPsf, @isnumeric);
addParameter(p, 'beta', defaultBeta, @isnumeric); % sparsity
addParameter(p, 'tau', defaultTau, @isnumeric);   % smoothness
addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numiter', defaultNumiter, @isnumeric);
parse(p, varargin{:});

psf = p.Results.psf;
beta = p.Results.beta;
tau = p.Results.tau;
direction = p.Results.direction;
numiter = p.Results.numiter;

%% initialization
g = initInputImage(g);
if min(g(:))>=0
    g = g - median(g(:));
end
[m, n] = size(g);
if numel(psf)==1 && psf==-1
    psf = getPsfApproximation(direction);
end
H = loadOrCreateDicConvMtxFromPsf(m, n, psf);

f = zeros(m,n);
dx = [0, -1, 1];
dy = [0; -1; 1];
Dx = loadOrCreateDicConvMtxFromPsf(m, n, dx);
Dy = loadOrCreateDicConvMtxFromPsf(m, n, dy);
global biomag___
biomag___.m = m;
biomag___.n = n;
cvx_begin quiet
    cvx_solver scs
    cvx_precision low
    cvx_solver_settings( 'max_iters', numiter)
    variable f(m,n) nonnegative
    minimize( pow_pos(norm(g(:) - H*f(:),2),2) + beta*sum(f(:)) ...
        + tau*sum(norms(pow_pos([Dx*f(:), Dy*f(:)],1),2,2)))
cvx_end

end

function H = loadOrCreateDicConvMtxFromPsf(m, n, psf)
folder = ['.', filesep, 'savedMatrices', filesep];
if 0 == exist(folder, 'dir')
    mkdir(folder);
end
psfstr = num2str(psf(:));
psfstr = psfstr';
psfstr = psfstr(:)';
fname = ['dic_convmtx_m', num2str(m), '_n', num2str(n), '_psf', num2str(psfstr)];
ext = '.mat';
if 0 == exist([folder, fname, ext], 'file')
    H = getConvMtx(psf, m, n);
    save([folder, fname, ext], 'H');
else
    load([folder, fname, ext]);
end
end
