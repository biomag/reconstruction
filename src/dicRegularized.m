function r = dicRegularized( I, varargin)

% r = dicRegularized(nevtelen, 'direction', 135, 'PSF', 'gaussian', 'sigma', 1, 'filterSize', 7, 'noisePower', 10); figure, imshow(mat2gray(r));
% r = dicRegularized(disc, 'direction', -135, 'PSF', 'gaussian', 'sigma', 1, 'filterSize', 7, 'noisePower', 250); figure, imshow(mat2gray(r));

I = initInputImage(I);
%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultKernelSigma = 1.5;
defaultPsf = -1;
defaultNoisePower = 1;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'psf', defaultPsf, @(A) isnumeric(A) || ischar(A));
addParameter(p, 'noisePower', defaultNoisePower, @isnumeric);
parse(p, varargin{:});

direction = p.Results.direction-180;
kernelSigma = p.Results.kernelSigma;
psf = p.Results.psf;
noisePower = p.Results.noisePower;

if numel(psf)==1 && psf==-1
    psf = getGaussDeriv('sigma', kernelSigma, 'direction', direction);
end
psf = psf./sum(abs(psf(:)));

%% reconstruction
r = deconvreg(I, psf, noisePower);

end

