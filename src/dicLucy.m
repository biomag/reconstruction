function r = dicLucy( g, varargin)

g = initInputImage(g);
%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultKernelSigma = 0.5;
defaultPsf = -1;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'psf', defaultPsf, @(A) isnumeric(A) || ischar(A));
parse(p, varargin{:});

direction = p.Results.direction;
kernelSigma = p.Results.kernelSigma;
psf = p.Results.psf;

if numel(psf)==1 && psf==-1
    psf = getGaussDeriv('sigma', kernelSigma, 'direction', direction);
end
% psf = psf./sum(abs(psf(:)));

%% reconstruction
r = deconvlucy(g, psf, 10);

end
