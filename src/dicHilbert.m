function f = dicHilbert( g, varargin )
%DICHILBERT DIC reconstruction by Hilbert transform
%   

g = initInputImage(g);
%% parse inputs
p = inputParser;
defaultDirection = 0;

addParameter(p, 'direction', defaultDirection, @isnumeric);
parse(p, varargin{:});

direction = p.Results.direction;

direction = sign(direction) * mod(abs(direction), 360);
while direction<0
    direction = direction + 360;
end
directionRad = direction*pi/180;
dirx = cos(directionRad);
diry = sin(directionRad);
if abs(dirx) < 0.01
    dirx = 0;
end
if abs(diry) < 0.01
    diry = 0;
end
dirx = sign(dirx);
diry = sign(diry);

%% calculate
[m, n] = size(g);
sgn = zeros(m,n);
m2 = round(m/2);
n2 = round(n/2);
for i = 1:m
    for j = 1:n
        u = i-m2;
        v = j-n2;
        sgn(i,j) = sign(diry*u-dirx*v);
    end
end
f = -ifft2(ifftshift((-1i*sgn).*fftshift(fft2(g))));
f = real(f);

end

