function f = dicSEMU( g, varargin )
%DICSEMU Sparseness-Enhanced Multiplicative Update DIC reconstruction algorithm
%   The implementation is based on the article by Kang Li and Takeo Kanade:
%   Nonnegative Mixed-Norm Preconditioning for Microscopy Image
%   Segmentation.
%   
%   The bias elimination (flat-field correction) step is not implemented 
%   (yet, at least). Furthermore, the discrete Laplacian operator can be
%   argued, because more kernel definitions exists.

% example
% result = dicSEMU(cns(1:end/2, 1:end/2), 'direction', -45, 'show', true, 'alpha', 2, 'gamma', 0.5, 'beta', 0.00055, 'maxiter', 10000);

%% parse inputs
p = inputParser;
defaultKernelSigma = 0.5;
defaultPsf = -1;
defaultShow = false;
defaultMaxiter = -1;
% defaultBeta = 0.25;
% defaultGamma = 0.05;
defaultAlpha = 0.2; % sparseness reweight factor
defaultBeta = 0.001; % smoothness
defaultGamma = 2; % sparsity
defaultDirection = 0;

addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'psf', defaultPsf, @isnumeric);
addParameter(p, 'show', defaultShow, @islogical);
addParameter(p, 'numiter', defaultMaxiter, @isnumeric);
addParameter(p, 'alpha', defaultAlpha, @isnumeric);
addParameter(p, 'beta', defaultBeta, @isnumeric);
addParameter(p, 'gamma', defaultGamma, @isnumeric);
addParameter(p, 'direction', defaultDirection, @isnumeric);
parse(p, varargin{:});

kernelSigma = p.Results.kernelSigma;
psf = p.Results.psf;
show = p.Results.show;
maxiter = p.Results.maxiter;
alpha = p.Results.alpha;
beta = p.Results.beta;
gamma = p.Results.gamma;
direction = p.Results.direction;

%% initialization
boundaryOpt = 'replicate';
g0 = g;
g = mat2gray(g);
g = imfilter(g, fspecial('gaussian'), boundaryOpt);
% g = medfilt2(g);
[m0, n0] = size(g0);
if numel(psf)==1 && psf==-1
    K = getGaussDeriv('sigma', kernelSigma, 'direction', direction);
else
    K = psf;
end
% K = K ./ sum(abs(K(:)))*0.25;

filter_center = floor((size(K) + 1)/2);
rm = filter_center(1) - 1;
rn = filter_center(2) - 1;

g = padarray(g, [rm, rn], boundaryOpt);
[m, n] = size(g);
N = m*n;
w = ones(N,1);

R = getLaplacian(m, n, beta); % -2 because of the 3x3 laplacian
H = convmtx2(K, m, n);
% H = getConvMtx(K, m, n);
Q = H'*H + (R'*R);
% Q = H*H' + R*R';
Qplus = Q;
Qminus = Q;
Qplus(Q<0) = 0;
Qminus(Q>0) = 0;
Qminus = -Qminus;
l = -H.'*reshape(padarray(g, [rm, rn], boundaryOpt), (m+2*rm)*(n+2*rn),1);
% l = -H.'*g(:);
% %reshape and set zeros at borders
% l = reshape(l, m, n);
% l([1:rm, end-rm:end], [1:rn, end-rn:end]) = 0;
% % l = l([1:2*rm, end-2*rm:end], [1:2*rn, end-2*rn:end]);
% % l = padarray(l, [rm, rn], 'replicate');
% l = l(:);

f = ones(m-2*rm, n-2*rn);
fPrev = padarray(f, [rm, rn], boundaryOpt);
convg = 1;
count = 0;
if show
    disp('Starting optimization');
    figure
end
while convg >= 10^-3 && (maxiter<0 || count<maxiter)
% while count<2000
    count = count + 1;
    f = padarray(f, [rm, rn], boundaryOpt);
    f = f(:);
%     a = -(l+beta*w);
%     b = (l+beta*w).^2;
%     penalty = beta * w;
%     a = a + penalty;
%     b = b + penalty.*(penalty+2*a);
%     a = l + penalty;
%     b = l + penalty.*(penalty+2*a);
    a = l + w;
%     b = l.^2 + w.*(w + 2*l);
%     b = a.^2;
    b = (l+w).^2;
%     b = l.^2 + 2.*l.*w + w.^2;
    fp = Qplus*f;
    fm = Qminus*f;
    c = 4 * fp .* fm;
    d = 2 * fp + eps;
%     d(d<eps) = eps;
%     if min(b(:))<0
%         maxval = max(b(:));
%         minval =min(b(:)); 
%         b = b - min(b(:));
%         b = b./(maxval+minval).*maxval;
%     end
%     bplusc = b+c;
%     bplusc(bplusc<0) = 0;
    f = (-a + sqrt(b+c) ./ d) .* f;
%     disp(mean(((-a + sqrt(b+c)) ./ d)));
    f(f>1) = 1;
%     f(f<0) = 0;% ???
    w = beta./(f+alpha);
    
    f = reshape(f, m, n);
    convg = norm(f-fPrev);
    fPrev = f;
    f = f(rm+1:end-rm, rn+1:end-rn);
    if ~isreal(f)
        f = fPrev;
        break
    end
    if show && mod(count, 100) == 0
%         imagesc(reshape(f,m,n));
        imagesc(f)
        axis square
%         imagesc(reshape(w,m,n))
        pause(0.01);
%         disp(convg);
        disp(count);
%         disp(max(w));
    end
end

% f = reshape(f, m, n);
% f = f(rm+1:end-rm, rn+1:end-rn);
end

function g = getLaplacian(m, n, smoothness)
%TODO might not work, should try Ixx + Iyy or the one below
% g = padarray(fspecial('laplacian', 0.5), [s-3 s-3]/2);
% g = fspecial('laplacian', 0.5);
% g = g ./ max(g(:)); % if uncommented, better approximates the proposed
% g = [-1/8 -1/8 -1/8; -1/8 1 -1/8; -1/8 -1/8 -1/8]; % same as the above
g = [-1/8 -1/8 -1/8; -1/8 1 -1/8; -1/8 -1/8 -1/8] * smoothness;
% g = [0 1 0; 1 -4 1; 0 1 0] * smoothness;
% g = -[0 -1 0; -1 4 -1; 0 -1 0] * smoothness;
% g = padarray(g, [s-3, s-3]/2);
% g = getConvMtx(g, m, n);
% g = [0, -1/8*smoothness, 0;
%      -1/8*smoothness, 0.5*smoothness + sparsity, -1/8*smoothness;
%      0, -1/8*smoothness, 0];
% g = [0, -1/4*smoothness, 0;
%      -1/4*smoothness, 1*smoothness + sparsity, -1/4*smoothness;
%      0, -1/4*smoothness, 0];
g = g./sum(abs(g(:)));
g = convmtx2(g, m, n);
end

function h = convByMtx(G, I)
h = reshape(G*I(:), size(I));
end