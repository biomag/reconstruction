classdef dic_gui_control
    %DIC_GUI_CONTROL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
    end
    
    methods(Static)
        function [paramList, defaults] = getParametersForAlgorithm(algName)
            switch algName
                case 'dicEMM'
                    paramList = {'direction', 'numiter', 'wSmooth', 'wAccept'};
                    defaults = {0, 20000, 0.0125, 0.025};
                case 'dicFeineigle'
                    paramList = {'direction', 'numiter', 'lambda1', 'lambda2'};
                    defaults = {0, 2000, 10^1, 10^-2};
                case 'dicCvxSocp'
                    paramList = {'direction', 'numiter', 'beta', 'tau'};
                    defaults = {0, 20000, 10^-4, 10^-3};
                case 'precondWinWrapper'
                    paramList = {'direction', 'numiter'};
                    defaults = {0, 10000};
                case 'dicZhaozhengYin'
                    paramList = {'direction', 's', 'r'};
                    defaults = {0, 10^1, 10^-3};
                case 'dicWiener'
                    paramList = {'direction', 'NSR'};
                    defaults = {0, 0.01};
                case 'dicHilbert'
                    paramList = {'direction'};
                    defaults = {0};
                otherwise
                    paramList = {'unknown'};
                    defaults = {0};
            end
        end
        
        function fcnName = convertAlg2FcnName(algName)
            switch algName
                case 'Energy Minimization (Koos15)'
                    fcnName = 'dicEMM';
                case 'Energy Minimization (Feineigle96)'
                    fcnName = 'dicFeineigle';
                case 'SOCP (Li09)'
                    fcnName = 'dicCvxSocp';
                case 'SEMU (Li09)'
                    fcnName = 'precondWinWrapper';
                case 'Inverse Filtering (Yin11)'
                    fcnName = 'dicZhaozhengYin';
                case 'Wiener Filtering (Heise05)'
                    fcnName = 'dicWiener';
                case 'Hilbert Transform (Heise05)'
                    fcnName = 'dicHilbert';
                otherwise
                    fcnName = 'function name not supported';
            end
        end
        
        function resim = callAlgorithm(algName, dicFile, params, values)
            alg = algName;
            if ~isa(alg, 'function_handler')
                alg = eval(['@', dic_gui_control.convertAlg2FcnName(algName)]);
            end
            parvalpairs = {};
            for i = 1:length(params)
                if ~isempty(values{i})
                    parvalpairs{end+1} = params{i};%#ok
                    parvalpairs{end+1} = str2double(values{i});%#ok
                end
            end
            resim = alg(dicFile, parvalpairs{:});
            resim(resim<0) = 0;
        end
        
        function r = getsetLastDirection(value)
            persistent lastDirection;
            if nargin
                lastDirection = value;
            end
            r = lastDirection;
        end
    end
    
end

