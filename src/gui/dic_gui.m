function varargout = dic_gui(varargin)

% Last Modified by GUIDE v2.5 14-Dec-2015 11:08:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dic_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @dic_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before dic_gui is made visible.
function dic_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% Choose default command line output for dic_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, true);


% --- Outputs from this function are returned to the command line.
function varargout = dic_gui_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
if isreset
    dic_gui_control.getsetLastDirection(0);
end

% Update handles structure
guidata(handles.figure1, handles);
algorithmChooser_Callback([], [], handles);

global resim
resim = [];


% --- Executes on selection change in algorithmChooser.
function algorithmChooser_Callback(hObject, eventdata, handles)
panel = handles.parametersPanel;
child_handles = allchild(panel);
for i = 1:numel(child_handles)
    delete(child_handles(i));
end
algName = handles.algorithmChooser.String(handles.algorithmChooser.Value);
algName = algName{:};
fcnName = dic_gui_control.convertAlg2FcnName(algName);
[paramList, defaults] = dic_gui_control.getParametersForAlgorithm(fcnName);
buildParametersPanel(handles, paramList, defaults);
handles.statusbar.ForegroundColor = [0 0 0];
handles.statusbar.String = '';
if strncmp(algName, 'SOCP', 4) || strncmp(algName, 'SEMU', 4)
    handles.statusbar.ForegroundColor = [1 0 0];
    handles.statusbar.String = 'This algorithm might require the installation of a 3rd party software! See Manual!';
end

function buildParametersPanel(handles, paramList, defaults)
panel = handles.parametersPanel;
panelHeight = panel.Position(4);
for i = 1:length(paramList)
    uicontrol(panel, 'Style', 'text', 'String', paramList{i}, 'units', 'characters', ...
        'Position',[2 panelHeight-(i-1)*1.5-3 10 1.2]);
    if ~strcmp('direction', paramList{i})
        uicontrol(panel, 'Style', 'edit', 'Tag', ['algpar_', paramList{i}], 'units', 'characters', ...
            'Position',[13 panelHeight-(i-1)*1.5-3 14 1.2], 'String', num2str(defaults{i}));
    else
        defaultValue = dic_gui_control.getsetLastDirection();
        uicontrol(panel, 'Style', 'edit', 'Tag', ['algpar_', paramList{i}], 'units', 'characters', ...
            'Position',[13 panelHeight-(i-1)*1.5-3 14 1.2], 'String', num2str(defaultValue), ...
            'Callback', @saveDirection);
    end
end

function saveDirection(source, callbackdata)
dic_gui_control.getsetLastDirection(str2double(source.String));


% --- Executes during object creation, after setting all properties.
function algorithmChooser_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in openfile.
function openfile_Callback(hObject, eventdata, handles)
[fileName, pathName] = uigetfile('*', 'Select a DIC image');
if ischar(fileName)
    handles.filepathEditor.String = [pathName, fileName];
end


function filepathEditor_Callback(hObject, eventdata, handles)
if isempty(handles.filepathEditor.String)
    handles.filepathEditor.String = 'Choose a file';
end


% --- Executes during object creation, after setting all properties.
function filepathEditor_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)
algName = handles.algorithmChooser.String(handles.algorithmChooser.Value);
algName = algName{:};
fcnName = dic_gui_control.convertAlg2FcnName(algName);
paramNames = dic_gui_control.getParametersForAlgorithm(fcnName);
parValues = cell(1, length(paramNames));
paramFields = allchild(handles.parametersPanel);
for i = 1:length(paramNames)
    for j = 1:numel(paramFields)
        if strcmp(paramFields(j).Style, 'edit') && ...
                strcmp(paramFields(j).Tag,['algpar_', paramNames{i}])
            parValues{i} = paramFields(j).String;
            break;
        end
    end
end
handles.statusbar.ForegroundColor = [1 0 0];
handles.statusbar.String = 'Reconstruction in progress... This might take some time!';
drawnow;
global resim
try
    if ~isempty(handles.filepathEditor.String) && ~strcmp(handles.filepathEditor.String, 'Choose a file')
        resim = dic_gui_control.callAlgorithm(algName, handles.filepathEditor.String, paramNames, parValues);
    else
        handles.statusbar.ForegroundColor = [1 0 0];
        handles.statusbar.String = 'Choose an input file!';
        return
    end
catch ex
    handles.statusbar.ForegroundColor = [1 0 0];
    handles.statusbar.String = ['Reconstruction failed! Error: ', ex.message];
    resim = [];
    handles.saveButton.Enable = 'off';
    rethrow(ex);
end
handles.statusbar.ForegroundColor = [0 0 0];
handles.statusbar.String = 'Reconstruction finished.';
figure, imagesc(resim), colormap gray
handles.saveButton.Enable = 'on';

% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
global resim
[fileName, pathName, filterIndex] = uiputfile({'*.png','PNG Files';...
          '*.*','All Files' },'Save Image',...
          'dic_reconstruction.png');
if filterIndex==0
    return
end
[~,name,ext] = fileparts(fileName);
if isempty(ext)
    ext = '.png';
end
try
    imwrite(mat2gray(resim), [pathName, name, ext]);
catch ex
    errordlg(ex.message, 'File error');
end


