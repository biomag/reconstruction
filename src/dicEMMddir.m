function f = dicEMMddir( gOrig, varargin )
%DICEMM DIC deconvolution using our energy minimization method
%   Detailed explanation goes here

%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultNumIter = 400;
defaultPyrSize = 3;
defaultWSmooth = 0.0125;
defaultWKeepCl = 0;
defaultWAccept = 0.025;
defaultKernelSize = 5;
defaultKernelSigma = 1.5;
defaultShow = false;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numIter', defaultNumIter, @isnumeric);
addParameter(p, 'pyrSize', defaultPyrSize, @(x) isnumeric(x) && x>0 && x<=4);
addParameter(p, 'wSmooth', defaultWSmooth, @isnumeric);
addParameter(p, 'wKeepCl', defaultWKeepCl, @isnumeric);
addParameter(p, 'wAccept', defaultWAccept, @isnumeric);
addParameter(p, 'kernelSize', defaultKernelSize, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'show', defaultShow, @islogical);
parse(p, varargin{:});

direction = p.Results.direction;
numIter = p.Results.numIter;
pyrSize = p.Results.pyrSize;
wSmooth = p.Results.wSmooth;
wKeepCl = p.Results.wKeepCl;
wAccept = p.Results.wAccept;
kernelSize = p.Results.kernelSize;
kernelSigma = p.Results.kernelSigma;
show = p.Results.show;

%% preprocessing
gOrig = gOrig - mean(gOrig(:));
if show
    figure
    subplot(1,3,1), imshow(mat2gray(gOrig))
end
dx = getCDiffKernel(1,0,8);
dy = getCDiffKernel(0,1,8);
dxx = getCDiffKernel(2,0);
der1 = imrotate(dx, -direction, 'bicubic');
der2 = imrotate(dxx, -direction, 'bicubic');

K = fspecial('gaussian', [kernelSize kernelSize], kernelSigma);
borderOpt = 'replicate';

%% reconstruction
for pyramidLevel = pyrSize-1:-1:0
    g = gOrig;
    for i = 1:pyramidLevel
        g = impyramid(g, 'reduce');
    end
    if pyramidLevel==pyrSize-1
        f = zeros(size(g));
    else
        f = impyramid(f, 'expand');
        [mg, ng] = size(g);
        [mf, nf] = size(f);
        if mf<mg
            f(end+1, :) = f(end, :); %#ok<AGROW>
        end
        if nf<ng
            f(:, end+1) = f(:, end); %#ok<AGROW>
        end
    end
    diff = imfilter(g, der1, borderOpt);
    fPrev = f;
    for iter = 1:numIter/(pyrSize-pyramidLevel);
        fx = imfilter(f, dx, borderOpt);
        fy = imfilter(f, dy, borderOpt);
        
        fdMagnitude = sqrt(fx.*fx + fy.*fy + eps);
        fxNorm = fx./fdMagnitude;
        fyNorm = fy./fdMagnitude;
        div = getDivergence2(fxNorm, fyNorm, borderOpt);
        
        der = imfilter(f, der2, borderOpt);
        addDiv = wSmooth*div;
        addAll = imfilter(der, K, borderOpt) - diff + addDiv;
        if pyramidLevel~=pyrSize-1
            addAll = addAll + wKeepCl*(fPrev-f);
        end
        f = f + wAccept*addAll;
        %% show the result in each iteration
        if show
            subplot(1,3,2), imshow(mat2gray(f)), 
            subplot(1,3,3), imshow(mat2gray(addAll))
            drawnow
        end
    end
end

end
