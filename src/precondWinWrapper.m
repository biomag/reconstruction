function result = precondWinWrapper( g, varargin )
%PRECONDWINWRAPPER Windows wrapper function for SEMU reconstruction algorithm
%   This function is a wrapper for the SEMU DIC reconstruction algorithm. It only works
%   in Windows environment. The function creates temporary image files
%   and executes the algorithm as an operating system command.

if ischar(g)
    g = im2double(imread(g));
end
p = inputParser;
defaultDirection = 0;
defaultKernelSigma = 0.5;
defaultNumiter = 10000;

addOptional(p, 'direction', defaultDirection, @isnumeric);
addOptional(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addOptional(p, 'numiter', defaultNumiter, @isnumeric);
parse(p, varargin{:});

angle = 180 - p.Results.direction;
softness = p.Results.kernelSigma;
numiter = p.Results.numiter;

%% generate random part for filename
symbols = ['a':'z' 'A':'Z' '0':'9'];
MAX_ST_LENGTH = 50;
stLength = randi(MAX_ST_LENGTH);
nums = randi(numel(symbols),[1 stLength]);
st = symbols(nums);

%%
input_filename = ['pcnd_wrapper_tmp_input', st, '.tif'];
output_filename = [input_filename(1:end-4), '.preprocessed.tif'];
imwrite(g, input_filename, 'tif');

pcnd = '.\precond\dist\bin\pcnd64.exe';
command = [pcnd, ' -r=0.5 -p=0.00055 -t=1e-4 -m=', num2str(numiter), ' -a=', num2str(angle), ' -s=', num2str(softness), ...
    ' ', input_filename];
system(command);
result = imread(output_filename);

delete(input_filename);
delete(output_filename);
end

