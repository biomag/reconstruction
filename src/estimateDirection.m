function dir = estimateDirection( g )
%ESTIMATEDIRECTION Determine the shear direction of a DIC image.
%   Determines the shear direction of an image. The shear
%   directions in practice are usually 0 or -45 degrees, but this function does not have
%   the ability at its current state to select one of these two.  It is 180
%   degrees invariant, it is not trivial to determine the exact direction.

fxData = zeros(1, 360);
fyData = zeros(1, 360);
fy2Data = zeros(1, 360);
for i = 1:360
    f = imrotate(g, i, 'crop');
    %f = f(100:400, 100:400);
    [fx, fy] = gradient(f);
    fxaData(i) = sum(abs(fx(:)));
    fyaData(i) = sum(abs(fy(:)));
    fyData(i) = sum(fx(:));
    fy2Data(i) = sum(fy(:));
end
figure('Name', 'x abs sum');
plot(fxaData-fyaData);
% figure('Name', 'x sum noabs');
% plot(fyData);
% figure('Name', 'y sum noabs');
% plot(fy2Data);
% figure('name', 'x-y')
% plot(abs(fyData)-abs(fy2Data));

diff = fxaData-fyaData;
[~, dir] = max(diff(1:180));
% dir = dir;
end

