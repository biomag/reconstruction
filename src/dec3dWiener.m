function r = dec3dWiener( I, varargin )
%DEC3DWIENER 3D DIC image reconstruction using Wiener filter
%   
%

%% parse inputs
p = inputParser;
defaultNSR = 10^-4;
defaultPSF = getCoherentDicPsf3D('lateralStep', 3, 'zStep', 1);

addParameter(p, 'NSR', defaultNSR, @isnumeric);
addParameter(p, 'PSF', defaultPSF, @(A) isnumeric(A) || ischar(A));
parse(p, varargin{:});

NSR = p.Results.NSR;
PSF = p.Results.PSF;

%% using Matlab wiener filter
r = deconvwnr(-I, PSF, NSR);

end