function r = highpassFilter(g, varargin)
%HIGHPASSFILTER Apply highpass filter to the input image
%

%% parse inputs
p = inputParser;
defaultD = 50; % d = 1.2*max(size(input));
defaultMode = 'log';

addParameter(p, 'd', defaultD, @isnumeric);
addParameter(p, 'mode', defaultMode, ...
    @(x) ischar(x) && (strcmp(x,'abs') || strcmp(x,'log')));
parse(p, varargin{:});

d = p.Results.d;
mode = p.Results.mode;
%% transform
g = g - mean(g(:));
[nx, ny] = size(g);
G = fft2(g, 2*nx-1, 2*ny-1);
A = abs(G);
P = angle(G);

%% ideal high pass filter
filter = ones(2*nx-1,2*ny-1);
for i = 1:2*nx-1
    for j = 1:2*ny-1
        dist = ((i-(nx+1))^2 + (j-(ny+1))^2)^.5;
        if dist<=d
            filter(i,j) = 0;
        end
    end
end
Ahpf = A.*filter;

%% reverse transform
r = ifft2(Ahpf.*exp(1i*P), 2*nx-1, 2*ny-1);
r = r(1:nx, 1:ny);
if strcmp(mode, 'log')
    r = log(1+abs(r));
else
    r = abs(r);
end
r = mat2gray(r);
end