function G = getGaussDeriv( varargin )
%GETGAUSSDERIV Returns the first derivative of Gaussian kernel
%   Parameters:
%    - direction in degrees

%% parse inputs
p = inputParser;
defaultSigma = 0.5;
defaultSize = -1;
defaultDirection = 0;

addOptional(p, 'size', defaultSize, @isnumeric);
addOptional(p, 'sigma', defaultSigma, @isnumeric);
addOptional(p, 'direction', defaultDirection, @isnumeric);
parse(p, varargin{:});

sigma = p.Results.sigma;
kernelSize = p.Results.size;
direction = pi - p.Results.direction*pi/180;
if kernelSize == defaultSize
    kernelSize = ceil(6*sigma+1);
    if mod(kernelSize,2)==0
        kernelSize = kernelSize + 2;
    end
end

%% create the kernel
G = zeros(kernelSize);
[m, n] = size(G);
for i = 1:m
    for j = 1:n
        x = j-ceil(n/2);
        y = i-ceil(m/2);
        G(i,j) = -x*exp(-(x^2+y^2)/sigma^2)*cos(direction);
        G(i,j) = G(i,j) - y*exp(-(x^2+y^2)/sigma^2)*sin(direction);
    end
end
G = G./sum(abs(G(:)));
G(abs(G)<eps) = 0;

end

