function psf = getPsfApproximation( direction )
%GETPSFAPPROXIMATION Returns a fair approximation of the DIC PSF
%   Returns an approximation of the DIC Point Spread Function (PSF) with a
%   small sigma value (\sigma = ~0.1) in the given direction. This function
%   only supports the most common directions which are multiplicatives of
%   45 degrees (including 0).
%
%       direction - direction of the PSF

if mod(direction, 45) ~= 0
    error('The direction parameter has to be a multiplicative of 45 degrees!');
end

psf = [0 0 0; 0 -1 1; 0 0 0]/2;
if direction ~= 0
psf = imrotate(psf, direction, 'crop');

end

