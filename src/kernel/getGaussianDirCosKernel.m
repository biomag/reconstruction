function K = getGaussianDirCosKernel( n, sigma, direction, a )
%GETGAUSSIANDIRCOSKERNEL Summary of this function goes here
%   Detailed explanation goes here

if nargin<4
    a = 1;
end

half = ceil(n/2);
K = fspecial('gaussian', [n n], sigma);
for i = 1:n
    K(:, i) = K(:, i) * cos(2*pi*(i-half)/(3*sigma-0.5)/a);
end
for i = 1:n
    K(i, :) = K(i, :) * cos(pi/2*((i-half)/(3*sigma-0.5)));
end
K = imrotate(K, direction, 'bilinear');
K = K / sum(abs(K(:)));
end

