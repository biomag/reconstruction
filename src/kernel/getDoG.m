function G = getDoG( varargin )
%UNTITLED Returns the difference of Gaussians kernel
%   Detailed explanation goes here

%% parse inputs
p = inputParser;
defaultSize = 21;
defaultSigma = 3;

addOptional(p, 'size', defaultSize, @isnumeric);
addOptional(p, 'sigma', defaultSigma, @isnumeric);
parse(p, varargin{:});

fs = p.Results.size;
sigma = p.Results.sigma;

%% create the kernel
fs2 = round(fs*3/2);
if mod(fs2,2) == 0
    fs2 = fs2 - 1;
end
gauss = fspecial('gaussian', [fs2, fs2], sigma);
[~, n] = size(gauss);
mid = round(fs2/2);
third = floor(fs2/3);
G = gauss(mid-third:mid+third, 1:round(2*n/3));
G = G - gauss(mid-third:mid+third, round(n/3)+1:n);
% G = gauss(round(m/6:5*m/6), 1:round(2*n/3));
% G = G - gauss(round(m/6:5*m/6), round(n/3)+1:n);
end

