function K = getGaussianRoundCosKernel( n, sigma, a )
%GETGAUSSIANROUNDCOSKERNEL Summary of this function goes here
%   Detailed explanation goes here

if nargin<3
    a = 1;
end

half = ceil(n/2);
K = fspecial('gaussian', [n n], sigma);
for i = 1:n
    for j = 1:n
        K(i, j) = K(i, j) * cos(2*pi*sqrt((i-half)^2+(j-half)^2)/(3*sigma-0.5)/a);
    end
end
K = K / sum(abs(K(:)));
end

