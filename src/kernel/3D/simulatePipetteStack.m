function img = simulatePipetteStack( n )
%SIMULATEPIPETTESTACK Summary of this function goes here
%   Detailed explanation goes here

img = zeros(n, n, n);
center = round(n/2);
maxrad = center/2;
minrad = maxrad*0.2;
circle = zeros(n,n);
for i = 1:center
%     radius = maxrad*(center-i+1)/center   *(i-1)/center
    radius = maxrad*(1-(i-1)/center*1);
    if radius - minrad < 0
        break;
    end
    circle(:) = 0;
    for j = 1:n
        for k = 1:n
            cx = j - center;
            cy = k - center;
            if abs(sqrt(cx^2+cy^2) - radius) < 1
                circle(cx+center, cy+center) = 1;
            end
        end
    end
    img(:,i,:) = circle;
end

end

