n = 15;
m = 10;
x = -n:n;
y = -n:n;
[X,Y] = meshgrid(x,y);
sigma = 1;
c = 0;

Z = getCoherentDicPsf3D('lateralStep', n, 'zStep', m, 'sigma', [sigma*3 sigma], 'mu', c);
% 
% figure(1);
% surf(X,Y,Z(:,:,1),'LineStyle','none');
% 
% figure(2);
% surf(X,Y,Z(:,:,5),'LineStyle','none');
% 
figure(3);
surf(X,Y,Z(:,:,11),'LineStyle','none');