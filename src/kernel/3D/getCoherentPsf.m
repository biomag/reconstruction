function K = getCoherentPsf(x, y, r0)
%GETCOHERENTPSF Returns the coherent PSF
%   The input parameters should be like variables produced by meshgrid. The
%   constant r0 is a microscope parameter, very similar to sigma of the 
%   Gaussian distribution.

if nargin < 3
    r0 = 1;
end
r = sqrt(x.^2+y.^2) + eps;
c = besselj(1,pi*(r/r0));
K = 2*c./(pi*(r/r0));
end
