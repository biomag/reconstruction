function L = getCoherentDicPsf3D(varargin)
%GETCOHERENTDICPSF3D Returns the 3D coherent DIC PSF.
%   Detailed explanation goes here
%
%   sigma - vector of two elements for xy plane and z values

%%
p = inputParser;
defaultLateralStep = 10;
defaultZStep = 5;
defaultSigma = [1, 1];
defaultMu = 0;
defaultR = 0.5;
defaultdeltaTheta = 0;
defaultdeltaX = 0.03;

addOptional(p, 'lateralStep', defaultLateralStep, @isnumeric);
addOptional(p, 'zStep', defaultZStep,@isnumeric );
addOptional(p, 'sigma', defaultSigma, @isnumeric);
addOptional(p, 'mu', defaultMu, @isnumeric);
addOptional(p, 'r', defaultR, @isnumeric);
addOptional(p, 'deltaTheta', defaultdeltaTheta, @isnumeric);
addOptional(p, 'deltaX', defaultdeltaX, @isnumeric);
parse(p, varargin{:});

lateralStep = p.Results.lateralStep;
zStep = p.Results.zStep;
s = p.Results.sigma;
mu = p.Results.mu;
r = p.Results.r;
deltaTheta = p.Results.deltaTheta;
deltaX = p.Results.deltaX;

%%
x = -lateralStep:lateralStep;
xnorm = x./1;
[X,Y] = meshgrid(xnorm, xnorm);
z = (-zStep:zStep)./zStep*2;
L = zeros(2*lateralStep+1, 2*lateralStep+1, 2*zStep+1);
K = (1-r)*exp(-1i*deltaTheta)*getCoherentPsf(X-deltaX,Y,s(2)) - ...
    r*exp(1i*deltaTheta)*getCoherentPsf(X+deltaX,Y,s(2));

% a = gaussmf(z, [s(1) mu]);
a = exp(-(z - mu).^2/(2*s(1)^2));
for i = 1:2*zStep+1
    L(:, :, i) = K*a(i); 
end

end

