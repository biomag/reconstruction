n = 15;
m = 10;
x = -n:n;
y = -n:n;
[X,Y] = meshgrid(x,y);
sigma = 1;
c = 0;

Z = getCoherentDicPsf3D('lateralStep', n, 'zStep', m, 'sigma', [sigma sigma*3], 'mu', c);
% Z = getCoherentDicPsf3D('lateralStep', 3, 'zStep', 1, 'mu', 0, 'deltaX', 0.1, 'r', 0.5);

numIso = 3;
values = unique(Z);
numValues = numel(values);
step = round(numValues/(numIso+1));

% indices = [1:numIso].*step;
% or
% indices = [10 100 280 500 4780 5000 5180 5270];
% indices = [5 20 40 60 75];
indices = round([0.05 0.1 0.15 0.85 0.9 0.95] .* numel(values));

isovalues = values(indices);
for i = 1:numel(isovalues)
    [faces,verts, colors] = isosurface(Z, isovalues(i),mat2gray(Z));
    patch('Vertices', verts, 'Faces', faces, ... 
        'FaceVertexCData', colors, ... 
        'FaceColor','interp', ... 
        'edgecolor', 'none');
end
daspect([1,1,1])
view([-25,30])
axis tight
camlight left;
lighting phong
alpha(0.4)

% img = load('clown');
% I = repmat(img.X,[1 1 5]);
% cmap = img.map;
figure
[X,Y] = meshgrid(1:size(Z,2), 1:size(Z,1));
Zc = ones(size(Z,1),size(Z,2));
for k=1:size(Z,3);
    surface('XData',X-0.5, 'YData',Y-0.5, 'ZData',Zc.*k, ...
        'CData',Z(:,:,k)./max(abs(Z(:))), ...
        'EdgeColor','none', 'FaceColor','texturemap')
end
% colormap gray
% colormap(cmap)
alpha(0.25)