function K = getCoherentDicPsf(n)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

x = -n:n;
xnorm = x./1;
[X,Y] = meshgrid(xnorm,xnorm);


deltaX = 0.03; 
deltaTheta = 0;
R = 0.5;    

 
K = (1-R)*exp(-1i*deltaTheta)*getCoherentPsf(X-deltaX,Y) - R*exp(1i*deltaTheta)*getCoherentPsf(X+deltaX,Y);

% K = K ./ max(abs(K(:)));
% K = K ./ sum(abs(K(:)));


end

