a = 21;
dx = 1;
x = -a:dx:a;
b = 21;
dy = 1;
y = -b:dy:b;
[X,Y] = meshgrid(x,y);

Z = getCoherentPsf(X,Y);

surf(X,Y,Z,'LineStyle','none');


figure(2);
deltaX = 1.1; %ezt v�ltoztatva lesz egy cs�cs az imag r�sz. Ha pl 0.05, akkor az imag r�sz is 2 cs�cs. 
deltaTheta = 0;
R = 0.5;    

% 
Z = (1-R)*exp(-1i*deltaTheta)*getCoherentPsf(X,Y-deltaX) - R*exp(1i*deltaTheta)*getCoherentPsf(X,Y+deltaX);
surf(X,Y,Z,'LineStyle','none');
xlabel('X','FontSize',14);
ylabel('Y','FontSize',14);
