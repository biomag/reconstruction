function f = dicEMMbilin( gOrig, varargin )
%DICEMMBILIN DIC deconvolution using our energy minimization method
%   Detailed explanation goes here

%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultNumIter = 400;
defaultPyrSize = 3;
defaultWSmooth = 0.0125;
defaultWKeepCl = 0;
defaultWAccept = 0.025;
defaultKernelSize = 5;
defaultKernelSigma = 1.5;
defaultShow = false;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numIter', defaultNumIter, @isnumeric);
addParameter(p, 'pyrSize', defaultPyrSize, @(x) isnumeric(x) && x>0 && x<=4);
addParameter(p, 'wSmooth', defaultWSmooth, @isnumeric);
addParameter(p, 'wKeepCl', defaultWKeepCl, @isnumeric);
addParameter(p, 'wAccept', defaultWAccept, @isnumeric);
addParameter(p, 'kernelSize', defaultKernelSize, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'show', defaultShow, @islogical);
parse(p, varargin{:});

direction = p.Results.direction;
numIter = p.Results.numIter;
pyrSize = p.Results.pyrSize;
wSmooth = p.Results.wSmooth;
wKeepCl = p.Results.wKeepCl;
wAccept = p.Results.wAccept;
kernelSize = p.Results.kernelSize;
kernelSigma = p.Results.kernelSigma;
show = p.Results.show;

%% preprocessing
direction = direction*pi/180;
u = cos(direction);
v = sin(direction);
gOrig = gOrig - mean(gOrig(:));
if show
    figure
    subplot(1,3,1), imshow(mat2gray(gOrig))
end

dx = getCDiffKernel(1,0); % finite difference kernels
dy = getCDiffKernel(0,1);
dxx = getCDiffKernel(2,0);
dxy = getCDiffKernel(1,1);
dyy = getCDiffKernel(0,2);
dx3 = getCDiffKernel(3,0);
dy3 = getCDiffKernel(0,3);
dx4 = getCDiffKernel(4,0);
dy4 = getCDiffKernel(0,4);
dx2y = getCDiffKernel(2,1);
dxy2 = getCDiffKernel(1,2);
dx3y  = getCDiffKernel(3,1);
dx2y2 = getCDiffKernel(2,2);
dxy3  = getCDiffKernel(1,3);
dx3y2 = getCDiffKernel(3,2);
dx2y3 = getCDiffKernel(2,3);
dx4y2 = getCDiffKernel(4,2);
dx3y3 = getCDiffKernel(3,3);
dx2y4 = getCDiffKernel(2,4);

K = fspecial('gaussian', [kernelSize kernelSize], kernelSigma);
borderOpt = 'replicate';

%% reconstruction
for pyramidLevel = pyrSize-1:-1:0
    g = gOrig;
    for i = 1:pyramidLevel
        g = impyramid(g, 'reduce');
    end
    if pyramidLevel==pyrSize-1
        f = zeros(size(g));
    else
        f = impyramid(f, 'expand');
        [mg, ng] = size(g);
        [mf, nf] = size(f);
        if mf<mg
            f(end+1, :) = f(end, :); %#ok<AGROW>
        end
        if nf<ng
            f(:, end+1) = f(:, end); %#ok<AGROW>
        end
    end
    gx = imfilter(g, dx, borderOpt);
    gy = imfilter(g, dy, borderOpt);
    gx3 = imfilter(g, dx3, borderOpt);
    gx2y = imfilter(g, dx2y, borderOpt);
    gxy2 = imfilter(g, dxy2, borderOpt);
    gy3 = imfilter(g, dy3, borderOpt);
    gx3y2 = imfilter(g, dx3y2, borderOpt);
    gx2y3 = imfilter(g, dx2y3, borderOpt);
    diff1 = u*gx + v*gy;
    diff2 = 1/2*(u*gx3 + v*gx2y);
    diff3 = 1/4*(u*gx3y2 + v*gx2y3);
    diff4 = 1/2*(u*gxy2 + v*gy3);
    diff = diff1 + diff2 + diff3 + diff4;
    fPrev = f;
    for iter = 1:numIter/(pyrSize-pyramidLevel);
        fx = imfilter(f, dx, borderOpt);
        fy = imfilter(f, dy, borderOpt);
        fx2 = imfilter(f, dxx, borderOpt);
        fxy = imfilter(f, dxy, borderOpt);
        fy2 = imfilter(f, dyy, borderOpt);
        fx4 = imfilter(f, dx4, borderOpt);
        fx3y = imfilter(f, dx3y, borderOpt);
        fx2y2 = imfilter(f, dx2y2, borderOpt);
        fxy3 = imfilter(f, dxy3, borderOpt);
        fy4 = imfilter(f, dy4, borderOpt);
        fx4y2 = imfilter(f, dx4y2, borderOpt);
        fx3y3 = imfilter(f, dx3y3, borderOpt);
        fx2y4 = imfilter(f, dx2y4, borderOpt);
        
        fdMagnitude = sqrt(fx.*fx + fy.*fy + eps);
        fxNorm = fx./fdMagnitude;
        fyNorm = fy./fdMagnitude;
        div = getDivergence2(fxNorm, fyNorm, borderOpt);

        der1 = u*(u*fx2 + v*fxy) + v*(u*fxy + v*fy2);
        der2 = 1/2*(u*(u*fx4 + v*fx3y) + v*(u*fx3y + v*fx2y2));
        der3 = 1/4*(u*(u*fx4y2 + v*fx3y3) + v*(u*fx3y3 + v*fx2y4));
        der4 = 1/2*(u*(u*fx2y2 + v*fxy3) + v*(u*fxy3 + v*fy4));
        der = der1 + der2 + der3 + der4;
        
        addDiv = wSmooth*div;
        addAll = imfilter(der, K) - diff + addDiv;
        if pyramidLevel~=pyrSize-1
            addAll = addAll + wKeepCl*(fPrev-f);
        end
        f = f + wAccept*addAll;
        %% show the result in each iteration
        if show
            subplot(1,3,2), imshow(mat2gray(f)), 
            subplot(1,3,3), imshow(mat2gray(addAll))
            drawnow
        end
    end
end

end
