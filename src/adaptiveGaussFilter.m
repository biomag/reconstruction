function r = adaptiveGaussFilter(I, kernelSize, sigma)
%UNTITLED Bumping Gaussian Filter
%   The kernel size is zoomed out on the image borders so it never uses
%   values outside the image.

K = fspecial('gaussian', [kernelSize kernelSize], sigma);
r = imfilter(I, K);
for i = kernelSize-2:-2:1
    K = fspecial('gaussian', [kernelSize kernelSize], sigma);
    r2 = imfilter(I, K);
    off = ceil(size(K,1)/2);
    r([off, end-off+1], off:end-off+1) = r2([off, end-off+1], off:end-off+1);
    r(off:end-off+1, [off, end-off+1]) = r2(off:end-off+1, [off, end-off+1]);
end
end

