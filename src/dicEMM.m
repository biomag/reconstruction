function f = dicEMM( gOrig, varargin )
%DICEMM Proposed DIC microscopy image reconstruction using variational framework

%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultNumiter = 20000;
defaultPyrSize = 1;
defaultWSmooth = 0.0125;
defaultWKeepCl = 0;
defaultWAccept = 0.025;
defaultPSF = -1;
defaultShow = false;
defaultAnimation = false;
defaultTolerance = 10^-7;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numiter', defaultNumiter, @isnumeric);
addParameter(p, 'pyrSize', defaultPyrSize, @(x) isnumeric(x) && x>0 && x<=4);
addParameter(p, 'wSmooth', defaultWSmooth, @isnumeric);
addParameter(p, 'wKeepCl', defaultWKeepCl, @isnumeric);
addParameter(p, 'wAccept', defaultWAccept, @isnumeric);
addParameter(p, 'psf', defaultPSF, @isnumeric);
addParameter(p, 'show', defaultShow, @islogical);
addParameter(p, 'animation', defaultAnimation, @islogical);
addParameter(p, 'tolerance', defaultTolerance, @(x) isnumeric(x) && x>0);
parse(p, varargin{:});

direction = p.Results.direction;
numiter = p.Results.numiter;
pyrSize = p.Results.pyrSize;
wSmooth = p.Results.wSmooth;
wKeepCl = p.Results.wKeepCl;
wAccept = p.Results.wAccept;
psf = p.Results.psf;
show = p.Results.show;
animation = p.Results.animation;
tolerance = p.Results.tolerance;

%% init
if mod(direction, 45) ~= 0
    warning(['Directions other than multiplicatives of 45 degrees may result poor quality reconstructions!', ...
        ' The input direction is not common in DIC microscopes, please check the setup!']);
end
gOrig = mat2gray(initInputImage(gOrig));
gOrig = padarray(gOrig, [1, 1]);
[m, n] = size(gOrig);
while direction<0
    direction = direction + 360;
end
direction = sign(direction) * mod(abs(direction), 360);
directionRad = direction*pi/180;
dirx = cos(directionRad);
diry = -sin(directionRad);

ifdx = [0, -1, 1]/2;
ibdx = [-1, 1, 0]/2;
ifdy = [0; -1; 1]/2;
ibdy = [-1; 1; 0]/2;
% set difference kernels and correct padding
if direction<90
    dx1 = ifdx;
    dy1 = ifdy;
    dx2 = ibdx;
    dy2 = ibdy;
    gOrig(1, :) = 1;
    gOrig(:, end) = 1;
    row = 2:m-1;
    col = 1:n-2;
elseif direction>=90 && direction<180
    dx1 = ifdx;
    dy1 = ibdy;
    dx2 = ibdx;
    dy2 = ifdy;
    gOrig(1, :) = 1;
    gOrig(:, 1) = 1;
    row = 3:m;
    col = 2:n-1;
elseif direction>=180 && direction<270
    dx1 = ibdx;
    dy1 = ibdy;
    dx2 = ifdx;
    dy2 = ifdy;
    gOrig(:, 1) = 1;
    gOrig(end, :) = 1;
    row = 2:m-1;
    col = 3:n;
else %direction>=270 && direction<360
    dx1 = ibdx;
    dy1 = ifdy;
    dx2 = ifdx;
    dy2 = ibdy;
    gOrig(:, end) = 1;
    gOrig(end, :) = 1;
    row = 1:m-2;
    col = 2:n-1;
end
if numel(psf)==1 && psf==-1
    psf = 1;
end
borderOpt = 'replicate';
animationFolder = 'animation';
if animation
    if ~exist(animationFolder, 'dir')
        mkdir(animationFolder);
    end
end

%% reconstruction
if show
    figure
    subplot(1,3,1), imagesc(gOrig)
    axis image
end
for pyramidLevel = pyrSize-1:-1:0
%     regHistory = [realmax, realmax-1, realmax-2, realmax-3, realmax-4];
    g = gOrig;
    for i = 1:pyramidLevel
        g = impyramid(g, 'reduce');
    end
    if pyramidLevel==pyrSize-1
        f = zeros(size(g));
    else
        f = impyramid(f, 'expand');
        [mg, ng] = size(g);
        [mf, nf] = size(f);
        if mf<mg
            f(end+1, :) = f(end, :); %#ok<AGROW>
        end
        if nf<ng
            f(:, end+1) = f(:, end); %#ok<AGROW>
        end
    end
    gx = imfilter(g, dx1, borderOpt);
    gy = imfilter(g, dy1, borderOpt);

    diff = sign(dirx)*dirx^2*gx + sign(diry)*diry^2*gy;
    fPrev = f;
    numiterByPyrsize = numiter/(pyrSize-pyramidLevel);
    for iter = 1:numiterByPyrsize
        fx = imfilter(f, dx1, borderOpt);
        fy = imfilter(f, dy1, borderOpt);
        fxx = imfilter(fx, dx2, borderOpt);
        fxy = (imfilter(fx, dy2, borderOpt) + imfilter(fy, dx2, borderOpt))/2;
        fyy = imfilter(fy, dy2, borderOpt);
        
        fdMagnitude = sqrt(fx.*fx + fy.*fy + eps);
        fxNorm = fx./fdMagnitude;
        fyNorm = fy./fdMagnitude;
        div = getDivergence2(fxNorm, fyNorm, borderOpt)/2;

        der = dirx^2*fxx + 2*dirx*diry*fxy + diry^2*fyy;
        addDiv = wSmooth*div;
        addAll = imfilter(der, psf, borderOpt) - diff + addDiv;
        if pyramidLevel~=pyrSize-1
            addAll = addAll + wKeepCl*(fPrev-f);
        end
        %%
%         if mod(iter,100)==0
%             change = sum(abs(addAll(:)));
%             if mean(regHistory)<sum(abs(addAll(:)))
%                 wSmooth = wSmooth*1.2
%                 regHistory = [realmax, realmax-1, realmax-2, realmax-3, realmax-4];
%             else
%                 regHistory = [regHistory(2:end), change];
%             end
%         end
        %%
        fPrev = f;
        f = f + wAccept.*addAll;
        f(f<0) = 0;
        
        %% show the result in each iteration
        if show && (mod(iter,100)==0 || iter==1)
            subplot(1,3,2), imagesc(f)
            axis image
            subplot(1,3,3), imagesc(addAll)
            axis image
            drawnow
%             disp(['iter ', num2str(iter), ', diff: ', num2str(norm(f-fPrev))]);
%             disp(['tv: ', num2str(sum(abs(addAll(:))))]);
        end
        if animation && mod(iter,100)==0
            number = sprintf('%04s', num2str(round(iter/100)));
            imwrite(mat2gray(f), ['.', filesep, animationFolder, filesep, 'img', number, '.png'], 'png');
        end
        
        %% check stopping criterium
        if mod(iter,100)==0
            normval = norm(f-fPrev);
            if normval <= tolerance
                break;
            end
        end
    end
end
f = f(row, col);

end


