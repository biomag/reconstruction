function f = bkp_dicZhaozhengYin( g, varargin )
%UNTITLED4 DIC reconstruction algorithm of Zhaozheng Yin et. al.
%   Based on the article Restoring DIC Microscopy Images from
% Multiple Shear Directions. Implemented for a single DIC image and not for
% multiple shear directions.
%
% Example:
%   figure, imshow(mat2gray(dicZhaozhengYin(resz, 's', 100000, 'r', 1000, 'diffKernel', -getGaussDeriv(13))))

%% parse inputs
p = inputParser;
defaultSmoothKernel = [1,1,1; 1,-8,1; 1,1,1]/8;
defaultDiffKernel = @() -getGaussDeriv(19);
defaultDirection = 0;
defaultS = 10;
defaultR = 1000;

addOptional(p, 'smoothKernel', defaultSmoothKernel, @isnumeric);
addOptional(p, 'diffKernel', defaultDiffKernel, @isnumeric);
addOptional(p, 'direction', defaultDirection, @isnumeric);
addOptional(p, 's', defaultS, @isnumeric);
addOptional(p, 'r', defaultR, @isnumeric);
parse(p, varargin{:});

a = p.Results.smoothKernel;
d = p.Results.diffKernel;
direction = p.Results.direction;
s = p.Results.s;
r = p.Results.r;
if isa(d, 'function_handle')
    d = d();
end
d = imrotate(d, direction, 'bilinear');

[m, n] = size(g);
A = fft2(a, m, n);
D = fft2(d, m, n);
G = fft2(g);
F = -(D.*G)./(s*A.*A + r - D.*D);
f = ifft2(F);
end