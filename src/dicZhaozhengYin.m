function f = dicZhaozhengYin( g, varargin )
%DICZHAOZHENGYIN DIC reconstruction algorithm of Zhaozheng Yin et. al.
%   Based on the article Restoring DIC Microscopy Images from
% Multiple Shear Directions. Implemented for a single DIC image and not for
% multiple shear directions.
%

%% parse inputs
g = initInputImage(g);
p = inputParser;
defaultSmoothKernel = [1,1,1; 1,-8,1; 1,1,1]/8;
defaultDiffKernel = -1;
defaultDirection = 0;
defaultS = 10^1;
defaultR = 10^-3;

addParameter(p, 'smoothKernel', defaultSmoothKernel, @isnumeric);
addParameter(p, 'psf', defaultDiffKernel);
addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 's', defaultS, @isnumeric);
addParameter(p, 'r', defaultR, @isnumeric);
parse(p, varargin{:});

a = p.Results.smoothKernel;
psf = p.Results.psf;
direction = 180 + p.Results.direction;
s = p.Results.s;
r = p.Results.r;

if numel(psf)==1 && psf==-1
    psf = getPsfApproximation(direction);
end

%% calculate
g = g - mean(g(:));
[m, n] = size(g);
sa = size(a);
sd = size(psf);
a = padarray(a, [m-ceil(sa(1)/2), n-ceil(sa(2)/2)]);
psf = padarray(psf, [m-ceil(sd(1)/2), n-ceil(sd(2)/2)]);
A = fftshift(fft2(a));
D = fftshift(fft2(psf));
G = fftshift(fft2(g, 2*m-1, 2*n-1));
F = -(D.*G)./(s*A.*A + r - D.*D);
F = ifftshift(F);
f = ifft2(F);

f = f(m:end, n:end);
end
