function f = dicFeineigle( g, varargin )
%DICFEINEIGLE Feineigle's DIC reconstruction algorithm
%   This algorithm reconstructs DIC images based on a variational
%   framework. The related publication is:
%   Feineigle, Patricia A., Andrew P. Witkin, and Virginia L. Stonick. 
%   "Processing of 3D DIC microscopy images for data visualization."
%   icassp. IEEE, 1996.

g = initInputImage(g);
%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultNumIter = 2000;
defaultLambda1 = 10^1;
defaultLambda2 = 10^-2;
defaultBackgroundValue = 1;
defaultBackground = 0;
defaultShow = false;
defaultUseVarianceAsBg = false;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'numIter', defaultNumIter, @isnumeric);
addParameter(p, 'lambda1', defaultLambda1, @isnumeric);
addParameter(p, 'lambda2', defaultLambda2, @isnumeric);
addParameter(p, 'backgroundValue', defaultBackgroundValue, @isnumeric);
addParameter(p, 'background', defaultBackground, @(x) isnumeric(x) || islogical(x) );
addParameter(p, 'show', defaultShow, @islogical);
addParameter(p, 'useVarianceAsBg', defaultUseVarianceAsBg, @islogical);
parse(p, varargin{:});

direction = p.Results.direction*pi/180;
numIter = p.Results.numIter;
lambda1 = p.Results.lambda1;
lambda2 = p.Results.lambda2;
fdb = p.Results.backgroundValue;
bg = p.Results.background;
show = p.Results.show;
useVariance = p.Results.useVarianceAsBg;
if numel(bg)==1
    if useVariance
        bg = 1-stdfilt(g, ones(9));
%         bg = 1-(bg>graythresh(bg));
    else
        bg = ones(size(g));
    end
end

%% setting constants
sx = cos(direction);
sy = -sin(direction);
a = sx^2 + lambda1*sy^2;
b = sy^2 + lambda1*sx^2;
c = 0.5*(1-lambda1)*sx*sy;
d = lambda2*b + 2*a + 2*b;
K = [-c, b, c;
     a, 0, a;
     c, b, -c];
dx = getCDiffKernel(1,0);
dy = getCDiffKernel(0,1);
borderOpt = 'replicate';
gx = sx/2 * imfilter(g, dx, borderOpt);
gy = sy/2 * imfilter(g, dy, borderOpt);
if show
    figure
end

%% iterations
f = zeros(size(g));
for i = 1:numIter
    f = 1/d * (imfilter(f, K, borderOpt) + lambda2*bg*fdb - gx - gy);
    if show
        imagesc(f), axis square, colormap gray
        drawnow
    end
end
f = f-mean2(f);
end

