
%% Set parameters of the target.

% Following numbers are from Fig 8.2 of the book chapter on MBL/NNF
% target.
% Rout=75/2;% Outer radius of siemens star.
% Rin=0.6; %Inner radius of siemens star.

% RISiO2=1.59;
% ThickSiO2=0.09;
RISiO2=1.59;
ThickSiO2=0.1;

%% Parameters of the DIC microscope.
% Parameters correspond to the experimental image that we use in the paper.

% DICparams.wavelength=0.546;
DICparams.wavelength=0.550;
DICparams.NAo=1.51;
% DICparams.NAo=6;
% DICparams.NAo=5;
% DICparams.NAo=0.75;
% DICparams.NAc=0.95; % Between the coverglass and coverslip airgap was present. Condenser was oiled though.
% DICparams.NAc=1;
% DICparams.NAc=15;
% DICparams.NAc=5;
DICparams.NAc=15;
DICparams.nEmbb=1;
DICparams.nImm=1.515;
% DICparams.nImm=1;
% DICparams.shear=0.48/2; %Measured from fringe.
DICparams.shear=DICparams.wavelength/DICparams.NAo;
% DICparams.shear=0.01;
DICparams.bias=45; 
% DICparams.bias=25; 
DICparams.shearangle=45;

% myTfun = zeros(size(syntheticImage));
% myTfun(round(size(myTfun,1)/2), round(size(myTfun,2)/2)) = 1;
% % myTfun(26:75, 26:75) = 1;
% % myTfun(:) = 1;
% DICparams.Tfun = myTfun;

%% Compute specimen transmission.

% xsim = 0:0.025:0.025*99;
% xsim = 0:0.09:0.09*99;
xsim = 0:0.095:0.095*99;
%2nm sampling is required to properly sample slight over or under-etching of the target
usim=0;
% usim=-1:0.2:1;  % To simulate defocus.

% Assume slight overetching and therefore non-square azimuthal phase
% grating. 
% OverEtchProfile=mblnnfSiemens(xsim,xsim,0.04); 

% oplSample=(2*pi/DICparams.wavelength)*(DICparams.nEmbb-RISiO2)*OverEtchProfile*ThickSiO2;
% sample=exp(1i*oplSample);

syntheticImage = im2double(imread('/home/koosk/BRC/DIC_Images/synthetic/circandcube.png'));
oplSample=(2*pi/DICparams.wavelength)*(DICparams.nEmbb-RISiO2)*syntheticImage*ThickSiO2;
sample=exp(1i*oplSample);


DICMicroscope=microlith(xsim,usim); % Second argument is z.

% Compute system pupils and the image according to the model proposed by
% Mehta and Sheppard.
DICMicroscope.computesys('DIC',DICparams);
DICImage=DICMicroscope.computeimage(sample,'CPU');

% Compute system pupils and the image according to the model proposed by
% Preza et al.
DICMicroscope.computesys('DIC-Preza',DICparams);
DICPrezaImage=DICMicroscope.computeimage(sample,'CPU');

% Compute system pupils and the image for PlasDIC system.
DICparams.NAc=0.25;
DICMicroscope.computesys('PlasDIC',DICparams);
PlasDICImage=DICMicroscope.computeimage(sample,'CPU');

%% Compare the etch profile and images.
% Dark regions are etched and white regions are intact silica.
% Images are linked, so panning or zooming one will do the same for all
% images.
DICDisp=gray2norm(DICImage);
DICPrezaDisp=gray2norm(DICPrezaImage);
PlasDICDisp=gray2norm(PlasDICImage);
figure(1); clf;

set(1,'color','white','Position',[100 100 800 800],'defaultaxesfontsize',14); 
colormap gray;
ha=imagecat(xsim,xsim,syntheticImage,DICDisp,DICPrezaDisp,PlasDICDisp,'equal','link');
% All four images are linked, so zooming or panning one will do the same on
% all others.

axes(ha(1)); title('Etched pattern');
axes(ha(2)); title('DIC image (model: Mehta and Sheppard)'); 
set(gca,'Clim',[0 1]);
axes(ha(3)); title('DIC image (model: Preza et al)');
set(gca,'Clim',[0 1]);
axes(ha(4)); title('PlasDIC image');
set(gca,'Clim',[0 1]);

xlim([min(xsim) max(xsim)]); ylim([min(xsim) max(xsim)]); 
% The features at the edges (about 200nm or the size of the image of a
% point) are affected by the edge artifacts.