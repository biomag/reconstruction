function structuresTest1()
%SYNTHETICTEST1 Synthetic DIC test
% K1 = getGaussDeriv('sigma', 0.1); K1 = K1./sum(abs(K1(:)));
% K2 = getGaussDeriv('sigma', 0.5); K2 = K2./sum(abs(K2(:)));
K3 = getGaussDeriv('sigma', 1.5); K3 = K3./sum(abs(K3(:)));
% 
% psf = {K1, K2, K3};
psf = K3;
% imagePath = {'../DIC_Images/DIC_structures/cubes/oblique_20x20x1.5um/1501152_20xobj_1micron_interval/15.tif',...
%     '../DIC_Images/DIC_structures/cubes/oblique_20x20x1.5um/1501151_20xobj_1micron_interval/21.tif'};
imagePath = {'../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/00.png',...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/01.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/02.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/03.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/04.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/05.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/06.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/07.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/08.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/09.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/10.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/11.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/12.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/14.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/15.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/16.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/17.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/18.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/19.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/20.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/21.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/22_1.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/22_2.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/23.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/24.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/25.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/26.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/27.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/28.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/29.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/30.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/31.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/32.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/33.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/34.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/35.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/36.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/37.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/38.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/39.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/40.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/41.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/42.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/44.png', ...
    '../DIC_Images/DIC_structures/cubes/DIC_20x20x1.5_20150413/p1_40obj/46.png' ...
    };
direction = num2cell(repmat(-45,1,numel(imagePath)));

% algorithm = {@dicEMM, @dicZhaozhengYin, @dicFeineigle, @dicWiener};
% algspecParams = {{'numiter', 20000, 'pyrsize', 1, 'wsmooth', 0.01, 'psf', 1},...
%     {'kernelSigma', 2.5}, {'numiter', 20000, 'lambda1', 1/15}, {'kernelSigma', 20}};
algorithm = {@dicEMM, @dicZhaozhengYin, @dicFeineigle, @dicWiener};
algspecParams = {{'numiter', 20000, 'pyrsize', 1, 'wsmooth', 0.01, 'psf', 1}, ...
    {'kernelSigma', 2.5}, {'numiter', 20000}, {'kernelSigma', 15}};

resultsFolder = '../DIC_Images/results/StructuresTest/4/';

[headers, params] = generalTest('image', imagePath, 'algorithm', algorithm, 'algspecParams', ...
    algspecParams, 'direction', direction, 'resultsFolder', resultsFolder);
resultFiles = cellfun(@(x) strcat(resultsFolder, x, '.mat'), params(:,1), 'uniformoutput', false);
results = cellfun(@(x) loadResultFile(x), resultFiles, 'uniformoutput', false);

statHeaders = {'norm_conv',... % 1 (3)
               'corr_conv',... % 2 (6)
               'MSE_norm_conv', 'SE_SD_norm_conv'... % 3-4 (9-10)
               'MAE_norm_convolved', 'AE_SD_norm_convolved',... % 5-6 (13-14)
               'pos_norm_convolved',... % 7 (17)
               'pos_corr_convolved',... % 8 (20)
               'pos_MSE_norm_conv', 'pos_SE_SD_norm_conv'... % 9-10 (23-24)
               'pos_MAE_norm_conv', 'pos_AE_SD_norm_conv', ... % 11-12 (27-28)
               'largest_area_1', 'largest_area_2', ... % 13-14
               'largest_area_3', 'largest_area_4', ... % 15-16
               'filled_largest_area_1', 'filled_largest_area_2', ... % 17-18
               'filled_largest_area_3', 'filled_largest_area_4', ... % 19-20
               };
numResults = length(results);
stats = cell(numResults, numel(statHeaders));
for i = 1:length(results)
    dicImagePath = imagePath{params{i,end}};
    dicImage = im2double(imread(dicImagePath));
    resim = results{i};
    posresim = resim;
    posresim(posresim<0) = 0;
    conv_resim = imfilter(resim./max(abs(resim(:))),psf);
    
    stats{i,1} = norm(mat2gray(dicImage) - mat2gray(conv_resim));
    stats{i,2} = corr2(dicImage,conv_resim);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_resim)).^2;
    stats{i,3} = mean2(sqdiffconv);
    stats{i,4} = std2(sqdiffconv);
    diffconv = mat2gray(dicImage) - mat2gray(conv_resim);
    stats{i,5} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,6} = std2(abs(diffconv));
    
    conv_posresim = imfilter(posresim./max(abs(posresim(:))),psf);
    stats{i,7} = norm(mat2gray(dicImage) - mat2gray(conv_posresim));
    stats{i,8} = corr2(dicImage,conv_posresim);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_posresim)).^2;
    stats{i,9} = mean2(sqdiffconv);
    stats{i,10} = std2(sqdiffconv);
    diffconv = mat2gray(dicImage) - mat2gray(conv_posresim);
    stats{i,11} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,12} = std2(abs(diffconv));
    
    nposresim = mat2gray(posresim);
    binim = nposresim>=graythresh(nposresim);
    binimfill = imfill(binim, 'holes');
    props = regionprops(binim);
    areas = [props.Area];
    N = min(numel(areas),4);
    [~, sortIdx] = sort(areas, 'descend');
    maxNAreas = areas(sortIdx(1:N));
    k = 13;
    for j = 1:N
        stats{i,k} = maxNAreas(j);
        k = k + 1;
    end
    for j = N+1:N
        stats{i,j} = {};
    end
    props = regionprops(binimfill); % filled areas
    areas = [props.Area];
    N = min(numel(areas),4);
    [~, sortIdx] = sort(areas, 'descend');
    maxNAreas = areas(sortIdx(1:N));
    k = 17;
    for j = 1:N
        stats{i,k} = maxNAreas(j);
        k = k + 1;
    end
    for j = N+1:N
        stats{i,j} = {};
    end
end

numHeaders = numel(headers);
numStatHeaders = numel(statHeaders);
numResults = length(results);
fid = fopen([resultsFolder,'stats.csv'], 'wt');
for i = 1:numHeaders
    fprintf(fid, '%s', headers{i});
    fprintf(fid, ',');
end
for i = 1:numStatHeaders
    fprintf(fid, '%s', statHeaders{i});
    if i ~= numStatHeaders
        fprintf(fid, ',');
    else
        fprintf(fid, '\n');
    end
end
for i = 1:numResults
    for j = 1:numHeaders
        fprintf(fid, '%s', any2str(params{i,j}));
        fprintf(fid, ',');
    end
    for j = 1:numStatHeaders
        fprintf(fid, '%s', any2str(stats{i,j}));
        if j ~= numStatHeaders
            fprintf(fid, ',');
        else
            fprintf(fid, '\n');
        end
    end
end
fclose(fid);
end % structuresTest

function image = loadResultFile(path) %#ok
load(path);
end