% input = resz;
% filterSize = 31;
% sigma = 0.3;
% direction = 0;
% image = input;
% nsr = 10;

% input = nevtelen;
% filterSize = 9;
% sigma = 1.5;
% direction = 135;
% image = input;
% nsr = 0.1;

input = disc;
filterSize = 7;
sigma = 1;
direction = -135;
image = input;
nsr = 10;

% filterSize = 27;
% sigma = 1.5;
% direction = 0;
% input = zeros(128);
% input(:,48:80) = 1;
% g = fspecial('gaussian', [floor(filterSize/2), floor(filterSize/2)], sigma);
% PSF = [-g, g];
% PSF = imrotate(PSF, direction-180, 'bilinear');
% lineDic = mat2gray(conv2(input, PSF, 'same'));
% image = lineDic;
% nsr = 0.001;


r = dicWiener(image, 'direction', direction, 'sigma', sigma, 'filterSize', filterSize, 'NSR', nsr);
g = fspecial('gaussian', [floor(filterSize/2), floor(filterSize/2)], sigma);
PSF = [-g, g];
PSF = imrotate(PSF, direction-180, 'bilinear');
dic = conv2(r, PSF, 'same');
figure
subplot(1,4,1), imagesc(input)
subplot(1,4,2), imagesc(image)
subplot(1,4,3), imagesc(r)
subplot(1,4,4), imagesc(dic)
colormap(gray)