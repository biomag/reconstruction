function folder_mat2png( folderPath, keyword, cropNegative )
%FOLDER_MAT2PNG Converts all mat files in a folder to png files
%   The mat files have to contain a properly named variable.

if nargin < 3
    cropNegative = true;
end
if nargin < 2
    keyword = '';
end
files = dir( fullfile(folderPath, ['*', keyword, '*.mat']) );

inputFiles = cell(numel(files),1);
for i = 1:length(files)
    inputFiles{i} = fullfile(folderPath, files(i).name);
end

for i = 1:numel(inputFiles)
    [~, filename, ~] = fileparts(inputFiles{i});
    resim = loadResultFile([folderPath, filesep, filename, '.mat']);
    if cropNegative
        resim(resim<0) = 0;
    end
    imwrite(mat2gray(resim), [folderPath, filesep, filename, '.png'], 'png');
end

end

