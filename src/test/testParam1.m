images = {'disc', 'nevtelen', 'resz'};
directions = [135, -135, 0];
resultsFolder = './DIC_Images/results/paramTest/';
numiter = 1200;
pyramids = [1, 3];
sigmas = 5:0.5:7;
smoothws = [0, 0.0125];
methods = {@dicEMM, @dicEMMderK};
K = cell(1,3);
res = cell(1,3);

imageDatas = cell(1, numel(images));
for i = 1:numel(images)
    imageDatas{i} = eval(images{i});
end
numRuns = numel(images)*numel(pyramids)*numel(sigmas)*numel(smoothws)*numel(methods)*numel(K);
numPars = 7;
parameters = struct([]);
parCounter = 0;
for imgIdx = 1:numel(images)
    for pyrIdx = 1:numel(pyramids)
        for sigmaIdx = 1:numel(sigmas)
            for smoothIdx = 1:numel(smoothws)
                for methodIdx = 1:numel(methods)
                    method = methods{methodIdx};
                    sigma = sigmas(sigmaIdx);
                    kernelSize = 6*sigma+1;
                    K{1} = getGaussianKernel(kernelSize, sigma);
                    K{2} = getGaussianDirCosKernel(kernelSize, sigma, -directions(imgIdx));
                    K{3} = getGaussianRoundCosKernel(kernelSize, sigma);
                    
                    commonNamepart = [resultsFolder, func2str(method), '_image_', images{imgIdx}, ...
                        '_direction_', num2str(directions(imgIdx)),...
                        '_numiter_', num2str(numiter), '_pyrSize_', num2str(pyramids(pyrIdx)),...
                        '_wsmooth_', num2str(smoothws(smoothIdx)), '_sigma_', num2str(sigma), ...
                        '_kernelSize_', num2str(6*sigma+1)];
                    filepaths = {[commonNamepart, '_kernel_', 'Gaussian'],...
                                 [commonNamepart, '_kernel_', 'GaussianDirCos']...
                                 [commonNamepart, '_kernel_', 'GaussianRoundCos']};
                    
                    for kernelIdx = 1:3
                        parameters(parCounter+kernelIdx).method = method;
                        parameters(parCounter+kernelIdx).imgIdx = imgIdx;
                        parameters(parCounter+kernelIdx).direction = directions(imgIdx);
                        parameters(parCounter+kernelIdx).K = K{kernelIdx};
                        parameters(parCounter+kernelIdx).pyr = pyramids(pyrIdx);
                        parameters(parCounter+kernelIdx).smooth = smoothws(smoothIdx);
                        parameters(parCounter+kernelIdx).sigma = sigma;
                        parameters(parCounter+kernelIdx).kernelSize = kernelSize;
                        parameters(parCounter+kernelIdx).filepath = filepaths{kernelIdx};
                    end
                    parCounter = parCounter + 3;
                end
            end
        end
    end
end

parfor i = 1:numel(parameters)
    method = parameters(i).method;
    image = imageDatas{parameters(i).imgIdx};
    direction = parameters(i).direction;
    K = parameters(i).K;
    pyr = parameters(i).pyr;
    wsmooth = parameters(i).smooth;
    tic;
    res = method(image, 'direction', direction, 'numiter', numiter, 'customKernel', K, ...
        'pyrsize', pyr, 'wsmooth', wsmooth);
    time = toc;
    imwrite(mat2gray(res), [parameters(i).filepath, '_time_', num2str(time), '.png'], 'png');
end