function generalStructuresTest( folder, direction, varargin)
%function generalStructuresTest( folder, direction, extension, numiter )
%GENERALSTRUCTURESTEST Summary of this function goes here
%   Detailed explanation goes here

%% parse inputs
p = inputParser;
defaultAlgorithm = {@dicEMM};
defaultAlgspecParams = {{}};
defaultExtension = 'tif';

addParameter(p, 'algorithm', defaultAlgorithm);
addParameter(p, 'algspecParams', defaultAlgspecParams);
addParameter(p, 'extension', defaultExtension);
parse(p, varargin{:});

algorithm = p.Results.algorithm;
algspecParams = p.Results.algspecParams;
extension = p.Results.extension;

if ~strcmp(folder(end), filesep)
    folder = [folder, filesep];
end
results = reconstructFolder(folder, direction, 'extension', extension, 'algorithm', algorithm, ...
    'algspecParams', algspecParams);

resultsFolder = [folder, 'reconstructions', filesep];
gtFolder = [folder, '..', filesep, 'fluorescent', filesep]; % syntheticImagePath
gtFolderElements = dir([gtFolder, '*']);
gtFiles = cellfun(@(x) [gtFolder, x], {gtFolderElements.name}, 'uniformoutput', false);
gtFiles = gtFiles(~[gtFolderElements.isdir])';
createStatsForSynthetic(results, [], getPsfApproximation(direction), resultsFolder, gtFiles);
movefile([resultsFolder, 'stats.csv'], [folder, '..']);


end
