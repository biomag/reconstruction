resultsFolder = '../DIC_Images/results/transformTest/disc/';
imageData = disc;
% image = image(128:end, 1:200);
% image = image - mean(image(:));
direction = -135;
for i = 5:9
    disp(num2str(i));
    for j = -10:5
        for sigma = 0.5:0.5:5
        K = -getGaussDeriv('size', 6*sigma+1, 'sigma', sigma);
        image = dicZhaozhengYin(imageData, 'direction', direction, 's', 10^i, 'r', 10^j, 'diffKernel', K);
        filename = ['invfilt_', 'disc_s10^', num2str(i), '-r10^', num2str(j),...
            '-sigma', num2str(sigma)];
        imwrite(imadjust(image), [resultsFolder, filename, '.png'], 'png');
        save([resultsFolder, filename, '.mat'], 'image');
%         modes(i+11, j+11) = mode(uint8(256*mat2gray(res(:))));
%         [gx, gy] = gradient(mat2gray(res));
%         grads(i+11, j+11) = sum(gx(:).^2 + gy(:).^2);
        end
    end
end
% for dir = 0:359
%     res = dicZhaozhengYin(image, 'direction', dir);
%     imwrite(mat2gray(res), [resultsFolder, 'direction', num2str(dir), '.png'], 'png');
% end
clear res