function structureTest1( )
%STRUCTURETEST0 First structures test on a not final cube image set
%   The file names should be similar to those in the folder where the path
%   in the code points.

numiter = 10000;
generalStructuresTest('../../DIC_Images/DIC_structures/cubes/dic-fluor-20150701/DIC/', -45, 'extension', 'tif',...
    'algorithm', {@dicEMM, @dicZhaozhengYin, @dicWiener, @dicFeineigle, @dicCvxSocp}, ...
    'algspecParams', {{'numiter', numiter, 'waccept', 0.5}, {}, {}, {'numiter', numiter}, ...
    {'beta', 10^-5, 'tau', 10^-5, 'numiter', numiter}});

end

