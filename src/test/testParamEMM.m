function [ output_args ] = testParamEMM( input_args )
%TESTPARAMSOCP Summary of this function goes here
%   Detailed explanation goes here

psfDirection = -45;
psf = getPsfApproximation(psfDirection);
rotation = {[0, -15, -45]};
syntheticImagePath = {'../DIC_Images/synthetic/cube.tif'};%, ...
%              '../DIC_Images/synthetic/circle.tif', ...
%              '../DIC_Images/synthetic/triangle.png', ...
%              '../DIC_Images/synthetic/star.png', ...
%              '../DIC_Images/synthetic/microscope.png', ...
%              '../DIC_Images/synthetic/circandcube.png', ...
%              '../DIC_Images/synthetic/diamond.png', ...
%              '../DIC_Images/synthetic/ellipse.png', ...
%              '../DIC_Images/synthetic/gear.png', ...
%              '../DIC_Images/synthetic/Lsign.png', ...
%              '../DIC_Images/synthetic/moon.png', ...
%              '../DIC_Images/synthetic/pentagon.png', ...
%              '../DIC_Images/synthetic/pie.png', ...
%              '../DIC_Images/synthetic/pipe.png', ...
%              '../DIC_Images/synthetic/rectangle.png', ...
%              '../DIC_Images/synthetic/circandcube_disttransf.png', ...
%              '../DIC_Images/synthetic/cone.png', ...
%              '../DIC_Images/synthetic/hangar.png', ...
%              '../DIC_Images/synthetic/pyramid.png' ...
%          };
algorithm = {@dicEMM};
algspecParams = {{'numiter', 100000}};
resultsFolder = '../DIC_Images/results/paramTest/EMM/1/';
wAccept = num2cell([0.25, 0.30, 0.5]);
noise = {Inf};


% psf = cell(numel(sigmas),1);
% for i = 1:numel(sigmas)
%     K = getGaussDeriv('sigma', sigmas{i});
%     K = K./sum(abs(K(:)));
%     psf{i} = imrotate(K, psfDirection);
% end

numRotations = sum(cellfun(@numel, rotation));
syntheticImage = cellfun(@(x)im2double(imread(x)) ,syntheticImagePath, 'uniformoutput', false);
inputImagePaths = cell(numRotations,1);
rotatedSyntheticImages = cell(numRotations,1);
mkdir(resultsFolder,'input_images');
mkdir(resultsFolder,'rotated_images');
ctr = 0;
for i = 1:numel(syntheticImage)
    curr_rotation = rotation{i};
    for j = 1:numel(curr_rotation)
        for k = 1:numel(noise)
            noiseLevel = noise{k};
            ctr = ctr + 1;
            rotatedImg = mat2gray(imrotate(syntheticImage{i}, curr_rotation(j), 'bilinear', 'crop'));
            img = imfilter(rotatedImg, psf, 'replicate');
            img = mat2gray(addWhiteNoise(img, noiseLevel));
            [~, fname, ~] = fileparts(syntheticImagePath{i});
            inputImgPath = [resultsFolder,'input_images/', fname, '_rotation', num2str(curr_rotation(j)), ...
                '_psf', num2str(j), '_noise', num2str(noiseLevel), '.tif'];
            rotSynthPath = [resultsFolder,'rotated_images/', fname, '_rotation', num2str(curr_rotation(j)), '.png'];
            inputImagePaths{ctr} = inputImgPath;
            imwrite(img, inputImgPath);
            rotatedSyntheticImages{ctr} = rotSynthPath;
            imwrite(rotatedImg, rotSynthPath, 'png');
        end
    end
end

results = generalTest('image', inputImagePaths, 'algorithm', ...
    algorithm, 'algspecParams', algspecParams, 'imgspecParams', {'direction', num2cell(repmat(psfDirection, numel(inputImagePaths),1))}, ...
    'resultsFolder', resultsFolder, 'wAccept', wAccept);
createStatsForSynthetic(results, inputImagePaths, psf, resultsFolder, rotatedSyntheticImages);


end
