function results = reconstructFolder( folderPath, direction, varargin )
%RECONSTRUCTFOLDER Reconstructs all DIC images in a folder
%   Additional input parameters:
%    - extension: extension of file names that should be reconstructed,
%    default is tif
%    - numiter: number of iterations, default is 10,000.

%% parse inputs
p = inputParser;
defaultExtension = '';
defaultAlgorithm = {@dicEMM};
defaultAlgspecParams = {{'numiter', 10000, 'waccept', 0.5}};

addParameter(p, 'extension', defaultExtension, @ischar);
addParameter(p, 'algorithm', defaultAlgorithm);
addParameter(p, 'algspecParams', defaultAlgspecParams);
parse(p, varargin{:});

extension = p.Results.extension;
algorithm = p.Results.algorithm;
algspecParams = p.Results.algspecParams;

%% main part
if ~strcmp(folderPath(end), filesep)
    folderPath = strcat(folderPath, filesep);
end

files = dir( fullfile(folderPath,['*.', extension]) );

inputFiles = cell(numel(files),1);
for i = 1:length(files)
    inputFiles{i} = fullfile(folderPath, files(i).name);
end

resultsFolder = [folderPath, 'reconstructions', filesep];

results = generalTest('image', inputFiles, 'algorithm', algorithm, 'algspecParams', algspecParams, ...
    'imgspecParams', {'direction', num2cell(repmat(direction, length(inputFiles),1))}, ...
    'resultsFolder', resultsFolder);

for i = 1:numel(results)
    filename = results(i).image;
    filename_norm = [filename, '_normalized'];
    resim = loadResultFile([resultsFolder,filename, '.mat']);
    imwrite(mat2gray(resim), [resultsFolder, filename_norm, '.png'], 'png');
    resim(resim<0) = 0;
    resim = mat2gray(resim);
    imwrite(resim, [resultsFolder, filename, '.png'], 'png');
end

end
