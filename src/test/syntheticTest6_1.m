function syntheticTest6_1()
%SYNTHETICTEST6_1 Synthetic DIC test 6 extending 5 by more images

psfDirection = -45;
rotation = {0:-15:-90+1, ...
            0, ...
            0:-15:-105, ...
            0:-15:-75, ...
            -90:15:90-1, ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            [0, -15, -30], ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            0:-15:-60, ...
            0:-15:-90+1, ...
            0, ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            0, ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            0:-15:-90+1, ...
            };
syntheticImagePath = {'../../DIC_Images/synthetic/cube.tif', ...
             '../../DIC_Images/synthetic/circle.tif', ...
             '../../DIC_Images/synthetic/triangle.png', ...
             '../../DIC_Images/synthetic/star.png', ...
             '../../DIC_Images/synthetic/microscope.png', ...
             '../../DIC_Images/synthetic/circandcube.png', ...
             '../../DIC_Images/synthetic/diamond.png', ...
             '../../DIC_Images/synthetic/ellipse.png', ...
             '../../DIC_Images/synthetic/gear.png', ...
             '../../DIC_Images/synthetic/Lsign.png', ...
             '../../DIC_Images/synthetic/moon.png', ...
             '../../DIC_Images/synthetic/pentagon.png', ...
             '../../DIC_Images/synthetic/pie.png', ...
             '../../DIC_Images/synthetic/pipe.png', ...
             '../../DIC_Images/synthetic/rectangle.png', ...
             '../../DIC_Images/synthetic/circandcube_disttransf.png', ...
             '../../DIC_Images/synthetic/cone.png', ...
             '../../DIC_Images/synthetic/hangar.png', ...
             '../../DIC_Images/synthetic/pyramid.png' ...
             '../../DIC_Images/synthetic/multilevel_blocks.png'...
         };
% rotation = {0:-15:-90+1, ...
%             0, ...
%             0:-15:-90+1, ...
%             };
% syntheticImagePath = {'../../DIC_Images/synthetic/cube.tif', ...
%              '../../DIC_Images/synthetic/circle.tif', ...
%              '../../DIC_Images/synthetic/multilevel_blocks.png'...
%          };
noise = {Inf};
algorithm = {@dicEMM, @dicZhaozhengYin, @dicWiener, @dicHilbert};
% algorithm = {@dicHilbert};
% algorithm = {@dicEMM, @dicZhaozhengYin, @dicWiener};
algspecParams = {{'numiter', 100000, 'wsmooth', 0, 'wAccept', 0.25}, {}, {}, {}};
% algspecParams = {{}};
% algspecParams = {{'numiter', 1, 'wsmooth', 0, 'wAccept', 0.25}, {}, {}};
% algspecParams = {{'numiter', 20, 'pyrsize', 1, 'wsmooth', 0.01}, {}, {}};
resultsFolder = '../../DIC_Images/results/SyntheticTest/12_1_1/';
% resultsFolder = '../../DIC_Images/results/SyntheticTest/12_1_1_hilbert/';

generalSyntheticTest(syntheticImagePath, rotation, noise, psfDirection, algorithm, algspecParams, resultsFolder);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

noise = {20};
algspecParams = {{'numiter', 100000, 'wsmooth', 0.01, 'wAccept', 0.25}, {}, {}, {}};
resultsFolder = '../../DIC_Images/results/SyntheticTest/12_1_2/';
% algspecParams = {{}};
% resultsFolder = '../../DIC_Images/results/SyntheticTest/12_1_2_hilbert/';

generalSyntheticTest(syntheticImagePath, rotation, noise, psfDirection, algorithm, algspecParams, resultsFolder);

end % syntheticTest