function cellTest3( )
%CELLTEST3 Cell test with precond algorithm
%   Cell test on 20x objective cell images. This file only runs software
%   tha only runs on Windows OS.
generalCellTest('../../DIC_Images/CHO/5/20x/', 'algorithm', {@precondWinWrapper}, ...
    'algspecParams', {{'numiter', 10000}});

end

