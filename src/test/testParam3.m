images = {'cho6small'};%{'blood1', 'cho6', 'disc', 'nevtelen'};
directions = -135; %[45, -135, 135, -135];
resultsFolder = '../DIC_Images/results/paramTest/4/';
numiters = [700, 4000];%[700, 1200, 4000, 10000];
pyramids = [1,3]; %[1, 2, 3, 4];
sigmas = 1:5;
smoothws = [0, 0.00625, 0.0125, 0.025];
% methods = {@dicEMM};
% K = cell(1,3);
% res = cell(1,3);

imageDatas = cell(1, numel(images));
for i = 1:numel(images)
    imageDatas{i} = eval(images{i});
end
numRuns = numel(images)*numel(numiters)*numel(pyramids)*numel(sigmas)*numel(smoothws);
parameters = struct([]);
parCounter = 0;
for imgIdx = 1:numel(images)
    for iterIdx = 1:numel(numiters)
        for pyrIdx = 1:numel(pyramids)
            for sigmaIdx = 1:numel(sigmas)
                for smoothIdx = 1:numel(smoothws)
                    sigma = sigmas(sigmaIdx);
                    kernelSize = 6*sigma+1;
                    pyrSize = pyramids(pyrIdx);
                    numiter = numiters(iterIdx);
                    smoothw = smoothws(smoothIdx);

                    filepath = [resultsFolder, 'dicEMM', '_image_', images{imgIdx}, ...
                        '_direction_', num2str(directions(imgIdx)),...
                        '_numiter_', num2str(numiter), '_pyrSize_', num2str(pyrSize),...
                        '_wsmooth_', num2str(smoothw), '_sigma_', num2str(sigma), ...
                        '_kernelSize_', num2str(6*sigma+1)];
%                         filepaths = {[commonNamepart, '_kernel_', 'Gaussian'],...
%                                      [commonNamepart, '_kernel_', 'GaussianDirCos']...
%                                      [commonNamepart, '_kernel_', 'GaussianRoundCos']};

                    parCounter = parCounter + 1;
                    parameters(parCounter).imgIdx = imgIdx;
                    parameters(parCounter).direction = directions(imgIdx);
%                         parameters(parCounter).K = K{kernelIdx};
                    parameters(parCounter).numiter = numiter;
                    parameters(parCounter).pyr = pyramids(pyrIdx);
                    parameters(parCounter).smooth = smoothws(smoothIdx);
                    parameters(parCounter).sigma = sigma;
                    parameters(parCounter).kernelSize = kernelSize;
                    parameters(parCounter).filepath = filepath;
                end
            end
        end
    end
end

results = cell(1,numel(parameters));
disp(['Started ', num2str(numel(parameters)), ' tests.']);
parfor i = 1:numel(parameters)
    image = imageDatas{parameters(i).imgIdx};
    numiter = parameters(i).numiter;
    direction = parameters(i).direction;
    pyr = parameters(i).pyr;
    wsmooth = parameters(i).smooth;
    kernelSize = parameters(i).kernelSize;
    sigma = parameters(i).sigma;
    
    tic;
    res = dicEMM(image, 'direction', direction, 'numiter', numiter, 'kernelSize', kernelSize,...
        'kernelSigma', sigma, 'pyrsize', pyr, 'wsmooth', wsmooth);
    time = toc;
    imwrite(imadjust(res), [parameters(i).filepath, '_time_', num2str(time), '.png'], 'png');
    results{i} = res;
    %save([parameters(i).filepath, '.mat'], res);
end

for i = 1:numel(parameters)
    image = results{i};
    save([parameters(i).filepath, '.mat'], 'image');
end

