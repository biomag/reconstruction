function cellTest2( )
%CELLTEST1 Cell test run with SOCP
%   Cell test on 20x objective cell images. This file only runs the slow
%   SOCP algorithm.
generalCellTest('../../DIC_Images/CHO/5/20x/', 'algorithm', {@dicCvxSocp}, ...
    'algspecParams', {{'numiter', 10000, 'beta', 10^-4, 'tau', 10^-3}});

end

