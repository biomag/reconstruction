function generalCellTest( folder, varargin )
%GENERALREALTEST Summary of this function goes here
%   Detailed explanation goes here

%% parse inputs
p = inputParser;
defaultAlgorithm = {@dicEMM};
defaultAlgspecParams = {{}};
defaultDirection = -45;
defaultExtension = 'png';
defaultOutlineExtension = 'png';

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'extension', defaultExtension, @ischar);
addParameter(p, 'outlineExtension', defaultOutlineExtension, @ischar);
addParameter(p, 'algorithm', defaultAlgorithm);
addParameter(p, 'algspecParams', defaultAlgspecParams);
parse(p, varargin{:});

direction = p.Results.direction;
extension = p.Results.extension;
outlineExtension = p.Results.outlineExtension;
algorithm = p.Results.algorithm;
algspecParams = p.Results.algspecParams;

%% init
if ~strcmp(folder(end), filesep)
    folder = [folder, filesep];
end

%% run
results = reconstructFolder(folder, direction, 'extension', extension, 'algorithm', algorithm, ...
    'algspecParams', algspecParams);

%% statistics
% TODO create statistics in a separate function that can be called with the
% results parameter (loaded from file)
outlinesFolder = [folder, 'outlines', filesep];
resultsFolder = [folder, 'reconstructions', filesep];
rocData = struct('X', [], 'Y', [], 'T', [], 'AUC', []);
rocData(length(results)).X = [];
for i = 1:length(results)
    [~, inputFname, ~]= fileparts(results(i).inputImage);
    gt = im2double(imread([outlinesFolder, inputFname, '.', outlineExtension]));
    gt = imfill(gt, 'holes');
    resim = loadResultFile([resultsFolder, results(i).image, '.mat']);
    resim(resim<0) = 0;
    resim = mat2gray(resim);
    imwrite(mat2gray(resim), [resultsFolder, results(i).image, '.png'], 'png');
    [rocData(i).X, rocData(i).Y, rocData(i).T, rocData(i).AUC] = perfcurve(gt(:) > 0, resim(:), true, ...
        'Options', statset('UseParallel', true), 'XVals', 0:0.001:1, 'UseNearest', 'off');
end
save([folder, 'stats.mat'], 'rocData');
saveStatisticsCsv(results, rocData, folder);

end

function saveStatisticsCsv(results, stats, path)
fid = fopen([path, 'stats.csv'], 'wt');
printHeaders(fid, results, false);
fprintf(fid,'AUC\n');
for i = 1:length(results)
    printFields(fid, results(i), false);
    fprintf(fid, '%s\n', toString(stats(i).AUC));
end
fclose(fid);
end

function str = toString(obj)
str = any2str(obj);
if ~isa(str, 'char')
    str = num2str(numel(str));
end
end
