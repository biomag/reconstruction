images = {'jurkat', 'nevtelen', 'cho6small'};
directions = [45, -45, -45];
resultsFolder = '../DIC_Images/results/SEMUparamTest/3/';
maxiter = 1000;
alphas = 10.^-[0, 1, 2];
betas = 0:0.01:0.25;
gammas = 0:0.01:0.05;
res = cell(1,3);

imageDatas = cell(1, numel(images));
for i = 1:numel(images)
    imageDatas{i} = eval(images{i});
end
numRuns = numel(images)*numel(alphas)*numel(betas)*numel(gammas);
% numPars = 7;
parameters = struct([]);
parCounter = 0;
for imgIdx = 1:numel(images)
    for alpha = alphas
        for beta = betas
            for gamma = gammas
                parCounter = parCounter + 1;
                filepath = [resultsFolder, images{imgIdx}, ...
                    '_direction_', num2str(directions(imgIdx)),...
                    '_maxiter_', num2str(maxiter),...
                    '_alpha_', num2str(alpha),...
                    '_beta_', num2str(beta), ...
                    '_gamma_', num2str(gamma)];
                parameters(parCounter).imgIdx = imgIdx;
                parameters(parCounter).direction = directions(imgIdx);
                parameters(parCounter).alpha = alpha;
                parameters(parCounter).beta = beta;
                parameters(parCounter).gamma = gamma;
                parameters(parCounter).filepath = filepath;
            end
        end
    end
end


results = cell(1,numel(parameters));
disp(['Started ', num2str(numel(parameters)), ' tests.']);
parfor i = 1:numel(parameters)
    image = imageDatas{parameters(i).imgIdx};
    direction = parameters(i).direction;
    alpha = parameters(i).alpha;
    beta = parameters(i).beta;
    gamma = parameters(i).gamma;
    tic;
    res = dicSEMU(image, 'direction', direction, 'maxiter', maxiter, ...
        'alpha', alpha, 'beta', beta, 'gamma', gamma);
    time = toc;
    imwrite(mat2gray(res), [parameters(i).filepath, '_time_', num2str(time), '.png'], 'png');
    results{i} = res;
end

for i = 1:numel(parameters)
    image = results{i};
    save([parameters(i).filepath, '.mat'], 'image');
end