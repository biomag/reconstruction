function results = generalTest(varargin)
% last modified: 15.05.2015.

assert(mod(nargin,2)==0, ['Arguments should be ''name'', value pairs, ',...
    'but one name or value is missing']);
[image, algorithm, algspecParams, imgspecParams, resultsFolder, parNames, ...
    params]= processInputs(varargin{:});

inputFilepaths = image;
paramvariables = [{'image', 'algorithm'}, parNames{:}];
headers = [paramvariables, {'time'}, {'algspecParams'}, {'imgspecParams'}, {'inputImage'}];

numpars = numel(paramvariables);
params = {image, algorithm, params{:}}; %#ok
parNums = cellfun(@numel, params);
if ~strcmp(resultsFolder(end), filesep)
    resultsFolder = [resultsFolder, filesep];
end

paramvalues = cell(1,numpars);
for i = 1:numpars
    paramvalues{i} = 1:parNums(i);
end
piter = paramiterator(paramvariables,paramvalues);
numRuns = length(piter);

results = struct([]);
for i = 1:numel(headers)
    results(numRuns).(headers{i}) = [];
end
disp(['Started ', num2str(numRuns), ' tests.']);
parfor i = 1:numRuns
    inputParams = cell(2,numpars-2);
    alg = 0;
    img = 0;
    algIdx = 0;
    imgIdx = 0;
    ctr = 0;
    for j = 1:numpars
        parname = paramvariables{j};
        parvalue = params{j}{piter.settingslist{i,j}};
        if strcmp(parname, 'algorithm')
            algIdx = piter.settingslist{i,j};
            alg = parvalue;
            results(i).algorithm = parvalue;
            continue
        elseif strcmp(parname, 'image')
            imgIdx = piter.settingslist{i,j};
            img = parvalue;
            continue
        end
        results(i).(parname) = parvalue;
        ctr = ctr + 1;
        inputParams{1,ctr} = parname;
        inputParams{2,ctr} = parvalue;
    end
    inputParams = inputParams(:);
    inputParams = inputParams';
    currImgspecParams = cell(1, numel(imgspecParams));
    for j = 1:2:numel(imgspecParams)
        currImgspecParams{j} = imgspecParams{j};
        if iscell(imgspecParams{j+1})
            currImgspecParams{j+1} = imgspecParams{j+1}{imgIdx};
        else
            currImgspecParams{j+1} = imgspecParams{j+1}(imgIdx);
        end
    end
    [~, fname, ~] = fileparts(inputFilepaths{imgIdx});
    outputFilename = [any2str(alg), fname, cellfun(@any2str,inputParams, 'uniformoutput', false)];
    outputFilename = cat(2,outputFilename, cellfun(@any2str, algspecParams{algIdx}, ...
        'uniformoutput', false));
    outputFilename = cat(2,outputFilename, cellfun(@any2str, currImgspecParams, ...
        'uniformoutput', false));
    outputFilename = strrep(strjoin(outputFilename), ' ', '_');
    results(i).image = outputFilename;
    algpars = [inputParams{:}, algspecParams{algIdx}, currImgspecParams];
    tic;
    result = alg(img, algpars{:});
    results(i).time = toc;
    results(i).inputImage = img;
    results(i).algspecParams = algspecParams{algIdx};
    results(i).imgspecParams = currImgspecParams;
    parsave(result, [resultsFolder, outputFilename, '.mat']);
end
save([resultsFolder, 'general_results.mat'], 'results');
end % generalTest

function [image, algorithm, algspecParams, imgspecParams, resultsFolder, parNames, params] = ...
    processInputs(varargin)
parNames = cell(max(0,nargin/2-4),1); % #special parameters = 4
params = cell(max(0,nargin/2-4),1);
image = {};
algorithm = {};
algspecParams = {{}};
imgspecParams = cell(0,1);
resultsFolder = './results/';
j = 0;
for i = 1:2:nargin
    name = varargin{i};
    value = varargin{i+1};
    if ~iscell(value)
        value = {value};
    end
    switch name
        case 'image'
            image = value;
        case 'algorithm'
            algorithm = value;
        case 'imgspecParams'
            imgspecParams = value;
        case 'algspecParams'
            algspecParams = value;
        case 'resultsFolder'
            resultsFolder = value{:};
            if ~strcmp(resultsFolder(end), filesep)
                resultsFolder= [resultsFolder, filesep]; %#ok
            end
        otherwise
        j = j + 1;
        parNames{j} = name;
        params{j} = value;
    end
end
% assert(exist(resultsFolder, 'dir')==0, ['Error creating results folder. Define ',...
%     'another location if the current one already exists.']);
assert(numel(algorithm)>0, 'No algorithms specified!');
assert(numel(image)>0, 'No images specified!');
assert(numel(algspecParams) <= numel(algorithm), ['More number of algorithm specific parameters than', ...
    'algorithms!']);
assert(checkNumImgspecParams(imgspecParams, numel(image)), ['Number of image specific parameters should ', ...
    'be 0 or the same as number of images!']);
if numel(algorithm) ~= numel(algspecParams)
    for i = 1:numel(algorithm)
        if i>numel(algspecParams) ...
            || (isempty(algspecParams{i}) && ~iscell(algspecParams{i}))
            algspecParams{i} = {};
        end
    end
end
mkdir(resultsFolder)
end % processInput

function b = checkNumImgspecParams(p, len)
b = true;
if isempty(p)
    return
end
for i = 1:2:numel(p)
    if length(p{i+1}) ~= len
        b = false;
        return
    end
end
end