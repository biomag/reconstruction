function feineigleParamTest()
%FEINEIGLEPARAMTEST Summary of this function goes here
%   Detailed explanation goes here

lambda1 = num2cell(10.^(-5:5));
lambda2 = num2cell(10.^(-5:5));
imagePath = {'../DIC_Images/unsorted/cho6small.tif'};
direction = {135};
algorithm = {@dicFeineigle};
numiter = {5000};

resultsFolder = '../DIC_Images/results/paramTest/dicFeineigle/';

[results, headers, params] = generalTest('image', imagePath, ...
    'algorithm', algorithm, 'direction', direction, 'numiter', numiter, ...
    'resultsFolder', resultsFolder, 'lambda1', lambda1, 'lambda2', lambda2);
end

