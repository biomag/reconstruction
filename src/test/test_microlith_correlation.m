
folder = '/home/koosk/BRC/DIC_Images/synthetic/';
syntheticImageList = { ...
    [folder, 'circandcube.png'], ...
    [folder, 'circandcube_disttransf.png'], ...
    [folder, 'circle.tif'], ...
    [folder, 'cone.png'], ...
    [folder, 'cube.tif'], ...
    [folder, 'diamond.png'], ...
    [folder, 'ellipse.png'], ...
    [folder, 'gear.png'], ...
    [folder, 'hangar.png'], ...
    [folder, 'Lsign.png'], ...
    [folder, 'microscope.png'], ...
    [folder, 'moon.png'], ...
    [folder, 'multilevel_blocks.png'], ...
    [folder, 'pentagon.png'], ...
    [folder, 'pie.png'], ...
    [folder, 'pipe.png'], ...
    [folder, 'pyramid.png'], ...
    [folder, 'rectangle.png'], ...
    [folder, 'smiley.png'], ...
    [folder, 'star.png'], ...
    [folder, 'triangle.png'], ...
    };

NAValues = [0.75, 0.95, 1.25, 1.4, 1.51, 10, 15];
sigmaValues = 0.5:0.1:3.5;

corrResults = zeros(numel(syntheticImageList), numel(NAValues));
sigmaResults = zeros(numel(syntheticImageList), numel(NAValues));
for i = 1:numel(syntheticImageList)
    syntheticImage = im2double(imread(syntheticImageList{i}));
    for j = 1:numel(NAValues)


        %% Set parameters of the target.
        RISiO2=1.59;
        ThickSiO2=0.1;

        %% Parameters of the DIC microscope.
        DICparams.wavelength=0.550;
        if NAValues(j) <= 1.51
            DICparams.NAo=NAValues(j);
        else
            DICparams.NAo=1.51;
        end
        DICparams.NAc=NAValues(j);
        DICparams.nEmbb=1;
        DICparams.nImm=1.515;
        DICparams.shear=DICparams.wavelength/DICparams.NAo;
        DICparams.bias=45;
        DICparams.shearangle=45;

        %% Compute specimen transmission.
        xsim = 0:0.095:0.095*99;
        usim=0;
        % usim=-1:0.2:1;  % To simulate defocus.

        oplSample=(2*pi/DICparams.wavelength)*(DICparams.nEmbb-RISiO2)*syntheticImage*ThickSiO2;
        sample=exp(1i*oplSample);

        DICMicroscope=microlith(xsim,usim); % Second argument is z.

        % Compute system pupils and the image according to the model proposed by
        % Mehta and Sheppard.
        DICMicroscope.computesys('DIC',DICparams);
        DICImage=DICMicroscope.computeimage(sample,'CPU');
        mlithImg = mat2gray(DICImage);

%         %% Compare the etch profile and images.
%         DICDisp=gray2norm(DICImage);
        
        for k = 1:numel(sigmaValues)
            sigma = sigmaValues(k);
            linImg = mat2gray(imfilter(syntheticImage, getGaussDeriv('direction', -45, 'sigma', sigma), 'same'));
            corrval = corr(linImg(:), mlithImg(:));
            if corrval > corrResults(i,j)
                corrResults(i,j) = corrval;
                sigmaResults(i,j) = sigma;
            end
        end
    end
end

fs = fopen('corrtest_sigma', 'w');
fc = fopen('corrtest_corr', 'w');
fprintf(fs, 'image/NA');
fprintf(fc, 'image/NA');
for i = 1:numel(NAValues)
    fprintf(fs, ',%.2f', NAValues(i));
    fprintf(fc, ',%.2f', NAValues(i));
end
fprintf(fs, ',\n');
fprintf(fc, ',\n');

for i = 1:numel(syntheticImageList);
    syntheticImage = syntheticImageList{i};
    [~,fname,~] = fileparts(syntheticImage);
    fprintf(fs, fname);
    fprintf(fc, fname);
    for j = 1:numel(NAValues)
        fprintf(fs, ',%.2f', sigmaResults(i,j));
        fprintf(fc, ',%.4f', corrResults(i,j));
    end
    fprintf(fs, ',\n');
    fprintf(fc, ',\n');
end

fclose(fc);
fclose(fs);