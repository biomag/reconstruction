function results = generalSyntheticTest( syntheticImagePath, rotation, noise, psfDirection, algorithm, ...
    algspecParams, resultsFolder )
%GENERALSYNTHETICTEST Summary of this function goes here
%   Detailed explanation goes here

%% construct test parameters
psf = getPsfApproximation(psfDirection);
numRotations = sum(cellfun(@numel, rotation));
syntheticImage = cellfun(@(x)im2double(imread(x)) ,syntheticImagePath, 'uniformoutput', false);
inputImagePaths = cell(numel(noise)*numRotations,1);
rotatedSyntheticImages = cell(numRotations,1);
mkdir(resultsFolder,'input_images');
mkdir(resultsFolder,'rotated_images');
ctr = 0;
for i = 1:numel(syntheticImage)
    [~, fname, ~] = fileparts(syntheticImagePath{i});
    curr_rotation = rotation{i};
    for k = 1:numel(curr_rotation)
        rotatedImg = mat2gray(imrotate(syntheticImage{i}, curr_rotation(k), 'bilinear', 'crop'));
        img = mat2gray(imfilter(rotatedImg, psf, 'replicate'));
        for j = 1:numel(noise)
            ctr = ctr + 1;
            img_noisy = mat2gray(addWhiteNoise(img, noise{j}));
            inputImgPath = [resultsFolder,'input_images/', fname, '_rotation', num2str(curr_rotation(k)), ...
                '_noise', num2str(noise{j}), '.tif'];
            inputImagePaths{ctr} = inputImgPath;
            rotSynthPath = [resultsFolder,'rotated_images/', fname, '_rotation', num2str(curr_rotation(k)), '.png'];
            rotatedSyntheticImages{ctr} = rotSynthPath;
            imwrite(img_noisy, inputImgPath);
        end
        imwrite(rotatedImg, rotSynthPath, 'png');
    end
end

%% run the tests
results = generalTest('image', inputImagePaths, 'algorithm', ...
    algorithm, 'algspecParams', algspecParams, 'imgspecParams', {'direction', num2cell(repmat(psfDirection, numel(inputImagePaths),1))}, ...
    'resultsFolder', resultsFolder);
createStatsForSynthetic(results, inputImagePaths, psf, resultsFolder, rotatedSyntheticImages);

end

