function syntheticTest1_1()
%SYNTHETICTEST1 Synthetic DIC test part 1 of 4 (est)
%   dicFeineigle is omitted because it has no sigma parameter.
K1 = getGaussDeriv('sigma', 0.1); K1 = K1./sum(abs(K1(:)));
sigmas = {0.1};

psf = {K1};
direction = {0, 0, 0, 0, 0, -45, -45, -45, -45, -45};
imagePath = {'../DIC_Images/synthetic/cube.tif',...
         '../DIC_Images/synthetic/circle.tif',...
         '../DIC_Images/synthetic/triangle.png',...
         '../DIC_Images/synthetic/star.png', ...
         '../DIC_Images/synthetic/multilevel_blocks.png',...
         '../DIC_Images/synthetic/cube.tif',...
         '../DIC_Images/synthetic/circle.tif',...
         '../DIC_Images/synthetic/triangle.png',...
         '../DIC_Images/synthetic/star.png', ...
         '../DIC_Images/synthetic/multilevel_blocks.png'};
algorithm = {@dicEMM, @dicZhaozhengYin, @dicWiener};
algspecParams = {{'numiter', 20000, 'pyrsize', 1, 'wsmooth', 0.01},...
    {}, {}};

resultsFolder = '../DIC_Images/results/SyntheticTest/4_1/';

image = cellfun(@(x)im2double(imread(x)) ,imagePath, 'uniformoutput', false);
imagepaths = cell(numel(image),1);
newdirections = zeros(numel(image)*numel(psf),1);
mkdir(resultsFolder,'input_images');
for i = 1:numel(image)
    for j = 1:numel(psf)
        currpsf = imrotate(psf{j}, direction{i});
        img = mat2gray(imfilter(image{i},currpsf, 'replicate'));
        [~, fname, ~] = fileparts(imagePath{i});
        path = [resultsFolder,'input_images/', fname, '_psf',...
            num2str(j), '_direction', num2str(direction{i}),'.tif'];
        imagepaths{(i-1)*numel(psf) + j} = path;
        imwrite(img, path);
        newdirections((i-1)*numel(psf) + j) = direction{i};
    end
end
direction = num2cell(newdirections);

[headers, params] = generalTest('image', imagepaths, 'algorithm', ...
    algorithm, 'algspecParams', algspecParams, 'direction', direction, ...
    'resultsFolder', resultsFolder, 'kernelSigma', sigmas);
resultFiles = cellfun(@(x) strcat(resultsFolder, x, '.mat'), params(:,1), 'uniformoutput', false);
results = cellfun(@(x) loadResultFile(x), resultFiles, 'uniformoutput', false);

statHeaders = {'norm', 'norm_normalized', 'norm_convolved',... % 1-3
               'number_of_nonzeros',... % 4
               'corr', 'corr_convolved',... % 5-6
               'MSE_norm', 'SE_SD_norm', 'MSE_norm_convolved', 'SE_SD_norm_convolved'... % 7-10
               'MAE_norm', 'AE_SD_norm', 'MAE_norm_convolved', 'AE_SD_norm_convolved',... % 11-14
               'pos_norm', 'pos_norm_normalized', 'pos_norm_convolved',... % 15-17
               'pos_number_of_nonzeros',... % 17
               'pos_corr', 'pos_corr_convolved',... % 19-20
               'pos_MSE_norm', 'pos_SE_SD_norm', 'pos_MSE_norm_convolved', 'pos_SE_SD_norm_convolved'... % 21-24
               'pos_MAE_norm', 'pos_AE_SD_norm', 'pos_MAE_norm_convolved', 'pos_AE_SD_norm_convolved'}; % 25-28
numResults = length(results);
stats = cell(numResults, numel(statHeaders));
for i = 1:length(results)
    dicImagePath = imagepaths{params{i,end}};
    [~,dicImageName,~] = fileparts(dicImagePath);
    dicImage = im2double(imread(dicImagePath));
    for j = 1:numel(imagePath)
        [~, fname, ~] = fileparts(imagePath{j});
        if strncmpi(fname, dicImageName, length(fname));
            origImage = image{j};
        end
    end
    resim = results{i};
    nresim = mat2gray(resim);
    posresim = resim;
    posresim(posresim<0) = 0;
    nposresim = mat2gray(posresim);
    psfStrIdx = strfind(params{i,1}, 'psf')+3;
    respsf = psf{str2double(params{i,1}(psfStrIdx(1)))};
    conv_resim = imfilter(resim./max(abs(resim(:))),respsf);
    stats{i,1} = norm(origImage - resim);
    stats{i,2} = norm(mat2gray(origImage) - nresim); % origImage is already normalized in this test
    stats{i,3} = norm(mat2gray(dicImage) - mat2gray(conv_resim));
    thim = resim>=graythresh(resim);
    stats{i,4} = nnz(origImage - thim);
    stats{i,5} = corr2(origImage,resim);
    stats{i,6} = corr2(dicImage,conv_resim);
    diff = mat2gray(origImage) - nresim;
    sqdiff = diff.^2;
    stats{i,7} = mean2(sqdiff);
    stats{i,8} = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_resim)).^2;
    stats{i,9} = mean2(sqdiffconv);
    stats{i,10} = std2(sqdiffconv);
    stats{i,11} = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats{i,12} = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_resim);
    stats{i,13} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,14} = std2(abs(diffconv));
    
    conv_posresim = imfilter(posresim./max(abs(posresim(:))),respsf);
    stats{i,15} = norm(origImage - posresim);
    stats{i,16} = norm(mat2gray(origImage) - nposresim); % origImage is already normalized in this test
    stats{i,17} = norm(mat2gray(dicImage) - mat2gray(conv_posresim));
    thim = posresim>=graythresh(posresim);
    stats{i,18} = nnz(origImage - thim);
    stats{i,19} = corr2(origImage,posresim);
    stats{i,20} = corr2(dicImage,conv_posresim);
    diff = mat2gray(origImage) - nposresim;
    sqdiff = diff.^2;
    stats{i,21} = mean2(sqdiff);
    stats{i,22} = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_posresim)).^2;
    stats{i,23} = mean2(sqdiffconv);
    stats{i,24} = std2(sqdiffconv);
    stats{i,25} = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats{i,26} = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_posresim);
    stats{i,27} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,28} = std2(abs(diffconv));
end

numHeaders = numel(headers);
numStatHeaders = numel(statHeaders);
numResults = length(results);
fid = fopen([resultsFolder,'stats.csv'], 'wt');
for i = 1:numHeaders
    fprintf(fid, '%s', headers{i});
    fprintf(fid, ',');
end
for i = 1:numStatHeaders
    fprintf(fid, '%s', statHeaders{i});
    if i ~= numStatHeaders
        fprintf(fid, ',');
    else
        fprintf(fid, '\n');
    end
end
for i = 1:numResults
    for j = 1:numHeaders
        fprintf(fid, '%s', any2str(params{i,j}));
        fprintf(fid, ',');
    end
    for j = 1:numStatHeaders
        fprintf(fid, '%s', any2str(stats{i,j}));
        if j ~= numStatHeaders
            fprintf(fid, ',');
        else
            fprintf(fid, '\n');
        end
    end
end
fclose(fid);
end % syntheticTest