function printHeaders(file, obj, newline)
%PRINTHEADER Summary of this function goes here
%   Detailed explanation goes here
if nargin<3
    newline = true;
end
fields = fieldnames(obj);
for i = 1:numel(fields)
    fprintf(file,'%s', fields{i});
    if ~newline || i < numel(fields)
        fprintf(file, ',');
    end
end
if newline
    fprintf(file, '\n');
end
end
