function createStatsForSynthetic( varargin )
%CREATESTATSFORSYNTHETIC Create statistics for synthetic test
%   This function helps on test partitioning: it includes common parts.
%% parse inputs
p = inputParser;
addRequired(p, 'headers')
addRequired(p, 'params');
addRequired(p, 'inputImagePaths');
addRequired(p, 'psf');
addRequired(p, 'resultsFolder');
addRequired(p, 'syntheticImagePath');
parse(p, varargin{:});

headers = p.Results.headers;
params = p.Results.params;
inputImagePaths = p.Results.inputImagePaths;
psf = p.Results.psf;
resultsFolder = p.Results.resultsFolder;
syntheticImagePath = p.Results.syntheticImagePath;

%%
image = cellfun(@(x)im2double(imread(x)) ,syntheticImagePath, 'uniformoutput', false);
resultFiles = cellfun(@(x) strcat(resultsFolder, x, '.mat'), params(:,1), 'uniformoutput', false);
results = cellfun(@(x) loadResultFile(x), resultFiles, 'uniformoutput', false);

statHeaders = {'norm', 'norm_normalized', 'norm_convolved',... % 1-3
               'number_of_nonzeros',... % 4
               'corr', 'corr_convolved',... % 5-6
               'MSE_norm', 'SE_SD_norm', 'MSE_norm_convolved', 'SE_SD_norm_convolved'... % 7-10
               'MAE_norm', 'AE_SD_norm', 'MAE_norm_convolved', 'AE_SD_norm_convolved',... % 11-14
               'pos_norm', 'pos_norm_normalized', 'pos_norm_convolved',... % 15-17
               'pos_number_of_nonzeros',... % 17
               'pos_corr', 'pos_corr_convolved',... % 19-20
               'pos_MSE_norm', 'pos_SE_SD_norm', 'pos_MSE_norm_convolved', 'pos_SE_SD_norm_convolved'... % 21-24
               'pos_MAE_norm', 'pos_AE_SD_norm', 'pos_MAE_norm_convolved', 'pos_AE_SD_norm_convolved'}; % 25-28
numResults = length(results);
stats = cell(numResults, numel(statHeaders));
for i = 1:length(results)
    dicImagePath = inputImagePaths{params{i,end}};
    [~,dicImageName,~] = fileparts(dicImagePath);
    dicImage = im2double(imread(dicImagePath));
    for j = 1:numel(syntheticImagePath)
        [~, fname, ~] = fileparts(syntheticImagePath{j});
        if strncmpi(fname, dicImageName, length(fname));
            origImage = image{j};
        end
    end
    resim = results{i};
    nresim = mat2gray(resim);
    posresim = resim;
    posresim(posresim<0) = 0;
    nposresim = mat2gray(posresim);
    psfStrIdxStart = strfind(params{i,1}, 'psf')+3;
    psfStrIdxStart = psfStrIdxStart(1);
    psfStrIdxEnd = psfStrIdxStart;
    while true
        if strcmp(params{i,1}(psfStrIdxEnd+1), '_')
            break
        end
        psfStrIdxEnd = psfStrIdxEnd + 1;
    end
    psfStrIdx = params{i,1}(psfStrIdxStart:psfStrIdxEnd);
    respsf = psf{str2double(psfStrIdx)};
    conv_resim = imfilter(resim./max(abs(resim(:))),respsf);
    stats{i,1} = norm(origImage - resim);
    stats{i,2} = norm(mat2gray(origImage) - nresim); % origImage is already normalized in this test
    stats{i,3} = norm(mat2gray(dicImage) - mat2gray(conv_resim));
    thim = resim>=graythresh(resim);
    stats{i,4} = nnz(origImage - thim);
    stats{i,5} = corr2(origImage,resim);
    stats{i,6} = corr2(dicImage,conv_resim);
    diff = mat2gray(origImage) - nresim;
    sqdiff = diff.^2;
    stats{i,7} = mean2(sqdiff);
    stats{i,8} = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_resim)).^2;
    stats{i,9} = mean2(sqdiffconv);
    stats{i,10} = std2(sqdiffconv);
    stats{i,11} = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats{i,12} = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_resim);
    stats{i,13} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,14} = std2(abs(diffconv));
    
    conv_posresim = imfilter(posresim./max(abs(posresim(:))),respsf);
    stats{i,15} = norm(origImage - posresim);
    stats{i,16} = norm(mat2gray(origImage) - nposresim); % origImage is already normalized in this test
    stats{i,17} = norm(mat2gray(dicImage) - mat2gray(conv_posresim));
    thim = posresim>=graythresh(posresim);
    stats{i,18} = nnz(origImage - thim);
    stats{i,19} = corr2(origImage,posresim);
    stats{i,20} = corr2(dicImage,conv_posresim);
    diff = mat2gray(origImage) - nposresim;
    sqdiff = diff.^2;
    stats{i,21} = mean2(sqdiff);
    stats{i,22} = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_posresim)).^2;
    stats{i,23} = mean2(sqdiffconv);
    stats{i,24} = std2(sqdiffconv);
    stats{i,25} = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats{i,26} = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_posresim);
    stats{i,27} = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats{i,28} = std2(abs(diffconv));
end

numHeaders = numel(headers);
numStatHeaders = numel(statHeaders);
numResults = length(results);
fid = fopen([resultsFolder,'stats.csv'], 'wt');
for i = 1:numHeaders
    fprintf(fid, '%s', headers{i});
    fprintf(fid, ',');
end
for i = 1:numStatHeaders
    fprintf(fid, '%s', statHeaders{i});
    if i ~= numStatHeaders
        fprintf(fid, ',');
    else
        fprintf(fid, '\n');
    end
end
for i = 1:numResults
    for j = 1:numHeaders
        fprintf(fid, '%s', any2str(params{i,j}));
        fprintf(fid, ',');
    end
    for j = 1:numStatHeaders
        fprintf(fid, '%s', any2str(stats{i,j}));
        if j ~= numStatHeaders
            fprintf(fid, ',');
        else
            fprintf(fid, '\n');
        end
    end
end
fclose(fid);

end

