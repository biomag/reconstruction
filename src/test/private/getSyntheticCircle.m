function A = getSyntheticCircle( varargin )
%GETSYNTHETICCIRCLE Returns an image of a circle
%   Parameters:
%       size - image size, 1 or 2 elements vector
%       radius
%% parse inputs
p = inputParser;
defaultSize = 100;
defaultRadius= 25;

addParameter(p, 'size', defaultSize, @(x) isnumeric(x) && all(x>0) && ...
    (numel(x)==1 || numel(x)==2));
addParameter(p, 'radius', defaultRadius, @(x) isnumeric(x) && x>0 && ...
    numel(x)==1);
parse(p, varargin{:});

siz = p.Results.size;
if numel(siz)==1
    siz = [siz siz];
end
radius = p.Results.radius;

%% main part
centerW = siz(1)/2;
centerH = siz(2)/2;
[W,H] = meshgrid(1:siz(1),1:siz(2));
A = zeros(siz);
A(sqrt((W-centerW).^2 + (H-centerH).^2) < radius) = 1;
end

