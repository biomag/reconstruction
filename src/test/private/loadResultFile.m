function image = loadResultFile(path) %#ok
%LOADRESULTFILE Loads a result mat file.
%   Loads a mat file that contains a variable named 'image'. It is written
%   for synthetic tests, but does not limits to that.
load(path);
end