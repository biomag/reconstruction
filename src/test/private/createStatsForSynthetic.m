function createStatsForSynthetic( varargin )
%CREATESTATSFORSYNTHETIC Create statistics for synthetic test
%   This function helps on test partitioning: it includes common parts.
%% parse inputs
p = inputParser;
addRequired(p, 'results');
addRequired(p, 'inputImagePaths');
addRequired(p, 'psf');
addRequired(p, 'resultsFolder');
addRequired(p, 'syntheticImagePath');
parse(p, varargin{:});

results = p.Results.results;
inputImagePaths = p.Results.inputImagePaths;
psf = p.Results.psf;
resultsFolder = p.Results.resultsFolder;
syntheticImagePath = p.Results.syntheticImagePath;

%%
image = cellfun(@(x)initInputImage(imread(x)), syntheticImagePath, 'uniformoutput', false);
resultFiles = cellfun(@(x) strcat(resultsFolder, x, '.mat'), {results.image}, 'uniformoutput', false);
resultImages = cellfun(@(x) loadResultFile(x), resultFiles, 'uniformoutput', false);

stats = struct('norm', [], 'norm_normalized', [], 'norm_convolved', [], ... % 1-3
               'number_of_nonzeros', [], ... % 4
               'corr', [], 'corr_convolved', [], ... % 5-6
               'MSE_norm', [], 'SE_SD_norm', [], 'MSE_norm_convolved', [], 'SE_SD_norm_convolved', [], ... % 7-10
               'MAE_norm', [], 'AE_SD_norm', [], 'MAE_norm_convolved', [], 'AE_SD_norm_convolved', [], ... % 11-14
               'pos_norm', [], 'pos_norm_normalized', [], 'pos_norm_convolved', [], ... % 15-17
               'pos_number_of_nonzeros', [], ... % 17
               'pos_corr', [], 'pos_corr_convolved', [], ... % 19-20
               'pos_MSE_norm', [], 'pos_SE_SD_norm', [], 'pos_MSE_norm_convolved', [], 'pos_SE_SD_norm_convolved', [], ... % 21-24
               'pos_MAE_norm', [], 'pos_AE_SD_norm', [], 'pos_MAE_norm_convolved', [], 'pos_AE_SD_norm_convolved', []); % 25-28
stats(length(results)).norm = [];
for i = 1:length(results)
    dicImagePath = results(i).inputImage;
    [~,dicImageName,~] = fileparts(dicImagePath);
    dicImage = initInputImage(dicImagePath);
    for j = 1:numel(syntheticImagePath)
        [~, fname, ~] = fileparts(syntheticImagePath{j});
        if strncmpi(fname, dicImageName, length(fname));
            origImage = image{j};
        end
    end
    resim = resultImages{i};
    nresim = mat2gray(resim);
    posresim = resim;
    posresim(posresim<0) = 0;
    nposresim = mat2gray(posresim);
    conv_resim = imfilter(resim./max(abs(resim(:))),psf);
    stats(i).norm = norm(origImage - resim);
    stats(i).norm_normalized = norm(mat2gray(origImage) - nresim); % origImage is already normalized in this test
    stats(i).norm_convolved = norm(mat2gray(dicImage) - mat2gray(conv_resim));
    thim = resim>=graythresh(resim);
    stats(i).number_of_nonzeros = nnz(origImage - thim);
    stats(i).corr = corr2(origImage,resim);
    stats(i).corr_convolved = corr2(dicImage,conv_resim);
    diff = mat2gray(origImage) - nresim;
    sqdiff = diff.^2;
    stats(i).MSE_norm = mean2(sqdiff);
    stats(i).SE_SD_norm = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_resim)).^2;
    stats(i).MSE_norm_convolved = mean2(sqdiffconv);
    stats(i).SE_SD_norm_convolved = std2(sqdiffconv);
    stats(i).MAE_norm = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats(i).AE_SD_norm = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_resim);
    stats(i).MAE_norm_convolved = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats(i).AE_SD_norm_convolved = std2(abs(diffconv));
    
    conv_posresim = imfilter(posresim./max(abs(posresim(:))),psf);
    stats(i).pos_norm = norm(origImage - posresim);
    stats(i).pos_norm_normalized = norm(mat2gray(origImage) - nposresim); % origImage is already normalized in this test
    stats(i).pos_norm_convolved = norm(mat2gray(dicImage) - mat2gray(conv_posresim));
    thim = posresim>=graythresh(posresim);
    stats(i).pos_number_of_nonzeros = nnz(origImage - thim);
    stats(i).pos_corr = corr2(origImage,posresim);
    stats(i).pos_corr_convolved = corr2(dicImage,conv_posresim);
    diff = mat2gray(origImage) - nposresim;
    sqdiff = diff.^2;
    stats(i).pos_MSE_norm = mean2(sqdiff);
    stats(i).pos_SE_SD_norm = std2(sqdiff);
    sqdiffconv = (mat2gray(dicImage) - mat2gray(conv_posresim)).^2;
    stats(i).pos_MSE_norm_convolved = mean2(sqdiffconv);
    stats(i).pos_SE_SD_norm_convolved = std2(sqdiffconv);
    stats(i).pos_MAE_norm = mean2(abs(diff)); % norm(diff(:),1)/numel(origImage);
    stats(i).pos_AE_SD_norm = std2(abs(diff));
    diffconv = mat2gray(dicImage) - mat2gray(conv_posresim);
    stats(i).pos_MAE_norm_convolved = mean2(abs(diffconv)); % norm(diffconv(:),1)/numel(origImage);
    stats(i).pos_AE_SD_norm_convolved = std2(abs(diffconv));
    
%     [~, resultFilename, ~] = fileparts(results(i).image);
    imwrite(mat2gray(resim), [resultsFolder, results(i).image, '.png'], 'png');
end

fid = fopen([resultsFolder,'stats.csv'], 'wt');
printHeaders(fid, results, false);
printHeaders(fid, stats);
for i = 1:length(results)
    printFields(fid, results(i), false);
    printFields(fid, stats(i));
end
fclose(fid);

end

