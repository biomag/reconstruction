function A = getSyntheticRectange(varargin)
% GETSYNTHETICSQUARE Returns an image of a square
% Parameters:
%       imsize - image size, 1 or 2 elements vector
%       rectsize - rectangle size, 1 or 2 elements vector
%% parse inputs
p = inputParser;
defaultImsize = 100;
defaultRectsize= 20;

addParameter(p, 'imsize', defaultImsize, @(x) isnumeric(x) && all(x>0) && ...
    (numel(x)==1 || numel(x)==2));
addParameter(p, 'rectsize', defaultRectsize, @(x) isnumeric(x) && all(x>0) && ...
    (numel(x)==1 || numel(x)==2));
parse(p, varargin{:});

imsize = p.Results.imsize;
if numel(imsize)==1
    imsize = [imsize imsize];
end
rectsize = p.Results.rectsize;
if numel(rectsize)==1
    rectsize = [rectsize rectsize];
end

%% main part
centerW = imsize(1)/2;
centerH = imsize(2)/2;
[W,H] = meshgrid(1:imsize(1),1:imsize(2));
A = zeros(imsize);
A(abs(W-centerW)<rectsize(1) & abs(H-centerH)<rectsize(2)) = 1;
end

