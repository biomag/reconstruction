function printFields(file, obj, newline)
    %PRINTVALUES Summary of this function goes here
%   Detailed explanation goes here
if nargin<3
    newline = true;
end
fields = fieldnames(obj);
for i = 1:numel(fields)
    str = any2str(obj.(fields{i}));
    if ~isa(str, 'char')
        str = num2str(numel(str));
    end
    fprintf(file, '%s', str);
    if ~newline || i < numel(fields)
        fprintf(file, ',');
    end
end
if newline
    fprintf(file, '\n');
end
end

