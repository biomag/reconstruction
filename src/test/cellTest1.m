function cellTest1( )
%CELLTEST1 First cell test
%   Cell test on 20x objective cell images. Some algorithms are not
%   included here.
generalCellTest('../../DIC_Images/CHO/5/20x/', 'algorithm', {@dicWiener, @dicZhaozhengYin, @dicHilbert, @dicEMM, @dicFeineigle}, ...
    'algspecParams', {{}, {}, {}, {'numiter', 10000, 'tolerance', 10^-7, 'waccept', 0.5, 'wsmooth', 0.01}, {'numiter', 10000}});
% generalCellTest('../../DIC_Images/CHO/5/20x/', 'algorithm', {@dicHilbert}, ...
%     'algspecParams', {{}});

end

