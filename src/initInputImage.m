function img = initInputImage( img )
%UNTITLED Initialized the input image for 2D DIC reconstruction algorithms.
%   Initializes the input image for 2D DIC reoncstruction algorithms by
%   reading them from file if necessary and converting them to 2D matrices
%   if they are 3D 

if ischar(img)
    img = im2double(imread(img));
end
if ~ismatrix(img)
    if 3 == ndims(img)
        img = rgb2gray(img);
    else
        error(['The input image has unsupported number of dimensions!', ...
            ' Only 2 dimensional matrices can be processed']);
    end
end
if ~isfloat(img)
    img = im2double(img);
end
end

