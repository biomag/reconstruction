function [r, rPSF] = dicBlind( I, varargin)

% [r, rPSF] = dicBlind(nevtelen, 'direction', 135, 'PSF', 'ones', 'sigma', 1, 'filterSize', 17); figure, imshow(mat2gray(r)); figure, surf(rPSF)
%% parse inputs
p = inputParser;
defaultDirection = 0;
defaultKernelSigma = 1.5;
defaultPSF = -1;

addParameter(p, 'direction', defaultDirection, @isnumeric);
addParameter(p, 'kernelSigma', defaultKernelSigma, @isnumeric);
addParameter(p, 'psf', defaultPSF, @(A) isnumeric(A) || ischar(A));
parse(p, varargin{:});

direction = p.Results.direction;
kernelSigma = p.Results.kernelSigma;
psf = p.Results.psf;

if numel(psf)==1 && psf==-1
    psf = getGaussDeriv('sigma', kernelSigma, 'direction', direction);
end
psf = psf./sum(abs(psf(:)));

%% reconstruction
[r, rPSF] = deconvblind(I, psf, 5);

end

