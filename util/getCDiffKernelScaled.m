function K = getCDiffKernelScaled( dx, dy, scale, p )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 2
    dy = 0;
    scale = 1;
end
validateattributes(dx,{'numeric'},{'>=', 0, '<=', 4});
validateattributes(dy,{'numeric'},{'>=', 0, '<=', 4});
if nargin < 4;
    p = 2;
else
    limit = 8;
    if (dx>2 || dy>2)
        limit = 6;
    end
    validateattributes(p,{'numeric'},{'even', '>=', 2, '<=', limit});
end

d1p2 = [-0.5, 0, 0.5];
d1p4 = [1/12, -2/3, 0, 2/3, -1/12];
d1p6 = [-1/60, 3/20, -3/4, 0, 3/4, -3/20, 1/60];
d1p8 = [1/280, -4/105, 1/5, -4/5, 0, 4/5, -1/5, 4/105, -1/280];

d2p2 = [1, -2, 1];
d2p4 = [-1/12, 4/3, -5/2, 4/3, -1/12];
d2p6 = [1/90, -3/20, 3/2, -49/18, 3/2, -3/20, 1/90];
d2p8 = [-1/560, 8/315, -1/5, 8/5, -205/72, 8/5, -1/5, 8/315, -1/560];

d3p2 = [-1/2, 1, 0, -1, 1/2];
d3p4 = [1/8, -1, 13/8, 0, -13/8, 1, -1/8];
d3p6 = [-7/240, 3/10, -169/120, 61/30, 0, -61/30, 169/120, -3/10, 7/240];

d4p2 = [1, -4, 6, -4, 1];
d4p4 = [-1/6, 2, -13/2, 28/3, -13/2, 2, -1/6];
d4p6 = [7/240, -2/5, 169/60, -122/15, 91/8, -122/15, 169/60, -2/5, 7/240];

if dx==0
    dxc = 1;
else
    dxc = eval(['d', num2str(dx), 'p', num2str(p)]);
end

if dy==0
    dyc = 1;
else
    dyc = eval(['d', num2str(dy), 'p', num2str(p)]);
end

K = dxc.'*dyc;
if dx==0 || dy==0
    K = K';
end

sx = size(K, 1);
sy = size(K, 2);


if sx == 1
    K = imresize(K, [1 scale*sy]);
elseif sy == 1
    K = imresize(K, [scale*sx, 1]);
else
    K = imresize(K, scale);
end


end


