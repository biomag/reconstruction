function [X, Y, T, AUC] = calculateROCCurve( labels, scores)
%CALCULATEROCCURVE Calculates the Receiver Operating Characteristic curve
%   This function calculates the Receiver Operating Characteristic curve
%   by using the built in function of MATLAB in the Statistics and Machine
%   Learning toolbox. The function is customized for image reconstruction
%   problems, but hopefully generally applicable to similar scenarios.
%
%   The required inputs are the label image and the score/reconstructed image (returned by e.g. a
%   classifier). The label image should contain outlines and be inverted,
%   that is, the outlines should be black and the background is white. As a
%   first step, the function inverts the label image and fills the holes.

labels = imfill(1 - labels, 'holes') > 0;
[X,Y,T,AUC] = perfcurve(labels(:), scores(:), true, 'XVals', 0:0.001:1);

end

