function images = loadImages(paths)
%LOADIMAGES Loads images from cell array of strings.

images = cellfun(@(x)im2double(imread(x)),paths,'uniformoutput', false);
end % loadImages