function div = getDivergence(u, v)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[fx, ~] = imgradientxy(u, 'CentralDifference');
[~, fy] = imgradientxy(v, 'CentralDifference');
div = fx+fy;
end

