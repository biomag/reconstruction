function noisy = addWhiteNoise(im, decibel)
% ADDWHITENOISE Adds white noise defined in decibel to the input image
%   This function is inpired by awgn from Communications Systems Toolbox
%   and http://www.gaussianwaves.com/2010/02/generating-a-signal-waveform-with-required-snr-in-matlab-2/
if ~isa(im, 'double')
    im = im2double(im);
end

signalPower = sum(abs(im(:)).^2)/numel(im);
signalPower = 10*log10(signalPower);
noisePower = signalPower-decibel;
noisePower = 10^(noisePower/10);
noisy = im + (sqrt(noisePower))*randn(size(im));
end
