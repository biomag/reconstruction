function a = setZeroBound(a, s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin<2
    s = 1;
end
validateattributes(s,{'numeric'},{'>=', 1});
a([1:s, end-s+1:end], :) = 0;
a(:, [1:s, end-s+1:end]) = 0;
end

