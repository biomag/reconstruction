function anything = any2str( anything )
%ANY2STRING Converts (almost) anything to string
%   Self explanatory. Might not support some things though.
    if ischar(anything)
        return;
    end
    if isnumeric(anything)
        anything = num2str(anything);
    end
    if islogical(anything)
        if anything
            anything = 'true';
        else
            anything = 'false';
        end
    end
    if iscell(anything)
        if ~isempty(anything)
            anything = strjoin(cellfun(@any2str, anything, ...
                'UniformOutput', false));
        else
            anything = '';
        end
    end
    if isa(anything, 'function_handle')
        anything = strrep(func2str(anything), ',', ' ');
    end

end

