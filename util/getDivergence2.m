function div = getDivergence2(u, v, borderOpt)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

dx = getCDiffKernel(1,0);
dy = getCDiffKernel(0,1);
fx = imfilter(u, dx, borderOpt);
fy = imfilter(v, dy, borderOpt);
div = fx+fy;
end

