function parsave( image, filename)
%PARSAVE Save variable from parfor loop.
%   Only save(filenames, variable) syntax is supported by this function.

save(filename, 'image');
end

