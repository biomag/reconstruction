function g = getConvMtx(kernel, m, n)
% GETCONVMTX Returns the convolution matrix of the input kernel. The size 
% of the matrix will be NxN, where N = n*m is the number of pixels in the
% image. This function only supports replicative boundary condition.
% TODO only replicative is available, should be selectable by a parameter
% VERY SLOW ALGORITHM
%
% kernel - the kernel of which to create the matrix.
% m - number of rows in the image
% n - number of columns in the image

[km, kn] = size(kernel);
filter_center = floor((size(kernel) + 1)/2);
kcm = filter_center(1);
kcn = filter_center(2);
rm = kcm - 1;
rn = kcn - 1;
N = n*m;
g = spalloc(N, N, numel(kernel)*N);
row = spalloc(m, n, numel(kernel));
for j = 1:n
    for i = 1:m
        kms = 1;  % kernel row index start
        kme = km; % kernel row index end
        kns = 1;  % kernel column index start
        kne = kn; % kernel column index end
        repliKernel = kernel;
        if i<kcm
            kms = kcm - i+1;
            repliKernel(kms, :) = repliKernel(kms, :) + sum(kernel(1:kcm-i,:), 1);
        end
        if i>m-rm
            kme = km-(rm-(m-i));
            repliKernel(kme, :) = repliKernel(kme, :) + sum(kernel(end-(rm-(m-i))+1:end,:), 1);
        end
        if j<kcn
            kns = kcn - j+1;
            repliKernel(:, kns) = repliKernel(:, kns) + sum(kernel(:, kcn-j), 2);
        end
        if j>n-rn
            kne = kn-(rn-(n-j));
            repliKernel(:, kne) = repliKernel(:, kne) + sum(kernel(:, end-(rn-(n-j))+1:end), 2);
        end
        if i<kcm
            if j<kcn
                repliKernel(kms,kns) = repliKernel(kms,kns) + sum(sum(kernel(1:kms-1,1:kns-1)));
            end
            if j>n-rn
                repliKernel(kms,kne) = repliKernel(kms,kne) + sum(sum(kernel(1:kms-1,kne+1:end)));
            end
        end
        if i>m-rm
            if j<kcn
                repliKernel(kme,kns) = repliKernel(kme,kns) + sum(sum(kernel(kme+1:end,1:kns-1)));
            end
            if j>n-rn
                repliKernel(kme,kne) = repliKernel(kme,kne) + sum(sum(kernel(kme+1:end,kne+1:end)));
            end
        end
        row(max(i-rm,1):min(i+rm,end), max(j-rn,1):min(j+rn,end)) = ...
            repliKernel(kms:kme, kns:kne);
        g((j-1)*m + i, :) = row(:);
        row(:) = 0;
    end
end
end