For complete functionality the following third party libraries have to be installed:
 - CVX toolbox version 3.0 http://cvxr.com/cvx/
 - SCS package https://github.com/cvxgrp/scs
 - SEMU software http://www.kangli.org/code/demos/pcnddemo.zip
