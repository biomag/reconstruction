%% load commonly used images
fs = filesep;
try
    imgfolder = ['..', fs, '..', fs, 'DIC_Images', fs, 'unsorted', fs];
    jurkatOrig = imread([imgfolder, 'jurkatOrig.png']);
    nevtelen = imread([imgfolder, 'Nevtelen.png']);
    resz = imread([imgfolder, 'resz.jpg']);
    cho6 = im2double(imread([imgfolder, 'cho6.tif']));
    cho6small = im2double(imread([imgfolder, 'cho6small.tif']));
    blood1 = im2double(imread([imgfolder, 'blood1.tif']));
    jurkat = imread([imgfolder, 'jurkat.png']);
    cube1 = imread([imgfolder, 'cube_oblique_1_5x20x20um.tif']);
    cns = imread([imgfolder, 'cnsdic_8bit.png']);
    jurkatOrig = jurkatOrig(:,:,1); jurkatOrig = im2double(jurkatOrig);
    nevtelen = nevtelen(:,:,1); nevtelen = im2double(nevtelen);
    resz = im2double(resz);
    jurkat = im2double(jurkat);
    cube1 = im2double(cube1(:,:,1));
    cns = im2double(cns(:,:,1));
    clear imgfolder
catch ex
    warning('Could not initialize sample images!');
end

%% add paths
[DIC_PATH, ~, ~] = fileparts(mfilename('fullpath'));
DIC_PATH = [DIC_PATH, fs];
addpath([DIC_PATH, 'src', fs])
addpath([DIC_PATH, 'src', fs, 'kernel', fs])
addpath([DIC_PATH, 'src', fs, 'kernel', fs, '3D', fs])
addpath([DIC_PATH, 'src', fs, 'test', fs])
addpath([DIC_PATH, 'src', fs, 'gui', fs])
addpath([DIC_PATH, 'util', fs])
addpath([DIC_PATH, 'util', fs, 'matlab2tikz', fs, 'src', fs])
addpath([DIC_PATH, 'util', fs, 'paramiterator', fs])
addpath([DIC_PATH, 'linprog', fs])

%% init linprog
run(['linprog', fs, 'cvx30_mod', fs, 'cvx_startup.m'])
% run('../src/linprog/cvx21/cvx_startup.m')

addpath(['linprog', fs, 'scs', fs])
addpath(['linprog', fs, 'scs', fs, 'matlab', fs])
clear fs DIC_PATH imgfolder ex
