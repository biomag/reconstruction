fs = filesep;
addpath(['src', fs])
addpath(['src', fs, 'kernel', fs])
addpath(['src', fs, 'gui', fs])
addpath(['util', fs])
addpath(['linprog', fs])

%% init linprog
try
	run(['linprog', fs, 'cvx30_mod', fs, 'cvx_startup.m'])
	addpath(['linprog', fs, 'scs', fs])
	addpath(['linprog', fs, 'scs', fs, 'matlab', fs])
catch ex
	warning('Could not initialize linear programming toolboxes!');
end
clear fs
